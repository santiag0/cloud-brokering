#ifndef INC_REQ_newGASA
#define INC_REQ_newGASA

#include <assert.h>

#include "newGASA.hh"
#include "newGA.hh"
#include "newGASAaux.hh"
#include "Utils.hh"
#include "config.hh"

int NEXT_G_WORKLOAD = 0;

// MACH_TYPE
int N_MACH_TYPE = 0;
// Formato: Booked  Demand  GB      price
// Ejemplo: 0.027   0.060   0.12    0.048
float MACH_TYPE_RAW[MAXN_MACH_TYPE][4];

// WORKLOAD (BATCH ACTUAL)
int N_WORKLOAD = 0;
// Formato: Memory, Processor, Storage, Number of cores, Data Transfer, Time, Deadline, Arrival Time
// Ejemplo: 1.00 1.00 466.00 1 221.00 1337.50 158.52 132.10
int WORKLOAD_ID[MAXN_WORKLOAD];
float WORKLOAD_MEM[MAXN_WORKLOAD];
float WORKLOAD_PROC[MAXN_WORKLOAD];
float WORKLOAD_DISK[MAXN_WORKLOAD];
int WORKLOAD_NCORES[MAXN_WORKLOAD];
float WORKLOAD_DTRANS[MAXN_WORKLOAD];
float WORKLOAD_ETIME[MAXN_WORKLOAD];
float WORKLOAD_DTIME[MAXN_WORKLOAD];
float WORKLOAD_ATIME[MAXN_WORKLOAD];

// WORKLOAD (TODOS LOS TRABAJOS)
int G_N_WORKLOAD = 0;
// Formato: Memory, Processor, Storage, Number of cores, Data Transfer, Time, Deadline, Arrival Time
// Ejemplo: 1.00 1.00 466.00 1 221.00 1337.50 158.52 132.10
float G_WORKLOAD_MEM[MAXN_G_WORKLOAD];
float G_WORKLOAD_PROC[MAXN_G_WORKLOAD];
float G_WORKLOAD_DISK[MAXN_G_WORKLOAD];
int G_WORKLOAD_NCORES[MAXN_G_WORKLOAD];
float G_WORKLOAD_DTRANS[MAXN_G_WORKLOAD];
float G_WORKLOAD_ETIME[MAXN_G_WORKLOAD];
float G_WORKLOAD_DTIME[MAXN_G_WORKLOAD];
float G_WORKLOAD_ATIME[MAXN_G_WORKLOAD];

int G_WORKLOAD_BOOKED_CHEAPEST[MAXN_G_WORKLOAD];
int G_WORKLOAD_ONDEMAND_CHEAPEST[MAXN_G_WORKLOAD];

int G_WORKLOAD_PRICE_CHEAPEST[MAXN_G_WORKLOAD];
float G_WORKLOAD_PRICE[MAXN_G_WORKLOAD];

// SCENARIO
int N_SCENARIO = 0;
// Formato: Memory, Processor, Storage, Number of cores, type
// Ejemplo: 17.10 3.25 420.00 2 3
float SCENARIO_MEM[MAXN_SCENARIO];
float SCENARIO_PROC[MAXN_SCENARIO];
float SCENARIO_DISK[MAXN_SCENARIO];
int SCENARIO_NCORES[MAXN_SCENARIO];
int SCENARIO_TYPE[MAXN_SCENARIO];
float SCENARIO_TIME_TO_READY[MAXN_SCENARIO];

// TRUE SCENARIO
// (sin repetir, ordenados por costo de menor a mayor)
int TN_SCENARIO_ONDEMAND_SORTED = 0;
int T_SCENARIO_ONDEMAND_SORTED[MAXN_SCENARIO];

int TN_SCENARIO_PREBOOKED_SORTED = 0;
int T_SCENARIO_PREBOOKED_SORTED[MAXN_SCENARIO];

int TN_SCENARIO_PRICE_SORTED = 0;
int T_SCENARIO_PRICE_SORTED[MAXN_SCENARIO];

inline bool can_exec_task(const int m_id, const int t_id) {
    return ((WORKLOAD_MEM[t_id] <= SCENARIO_MEM[m_id]) &&
        (WORKLOAD_PROC[t_id] <= SCENARIO_PROC[m_id]) &&
        (WORKLOAD_DISK[t_id] <= SCENARIO_DISK[m_id]) &&
        (WORKLOAD_NCORES[t_id] <= SCENARIO_NCORES[m_id]));
}

inline bool global_can_exec_task(const int m_id, const int t_id) {
    return ((G_WORKLOAD_MEM[t_id] <= SCENARIO_MEM[m_id]) &&
        (G_WORKLOAD_PROC[t_id] <= SCENARIO_PROC[m_id]) &&
        (G_WORKLOAD_DISK[t_id] <= SCENARIO_DISK[m_id]) &&
        (G_WORKLOAD_NCORES[t_id] <= SCENARIO_NCORES[m_id]));
}

skeleton newGASA
{
    // Problem ---------------------------------------------------------------

    //:_dimension(0)
    Problem::Problem()
    {}

    ostream& operator<< (ostream& os, const Problem& pbm)
    {
        cout << "ostream& operator<< (ostream& os, const Problem& pbm)";
        exit(-1);

        //os << endl << endl << "Number of Variables " << pbm._dimension
        //   << endl;
        return os;
    }

    /*istream& operator>> (istream& is, Problem& pbm)
    {
        char buffer[MAX_BUFFER];
        int i;

        is.getline(buffer,MAX_BUFFER,'\n');
        sscanf(buffer,"%d",&pbm._dimension);

        return is;
    }*/

    void Problem::load(istream& c_file, istream& s_file, istream& w_file) {
        char buffer[F_MAX_BUFFER];

        // Leo el archivo de costos
        while (!c_file.eof()) {
            c_file.getline(buffer, F_MAX_BUFFER, '\n');

            if (!c_file.eof()) {
                sscanf(buffer, "%f %f %f %f",
                    &MACH_TYPE_RAW[N_MACH_TYPE][0],
                    &MACH_TYPE_RAW[N_MACH_TYPE][1],
                    &MACH_TYPE_RAW[N_MACH_TYPE][2],
                    &MACH_TYPE_RAW[N_MACH_TYPE][3]);

                N_MACH_TYPE++;
            }
        }

        #ifdef DEBUG
            /*printf("=== COST FILE =====================\n");
            for (int i=0; i<N_MACH_TYPE; i++) {
                printf("[%d] %.4f\t%.4f\t%.4f\t%.4f\n", i,
                    MACH_TYPE_RAW[i][0], MACH_TYPE_RAW[i][1], MACH_TYPE_RAW[i][2], MACH_TYPE_RAW[i][3]);
            }*/
        #endif

        // Leo el archivo de scenario
        while (!s_file.eof()) {
            s_file.getline(buffer, F_MAX_BUFFER, '\n');

            if (!s_file.eof()) {
                sscanf(buffer, "%f %f %f %d %d",
                    &SCENARIO_MEM[N_SCENARIO],
                    &SCENARIO_PROC[N_SCENARIO],
                    &SCENARIO_DISK[N_SCENARIO],
                    &SCENARIO_NCORES[N_SCENARIO],
                    &SCENARIO_TYPE[N_SCENARIO]);

                SCENARIO_TIME_TO_READY[N_SCENARIO] = 0;

                N_SCENARIO++;
            }
        }

        #ifdef DEBUG
            /*printf("=== SCENARIO FILE =================\n");
            for (int i=0; i<N_SCENARIO; i++) {
                printf("[%d] %.4f\t%.4f\t%.4f\t%d\t%d\t%.2f\n", i,
                    SCENARIO_MEM[i], SCENARIO_PROC[i],
                    SCENARIO_DISK[i], SCENARIO_NCORES[i],
                    SCENARIO_TYPE[i], SCENARIO_TIME_TO_READY[i]);
            }*/
        #endif

        // Leo el archivo de workload
        while (!w_file.eof()) {
            w_file.getline(buffer, F_MAX_BUFFER, '\n');

            if (!w_file.eof()) {
                sscanf(buffer, "%f %f %f %d %f %f %f %f",
                    &G_WORKLOAD_MEM[G_N_WORKLOAD],
                    &G_WORKLOAD_PROC[G_N_WORKLOAD],
                    &G_WORKLOAD_DISK[G_N_WORKLOAD],
                    &G_WORKLOAD_NCORES[G_N_WORKLOAD],
                    &G_WORKLOAD_DTRANS[G_N_WORKLOAD],
                    &G_WORKLOAD_ETIME[G_N_WORKLOAD],
                    &G_WORKLOAD_DTIME[G_N_WORKLOAD],
                    &G_WORKLOAD_ATIME[G_N_WORKLOAD]);

                G_N_WORKLOAD++;
            }
        }

        #ifdef SUPERDEBUG
            printf("=== WORKLOAD FILE =================\n");
            for (int i=0; i<G_N_WORKLOAD; i++) {
                printf("[%d] %.2f\t%.2f\t%.2f\t%d\t%.2f\t%.2f\t\t%.2f\t\t%.2f\n", i,
                    G_WORKLOAD_MEM[i], G_WORKLOAD_PROC[i],
                    G_WORKLOAD_DISK[i], G_WORKLOAD_NCORES[i],
                    G_WORKLOAD_DTRANS[i], G_WORKLOAD_ETIME[i],
                    G_WORKLOAD_DTIME[i], G_WORKLOAD_ATIME[i]);
            }
        #endif

        // =====================================================
        // Ordeno los tipos de máquina por costo y sin repetidos
        // =====================================================
        {
            TN_SCENARIO_ONDEMAND_SORTED = N_SCENARIO;
            for (int i = 0; i < N_SCENARIO; i++) T_SCENARIO_ONDEMAND_SORTED[i] = i;

            int index = 0;
            for (int i = 0; i < N_SCENARIO; i++) {
                int min_s;
                min_s = i;

                for (int j = i+1; j < N_SCENARIO; j++) {
                    if (MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_ONDEMAND_SORTED[j]]][1] <
                        MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_ONDEMAND_SORTED[min_s]]][1]) {

                        min_s = j;
                    }
                }

                if (min_s > i) {
                    int aux;
                    aux = T_SCENARIO_ONDEMAND_SORTED[i];
                    T_SCENARIO_ONDEMAND_SORTED[i] = T_SCENARIO_ONDEMAND_SORTED[min_s];
                    T_SCENARIO_ONDEMAND_SORTED[min_s] = aux;
                }

                T_SCENARIO_ONDEMAND_SORTED[index] = T_SCENARIO_ONDEMAND_SORTED[i];

                if (index-1 >= 0) {
                    if ((SCENARIO_MEM[T_SCENARIO_ONDEMAND_SORTED[index]] == SCENARIO_MEM[T_SCENARIO_ONDEMAND_SORTED[index-1]]) &&
                        (SCENARIO_PROC[T_SCENARIO_ONDEMAND_SORTED[index]] == SCENARIO_PROC[T_SCENARIO_ONDEMAND_SORTED[index-1]]) &&
                        (SCENARIO_DISK[T_SCENARIO_ONDEMAND_SORTED[index]] == SCENARIO_DISK[T_SCENARIO_ONDEMAND_SORTED[index-1]]) &&
                        (SCENARIO_NCORES[T_SCENARIO_ONDEMAND_SORTED[index]] == SCENARIO_NCORES[T_SCENARIO_ONDEMAND_SORTED[index-1]]) &&
                        (SCENARIO_TYPE[T_SCENARIO_ONDEMAND_SORTED[index]] == SCENARIO_TYPE[T_SCENARIO_ONDEMAND_SORTED[index-1]])) {
                            TN_SCENARIO_ONDEMAND_SORTED--;
                        } else {
                            index++;
                        }
                } else {
                    index++;
                }
            }
        }
        {
            TN_SCENARIO_PREBOOKED_SORTED = N_SCENARIO;
            for (int i = 0; i < N_SCENARIO; i++) T_SCENARIO_PREBOOKED_SORTED[i] = i;

            int index = 0;
            for (int i = 0; i < N_SCENARIO; i++) {
                int min_s;
                min_s = i;

                for (int j = i+1; j < N_SCENARIO; j++) {
                    if (MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PREBOOKED_SORTED[j]]][0] <
                        MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PREBOOKED_SORTED[min_s]]][0]) {

                        min_s = j;
                    }
                }

                if (min_s > i) {
                    int aux;
                    aux = T_SCENARIO_PREBOOKED_SORTED[i];
                    T_SCENARIO_PREBOOKED_SORTED[i] = T_SCENARIO_PREBOOKED_SORTED[min_s];
                    T_SCENARIO_PREBOOKED_SORTED[min_s] = aux;
                }

                T_SCENARIO_PREBOOKED_SORTED[index] = T_SCENARIO_PREBOOKED_SORTED[i];

                if (index-1 >= 0) {
                    if ((SCENARIO_MEM[T_SCENARIO_PREBOOKED_SORTED[index]] == SCENARIO_MEM[T_SCENARIO_PREBOOKED_SORTED[index-1]]) &&
                        (SCENARIO_PROC[T_SCENARIO_PREBOOKED_SORTED[index]] == SCENARIO_PROC[T_SCENARIO_PREBOOKED_SORTED[index-1]]) &&
                        (SCENARIO_DISK[T_SCENARIO_PREBOOKED_SORTED[index]] == SCENARIO_DISK[T_SCENARIO_PREBOOKED_SORTED[index-1]]) &&
                        (SCENARIO_NCORES[T_SCENARIO_PREBOOKED_SORTED[index]] == SCENARIO_NCORES[T_SCENARIO_PREBOOKED_SORTED[index-1]]) &&
                        (SCENARIO_TYPE[T_SCENARIO_PREBOOKED_SORTED[index]] == SCENARIO_TYPE[T_SCENARIO_PREBOOKED_SORTED[index-1]])) {
                            TN_SCENARIO_PREBOOKED_SORTED--;
                        } else {
                            index++;
                        }
                } else {
                    index++;
                }
            }
        }
        {
            TN_SCENARIO_PRICE_SORTED = N_SCENARIO;
            for (int i = 0; i < N_SCENARIO; i++) T_SCENARIO_PRICE_SORTED[i] = i;

            int index = 0;
            for (int i = 0; i < N_SCENARIO; i++) {
                int min_s;
                min_s = i;

                for (int j = i+1; j < N_SCENARIO; j++) {
                    if (MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[j]]][3] <
                        MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[min_s]]][3]) {

                        min_s = j;
                    }
                }

                if (min_s > i) {
                    int aux;
                    aux = T_SCENARIO_PRICE_SORTED[i];
                    T_SCENARIO_PRICE_SORTED[i] = T_SCENARIO_PRICE_SORTED[min_s];
                    T_SCENARIO_PRICE_SORTED[min_s] = aux;
                }

                T_SCENARIO_PRICE_SORTED[index] = T_SCENARIO_PRICE_SORTED[i];

                if (index-1 >= 0) {
                    if ((SCENARIO_MEM[T_SCENARIO_PRICE_SORTED[index]] == SCENARIO_MEM[T_SCENARIO_PRICE_SORTED[index-1]]) &&
                        (SCENARIO_PROC[T_SCENARIO_PRICE_SORTED[index]] == SCENARIO_PROC[T_SCENARIO_PRICE_SORTED[index-1]]) &&
                        (SCENARIO_DISK[T_SCENARIO_PRICE_SORTED[index]] == SCENARIO_DISK[T_SCENARIO_PRICE_SORTED[index-1]]) &&
                        (SCENARIO_NCORES[T_SCENARIO_PRICE_SORTED[index]] == SCENARIO_NCORES[T_SCENARIO_PRICE_SORTED[index-1]]) &&
                        (SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[index]] == SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[index-1]])) {
                            TN_SCENARIO_PRICE_SORTED--;
                        } else {
                            index++;
                        }
                } else {
                    index++;
                }
            }
        }
        /*
        #ifdef DEBUG
            printf("=== PREBOOKED SORTED SCENARIOS ==============\n");
            cout << "TN_SCENARIO_PREBOOKED_SORTED = " << TN_SCENARIO_PREBOOKED_SORTED << endl;
            for (int i = 0; i < TN_SCENARIO_PREBOOKED_SORTED; i++) {
                cout << "[" << i << "] " << T_SCENARIO_PREBOOKED_SORTED[i]
                    << ":" << MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PREBOOKED_SORTED[i]]][0]
                    << endl;
            }
            cout << endl;
            printf("=== ONDEMAND SORTED SCENARIOS ==============\n");
            cout << "TN_SCENARIO_ONDEMAND_SORTED = " << TN_SCENARIO_ONDEMAND_SORTED << endl;
            for (int i = 0; i < TN_SCENARIO_ONDEMAND_SORTED; i++) {
                cout << "[" << i << "] " << T_SCENARIO_ONDEMAND_SORTED[i]
                    << ":" << MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_ONDEMAND_SORTED[i]]][1]
                    << endl;
            }
            cout << endl;
            printf("=== PRICE SORTED SCENARIOS ==============\n");
            cout << "TN_SCENARIO_PRICE_SORTED = " << TN_SCENARIO_PRICE_SORTED << endl;
            for (int i = 0; i < TN_SCENARIO_PRICE_SORTED; i++) {
                cout << "[" << i << "] " << T_SCENARIO_PRICE_SORTED[i]
                    << ":" << MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[i]]][1]
                    << endl;
            }
            cout << endl;
        #endif
        */
        for (int t_id = 0; t_id < G_N_WORKLOAD; t_id++) {
            // Busco la máquina más barata en booked...
            G_WORKLOAD_BOOKED_CHEAPEST[t_id] = -1;

            for (int i = 0; (i < TN_SCENARIO_PREBOOKED_SORTED) && (G_WORKLOAD_BOOKED_CHEAPEST[t_id] == -1); i++) {
                if (global_can_exec_task(T_SCENARIO_PREBOOKED_SORTED[i], t_id)) {
                    G_WORKLOAD_BOOKED_CHEAPEST[t_id] = T_SCENARIO_PREBOOKED_SORTED[i];
                }
            }

            // Busco la máquina más barata en ondemand...
            G_WORKLOAD_ONDEMAND_CHEAPEST[t_id] = -1;

            for (int i = 0; (i < TN_SCENARIO_ONDEMAND_SORTED) && (G_WORKLOAD_ONDEMAND_CHEAPEST[t_id] == -1); i++) {
                if (global_can_exec_task(T_SCENARIO_ONDEMAND_SORTED[i], t_id)) {
                    G_WORKLOAD_ONDEMAND_CHEAPEST[t_id] = T_SCENARIO_ONDEMAND_SORTED[i];
                }
            }

            // Busco la máquina más barata en price...
            G_WORKLOAD_PRICE_CHEAPEST[t_id] = -1;

            for (int i = 0; (i < TN_SCENARIO_PRICE_SORTED) && (G_WORKLOAD_PRICE_CHEAPEST[t_id] == -1); i++) {
                if (global_can_exec_task(T_SCENARIO_PRICE_SORTED[i], t_id)) {
                    G_WORKLOAD_PRICE_CHEAPEST[t_id] = T_SCENARIO_PRICE_SORTED[i];
                    G_WORKLOAD_PRICE[t_id] = MACH_TYPE_RAW[SCENARIO_TYPE[T_SCENARIO_PRICE_SORTED[i]]][3];
                }
            }
        }

        /*#ifdef DEBUG
            printf("=== PREBOOKED CHEAPEST MACHINES ==============\n");
            for (int t_id = 0; t_id < G_N_WORKLOAD; t_id++) {
                cout << "[" << t_id << "] cheapest m_id=" << G_WORKLOAD_BOOKED_CHEAPEST[t_id]
                    << " type=" << SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[t_id]]
                    << " cost=" << MACH_TYPE_RAW[SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[t_id]]][0]
                    << endl;
            }
            cout << endl;
            printf("=== ONDEMAND CHEAPEST MACHINES ==============\n");
            for (int t_id = 0; t_id < G_N_WORKLOAD; t_id++) {
                cout << "[" << t_id << "] cheapest m_id=" << G_WORKLOAD_ONDEMAND_CHEAPEST[t_id]
                    << " type=" << SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[t_id]]
                    << " cost=" << MACH_TYPE_RAW[SCENARIO_TYPE[G_WORKLOAD_ONDEMAND_CHEAPEST[t_id]]][1]
                    << endl;
            }
            cout << endl;
            printf("=== PRICE CHEAPEST MACHINES ==============\n");
            for (int t_id = 0; t_id < G_N_WORKLOAD; t_id++) {
                cout << "[" << t_id << "] cheapest m_id=" << G_WORKLOAD_BOOKED_CHEAPEST[t_id]
                    << " type=" << SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[t_id]]
                    << " cost=" << MACH_TYPE_RAW[SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[t_id]]][0]
                    << " (" << G_WORKLOAD_PRICE << ")"
                    << endl;
            }
            cout << endl;
        #endif
        */
     }

    bool Problem::operator== (const Problem& pbm) const
    {
        cout << "bool Problem::operator== (const Problem& pbm) const";
        exit(-1);

        //if (_dimension!=pbm.dimension()) return false;
        return true;
    }

    bool Problem::operator!= (const Problem& pbm) const
    {
        return !(*this == pbm);
    }

    Direction Problem::direction() const
    {
        return maximize;
        //return minimize;
    }

    int Problem::get_workload_count() const {
        return N_WORKLOAD;
    }

    int Problem::get_workload_id(const int t_id) const {
        return WORKLOAD_ID[t_id];
    }

    float Problem::get_global_workload_etime(const int t_id) const {
        return G_WORKLOAD_ETIME[t_id];
    }

    int Problem::get_global_workload_count() const {
        return G_N_WORKLOAD;
    }

    float Problem::get_cost_ondemand(const int c_id) const {
        return MACH_TYPE_RAW[c_id][1];
    }

    int Problem::get_scenario_type(const int m_id) const {
        return SCENARIO_TYPE[m_id];
    }

    void Problem::update_workload_batch(const Solution &initial) const {
        if (initial.is_initialized() == true) {
            cout << "initial.is_initialized() = " << initial.is_initialized() << endl;
            initial.print();

            // Descartar el workload que ya se ejecutó...
            // Pasar el workload en ejecución a machine initial load...
            // Pasar el workload sin ejecutar a N_WORKLOAD...
            int current_index = -1;
            float last_time_step = CURRENT_TIME - TIME_STEP;

            for (int i = 0; i < N_SCENARIO; i++) {
                if (SCENARIO_TIME_TO_READY[i] > 0) {
                    SCENARIO_TIME_TO_READY[i] -= TIME_STEP;

                    if (SCENARIO_TIME_TO_READY[i] < 0) {
                        SCENARIO_TIME_TO_READY[i] = 0;
                    }
                }
            }

            for (int i = 0; i < N_WORKLOAD; i++) {
                int m_id = initial.get_machine_assign(i);
                float stime = initial.get_task_stime(i) - TIME_STEP;

                if (m_id >= 0) {
                    if ((stime > 0) && (CURRENT_TIME < WORKLOAD_DTIME[i])) {
                        // Puedo re-schedulearlo
                        current_index++;

                        if (current_index < i) {
                            WORKLOAD_ID[current_index] = WORKLOAD_ID[i];
                            WORKLOAD_MEM[current_index] = WORKLOAD_MEM[i];
                            WORKLOAD_PROC[current_index] = WORKLOAD_PROC[i];
                            WORKLOAD_DISK[current_index] = WORKLOAD_DISK[i];
                            WORKLOAD_NCORES[current_index] = WORKLOAD_NCORES[i];
                            WORKLOAD_DTRANS[current_index] = WORKLOAD_DTRANS[i];
                            WORKLOAD_ETIME[current_index] = WORKLOAD_ETIME[i];
                            WORKLOAD_DTIME[current_index] = WORKLOAD_DTIME[i];
                            WORKLOAD_ATIME[current_index] = WORKLOAD_ATIME[i];
                        }
                    } else {
                        // NO puedo re-schedulearlo
                        if (stime + WORKLOAD_ETIME[i] <= 0) {
                            // Ya termine de ejecutar la tarea!!!
                            // nada para hacer...
                        } else {
                            #ifdef SUPERDEBUG
                                cout << "t_id=" << i << " m_id=" << m_id << " "
                                    << stime << "+" << WORKLOAD_ETIME[i] << "-" << TIME_STEP << "="
                                    << stime + WORKLOAD_ETIME[i] - TIME_STEP << endl;
                            #endif
                            SCENARIO_TIME_TO_READY[m_id] = stime + WORKLOAD_ETIME[i];
                        }
                    }
                }
            }

            N_WORKLOAD = current_index+1;
        } else {
            N_WORKLOAD = 0;
        }

        // Pasar el workload de G_N_WORKLOAD que llegó en el nuevo batch...
        bool finished = false;
        while (!finished) {
            finished = (NEXT_G_WORKLOAD >= G_N_WORKLOAD);

            if (!finished) {
                finished = (G_WORKLOAD_ATIME[NEXT_G_WORKLOAD] > CURRENT_TIME);

                if (!finished) {
                    WORKLOAD_ID[N_WORKLOAD] = NEXT_G_WORKLOAD;
                    WORKLOAD_MEM[N_WORKLOAD] = G_WORKLOAD_MEM[NEXT_G_WORKLOAD];
                    WORKLOAD_PROC[N_WORKLOAD] = G_WORKLOAD_PROC[NEXT_G_WORKLOAD];
                    WORKLOAD_DISK[N_WORKLOAD] = G_WORKLOAD_DISK[NEXT_G_WORKLOAD];
                    WORKLOAD_NCORES[N_WORKLOAD] = G_WORKLOAD_NCORES[NEXT_G_WORKLOAD];
                    WORKLOAD_DTRANS[N_WORKLOAD] = G_WORKLOAD_DTRANS[NEXT_G_WORKLOAD];
                    WORKLOAD_ETIME[N_WORKLOAD] = G_WORKLOAD_ETIME[NEXT_G_WORKLOAD];

                    //HACK!
                    #ifdef HACK_NO_ARRIVAL
                        WORKLOAD_DTIME[N_WORKLOAD] = G_WORKLOAD_DTIME[NEXT_G_WORKLOAD];
                    #else
                        WORKLOAD_DTIME[N_WORKLOAD] = G_WORKLOAD_DTIME[NEXT_G_WORKLOAD] - CURRENT_TIME;
                    #endif

                    WORKLOAD_ATIME[N_WORKLOAD] = G_WORKLOAD_ATIME[NEXT_G_WORKLOAD];

                    N_WORKLOAD++;
                    NEXT_G_WORKLOAD++;
                }
            }
        }

        int aux_WORKLOAD_ID;
        float aux_WORKLOAD_MEM;
        float aux_WORKLOAD_PROC;
        float aux_WORKLOAD_DISK;
        int aux_WORKLOAD_NCORES;
        float aux_WORKLOAD_DTRANS;
        float aux_WORKLOAD_ETIME;
        float aux_WORKLOAD_DTIME;
        float aux_WORKLOAD_ATIME;

        #ifdef WORKLOAD_DEADLINE_SORT
            // Re-ordeno el batch por earliest deadline
            for (int end_pos = 0; end_pos < N_WORKLOAD; end_pos++) {
                for (int current_t_id = N_WORKLOAD-1; current_t_id > end_pos; current_t_id--) {
                    if (WORKLOAD_DTIME[current_t_id] < WORKLOAD_DTIME[current_t_id-1]) {
                        aux_WORKLOAD_ID = WORKLOAD_ID[current_t_id];
                        aux_WORKLOAD_MEM = WORKLOAD_MEM[current_t_id];
                        aux_WORKLOAD_PROC = WORKLOAD_PROC[current_t_id];
                        aux_WORKLOAD_DISK = WORKLOAD_DISK[current_t_id];
                        aux_WORKLOAD_NCORES = WORKLOAD_NCORES[current_t_id];
                        aux_WORKLOAD_DTRANS = WORKLOAD_DTRANS[current_t_id];
                        aux_WORKLOAD_ETIME = WORKLOAD_ETIME[current_t_id];
                        aux_WORKLOAD_DTIME = WORKLOAD_DTIME[current_t_id];
                        aux_WORKLOAD_ATIME = WORKLOAD_ATIME[current_t_id];

                        WORKLOAD_ID[current_t_id] = WORKLOAD_ID[current_t_id-1];
                        WORKLOAD_MEM[current_t_id] = WORKLOAD_MEM[current_t_id-1];
                        WORKLOAD_PROC[current_t_id] = WORKLOAD_PROC[current_t_id-1];
                        WORKLOAD_DISK[current_t_id] = WORKLOAD_DISK[current_t_id-1];
                        WORKLOAD_NCORES[current_t_id] = WORKLOAD_NCORES[current_t_id-1];
                        WORKLOAD_DTRANS[current_t_id] = WORKLOAD_DTRANS[current_t_id-1];
                        WORKLOAD_ETIME[current_t_id] = WORKLOAD_ETIME[current_t_id-1];
                        WORKLOAD_DTIME[current_t_id] = WORKLOAD_DTIME[current_t_id-1];
                        WORKLOAD_ATIME[current_t_id] = WORKLOAD_ATIME[current_t_id-1];

                        WORKLOAD_ID[current_t_id-1] = aux_WORKLOAD_ID;
                        WORKLOAD_MEM[current_t_id-1] = aux_WORKLOAD_MEM;
                        WORKLOAD_PROC[current_t_id-1] = aux_WORKLOAD_PROC;
                        WORKLOAD_DISK[current_t_id-1] = aux_WORKLOAD_DISK;
                        WORKLOAD_NCORES[current_t_id-1] = aux_WORKLOAD_NCORES;
                        WORKLOAD_DTRANS[current_t_id-1] = aux_WORKLOAD_DTRANS;
                        WORKLOAD_ETIME[current_t_id-1] = aux_WORKLOAD_ETIME;
                        WORKLOAD_DTIME[current_t_id-1] = aux_WORKLOAD_DTIME;
                        WORKLOAD_ATIME[current_t_id-1] = aux_WORKLOAD_ATIME;
                    }
                }
            }
        #endif

        #ifdef SUPERDEBUG
            cout << "=== CURRENT BATCH ==============================" << endl;
            cout << "=== Current time: " << CURRENT_TIME << endl;
            cout << "=== NEXT_G_WORKLOAD: " << NEXT_G_WORKLOAD << " of " << G_N_WORKLOAD << endl;
            cout << "=== N_WORKLOAD: " << N_WORKLOAD << endl;
            cout << "================================================" << endl;
            for (int i=0; i<N_WORKLOAD; i++) {
                printf("[%d] <%d> %.2f\t%.2f\t%.2f\t%d\t%.2f\t%.2f\t\t%.2f\t\t%.2f\n",
                    i, WORKLOAD_ID[i],
                    WORKLOAD_MEM[i], WORKLOAD_PROC[i],
                    WORKLOAD_DISK[i], WORKLOAD_NCORES[i],
                    WORKLOAD_DTRANS[i], WORKLOAD_ETIME[i],
                    WORKLOAD_DTIME[i], WORKLOAD_ATIME[i]);
            }
            cout << "================================================" << endl;
                for (int i=0; i<N_SCENARIO; i++) {
                    printf("[%d] %.4f\t%.4f\t%.4f\t%d\t%d\t%.2f\n", i,
                        SCENARIO_MEM[i], SCENARIO_PROC[i],
                        SCENARIO_DISK[i], SCENARIO_NCORES[i],
                        SCENARIO_TYPE[i], SCENARIO_TIME_TO_READY[i]);
                }
            cout << "================================================" << endl;
        #endif
    }

    bool Problem::all_workload_done() const {
        return (NEXT_G_WORKLOAD >= G_N_WORKLOAD);
    }

    Problem::~Problem()
    {
    }

    // Solution --------------------------------------------------------------

    Solution::Solution (const Problem& pbm):_pbm(pbm), _initialized(false)
    {
        _reset_fitness = true;
        _reset_count = 0;
    }

    const Problem& Solution::pbm() const
    {
        return _pbm;
    }

    Solution::Solution(const Solution& sol):_pbm(sol.pbm()), _initialized(sol._initialized)
    {
        *this=sol;
    }

    void Solution::print() const {
        for (int i = 0; i < N_SCENARIO; i++) {
            cout << i << "|";
            for (int j = 0; j < this->get_task_count(i); j++) {
                cout << WORKLOAD_ID[this->get_task_assign(i,j)]
                    << "+" << this->get_task_stime(this->get_task_assign(i,j))
                    << "-" << this->get_task_stime(this->get_task_assign(i,j))+WORKLOAD_ETIME[this->get_task_assign(i,j)] << " ";
            }
            cout << endl;
        }
        cout << "dd|";
        for (int i = 0; i < this->get_ondemand_task_count(); i++) {
            cout << WORKLOAD_ID[this->get_ondemand_task(i)]
                << "+" << this->get_ondemand_machine(this->get_ondemand_task(i))
                << "+" << this->get_task_stime(this->get_ondemand_task(i))
                << "-" << this->get_task_stime(this->get_ondemand_task(i))+WORKLOAD_ETIME[this->get_ondemand_task(i)] << " ";
        }
        cout << endl << "fitness = " << _fitness_prebooked + _fitness_ondemand << endl;
        cout << endl;
    }

    istream& operator>> (istream& is, Solution& sol)
    {
        cout << "istream& operator>> (istream& is, Solution& sol)";
        exit(-1);

        //for (int i=0;i<sol.pbm().dimension();i++)
        //  is >> sol._var[i];
        return is;
    }

    ostream& operator<< (ostream& os, const Solution& sol)
    {
        #ifdef DEBUG
            sol.validar();
        #endif

        //os << endl;
        for (int i = 0; i < N_SCENARIO; i++) {
            os << i << "|";
            for (int j = 0; j < sol.get_task_count(i); j++) {
                os << WORKLOAD_ID[sol.get_task_assign(i,j)] << " ";
            }
            os << endl;
        }
        os << "dd|";
        for (int i = 0; i < sol.get_ondemand_task_count(); i++) {
            os << WORKLOAD_ID[sol.get_ondemand_task(i)] << "+" << sol.get_ondemand_machine(sol.get_ondemand_task(i)) << " ";
        }
        os << endl;

        return os;
    }

    NetStream& operator << (NetStream& ns, const Solution& sol)
    {
        //cout << "NetStream& operator << (NetStream& ns, Solution& sol)" << endl;

        char *serialized = sol.to_String();

        for (int i = 0; i < sol.size(); i++) {
            ns << serialized[i];
        }

        //cout << "Sent: " << sol.size() << " bytes" << endl;
        //cout << "Sent: fitness=" << sol.fitness_readonly() << endl;
        //cout << "Sent: ready!" << endl;

        #ifdef DEBUG
            sol.validar();
        #endif

        return ns;
    }

    NetStream& operator >> (NetStream& ns, Solution& sol)
    {
        //cout << "NetStream& operator >> (NetStream& ns, Solution& sol)" << endl;

        char *serialized = sol.get_serialization();

        for (int i = 0; i < sol.size(); i++) {
            //cout << ".";
            ns >> serialized[i];
        }
        //cout << endl;
        
        //cout << "Received: " << sol.size() << " bytes" << endl;

        sol.to_Solution(serialized);

        //cout << "Received: fitness=" << sol.fitness() << endl;

        #ifdef DEBUG
            sol.validar();
            cout << "Valid!" << endl;
        #endif

        //cout << "Received: ready!" << endl;

        return ns;
    }

    char* Solution::get_serialization() {
        return _serialization;
    }

    Solution& Solution::operator= (const Solution &sol)
    {
        #ifdef DEBUG
            sol.validar();
        #endif

        this->_initialized = sol._initialized;
        this->_reset_fitness = sol._reset_fitness;
        this->_fitness_prebooked = sol._fitness_prebooked;
        this->_fitness_ondemand = sol._fitness_ondemand;

        memcpy(this->_task_count, sol._task_count, N_SCENARIO * sizeof(int));
        memcpy(this->_task_assign, sol._task_assign, MAXN_SCENARIO * MAXN_WORKLOAD * sizeof(int));
        memcpy(this->_task_stime, sol._task_stime, MAXN_WORKLOAD * sizeof(float));
        memcpy(this->_machine_assign, sol._machine_assign, MAXN_WORKLOAD * sizeof(int));

        this->_ondemand_task_count = sol._ondemand_task_count;
        memcpy(this->_ondemand_task, sol._ondemand_task, N_WORKLOAD * sizeof(int));
        memcpy(this->_ondemand_machine, sol._ondemand_machine, N_WORKLOAD * sizeof(int));

        #ifdef DEBUG
            assert(this->_ondemand_task_count == sol._ondemand_task_count);

            for (int i = 0; i < N_SCENARIO; i++) {
                assert(this->_task_count[i] == sol._task_count[i]);
            }

            for (int i = 0; i < N_SCENARIO; i++) {
                for (int j = 0; j < this->_task_count[i]; j++) {
                    assert(this->_task_assign[i][j] == sol._task_assign[i][j]);
                }
            }

            for (int j = 0; j < this->_ondemand_task_count; j++) {
                assert(this->_ondemand_task[j] == sol._ondemand_task[j]);
                assert(this->_ondemand_machine[j] == sol._ondemand_machine[j]);
            }
        #endif

        #ifdef DEBUG
            this->validar();
        #endif

        return *this;
    }

    bool Solution::operator== (const Solution& sol) const
    {
        if (sol.pbm() != _pbm) return false;
        return true;
    }

    bool Solution::operator!= (const Solution& sol) const
    {
        return !(*this == sol);
    }

    bool Solution::is_initialized() const {
        return _initialized;
    }

    void Solution::initialize()
    {
        #ifdef SUPERDEBUG
            cout << "void Solution::initialize()" << endl;
        #endif

        if (!_initialized) {
            _initialized = true;
            _reset_count = 0;

            _ondemand_task_count = 0;

            for (int m_id = 0; m_id < N_SCENARIO; m_id++) {
                _task_count[m_id] = 0;
            }

            #ifndef INIT_RAND_TASK
            int task_count = 0;
            for (int t_id = (rand01()*N_WORKLOAD); task_count < N_WORKLOAD; t_id=(t_id+1) % N_WORKLOAD) {
                task_count++;
            #else
            for (int t_id = 0; t_id < N_WORKLOAD; t_id++) {
            #endif
                #ifdef INIT_BEST_FIT
                    int c_id;
                    c_id = SCENARIO_TYPE[G_WORKLOAD_BOOKED_CHEAPEST[WORKLOAD_ID[t_id]]];

                    int m_id;
                    for (m_id = (rand01()*N_SCENARIO); SCENARIO_TYPE[m_id] != c_id; m_id=(m_id+1) % N_SCENARIO) {}
                #else
                    int m_id;
                    for (m_id = (rand01()*N_SCENARIO); 
                        !can_exec_task(m_id, t_id); 
                        m_id=(m_id+1) % N_SCENARIO) {}
                #endif

                if (_task_count[m_id] == 0) {
                    _task_stime[t_id] = SCENARIO_TIME_TO_READY[m_id];
                } else {
                    int last_t_id;
                    last_t_id = _task_assign[m_id][_task_count[m_id]-1];
                    _task_stime[t_id] = _task_stime[last_t_id] + WORKLOAD_ETIME[last_t_id];
                }

                if (_task_stime[t_id] <= WORKLOAD_DTIME[t_id]) {
                    // Es posible cumplir el deadline con esta asignación.

                    if (can_exec_task(m_id, t_id)) {
                        // Es posible cumplir con los req. de hardware.

                        _task_assign[m_id][_task_count[m_id]] = t_id;
                        _task_count[m_id]++;
                        _machine_assign[t_id] = m_id;

                        #ifdef SUPERDEBUG
                            cout << "|| added("<<m_id<<","<<_task_count[m_id]<<") t_id="<<t_id<<" st="<<_task_stime[t_id] << endl;
                        #endif
                    } else {
                        // No cumple con los req. de hardware.
                        // Va a parar a ondemand.

                        this->move_task_to_ondemand(t_id);
                    }
                } else {
                    // No puedo cumplir con el deadline.
                    // Va a parar a ondemand.

                    this->move_task_to_ondemand(t_id);
               }
            }
        }

        #ifdef DEBUG
            this->fitness();
            this->validar();

            //this->print();
            //cout << "init fitness: " << _fitness_prebooked + _fitness_ondemand << endl;
        #endif

        /*#ifdef SUPERDEBUG
            this->print();
        #endif*/
    }

    void Solution::move_task_to_ondemand(const int t_id) {
        _machine_assign[t_id] = -1;
        _task_stime[t_id] = 0;
        _ondemand_task[_ondemand_task_count] = t_id;

        int m_id = G_WORKLOAD_ONDEMAND_CHEAPEST[WORKLOAD_ID[t_id]];
        _ondemand_machine[t_id] = m_id;

        #ifdef DEBUG
            assert(can_exec_task(m_id, t_id));
        #endif

        _fitness_ondemand += WORKLOAD_ETIME[t_id] * (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[m_id]][1]);

        #ifdef DEBUG
            assert(_ondemand_machine[t_id] >= 0);
        #endif

        _ondemand_task_count++;
    }

    void Solution::force_fitness_reset() {
        _reset_fitness = true;
        this->fitness();
    }

    double Solution::fitness_readonly () const
    {
        //TODO: hacer tiempo relativo a la velocidad de la máquina.
        #ifdef DEBUG
            this->validar();
        #endif

        return _fitness_prebooked + _fitness_ondemand;
    }

    double Solution::fitness ()
    {
        if (_reset_fitness || _reset_count >= FITNESS_RESET_COUNT) {
            _reset_fitness = false;
            _reset_count = 0;

            _fitness_prebooked = compute_prebooked_fitness();
            _fitness_ondemand = compute_ondemand_fitness();
        }

        //TODO: hacer tiempo relativo a la velocidad de la máquina.
        #ifdef DEBUG
            this->validar();
        #endif

        return _fitness_prebooked + _fitness_ondemand;
    }

    double Solution::compute_prebooked_fitness() const {
        float aux_fitness_prebooked = 0.0;

        for (int j = 0; j < N_SCENARIO; j++) {
            int type_id;
            type_id = SCENARIO_TYPE[j];

            float type_cost;
            type_cost = MACH_TYPE_RAW[type_id][0];

            for (int i = 0; i < _task_count[j]; i++) {
                int t_id;
                t_id = _task_assign[j][i];

                float task_etime;
                task_etime = WORKLOAD_ETIME[t_id];

                aux_fitness_prebooked += (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-type_cost) * task_etime;
            }
        }

        return aux_fitness_prebooked;
    }

    double Solution::compute_ondemand_fitness() const {
        float aux_fitness_ondemand = 0.0;

        for (int i = 0; i < _ondemand_task_count; i++) {
            int t_id;
            t_id = _ondemand_task[i];

            float task_etime;
            task_etime = WORKLOAD_ETIME[t_id];

            int m_type;
            m_type = _ondemand_machine[t_id];

            int type_id;
            type_id = SCENARIO_TYPE[m_type];

            float type_cost;
            type_cost = MACH_TYPE_RAW[type_id][1];

            aux_fitness_ondemand += task_etime * (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-type_cost);
        }

        return aux_fitness_ondemand;
    }

    char* Solution::to_String() const
    {
        #ifdef DEBUG
            this->validar();
        #endif

        int* task_count = (int*)(_serialization);
        int* task_assignment = (int*)(task_count + SIZEOF_INT);

        int task_assignment_index = 0;
        int total_tasks = 0;
        for (int j = 0; j < N_SCENARIO; j++) {
            total_tasks += _task_count[j];

            for (int i = 0; i < _task_count[j]; i++) {
                task_assignment[task_assignment_index] = _task_assign[j][i];
                task_assignment_index++;
            }

            task_assignment[task_assignment_index] = SERIALIZATION_SEP;
            task_assignment_index++;
        }

        for (int i = 0; i < _ondemand_task_count; i++) {
            task_assignment[task_assignment_index] = _ondemand_task[i];
            task_assignment[++task_assignment_index] = _ondemand_machine[_ondemand_task[i]];
            task_assignment_index++;
        }
        total_tasks += _ondemand_task_count;
        task_count[0] = total_tasks;

        /*
        #ifdef DEBUG
            int total = 0;
            for (int i = 0; i < N_SCENARIO; i++) {
                cout << "machine_init_load[" << i << "]=" << machine_init_load[i] << endl;
                cout << "task_count[" << i << "]=" << _task_count[i] << endl;
                total += _task_count[i];
            }
            cout << "total: " << total << "(" << task_count[0] << ")" << endl;

            Solution s(*this);
            s.to_Solution((char*)_serialization);
        #endif
        * */

        return (char*)_serialization;
    }

    void Solution::to_Solution(char *_string_)
    {
        int* task_count = (int*)(_string_);
        int* task_assignment = (int*)(task_count + SIZEOF_INT);

        int total_tasks = task_count[0];

        int tasks_found = 0;
        int current_m_id = 0;
        int current_m_id_count = 0;
        int index = 0;
        #ifdef DEBUG
            //cout << "[" << current_m_id << "]>> ";
        #endif
        while (current_m_id < N_SCENARIO) {
            #ifdef DEBUG
                if (index >= N_WORKLOAD + N_SCENARIO + N_WORKLOAD) {
                    cout << "INDEX: " << index << endl;
                    cout << "current_m_id: " << current_m_id << endl;
                    cout << "current_m_id_count: " << current_m_id_count << endl;
                    cout << "tasks_found: " << tasks_found << endl;
                }
                assert(index < (N_WORKLOAD + N_SCENARIO + N_WORKLOAD));
            #endif

            if (task_assignment[index] == SERIALIZATION_SEP) {
                _task_count[current_m_id] = current_m_id_count;

                /*#ifdef DEBUG
                    cout << " (" << _task_count[current_m_id] << ")" << endl;
                #endif*/

                current_m_id_count = 0;
                current_m_id++;

                /*#ifdef DEBUG
                    cout << "[" << current_m_id << "]>> ";
                #endif*/
            } else {
                _task_assign[current_m_id][current_m_id_count] = task_assignment[index];

                if (current_m_id_count == 0) {
                    _task_stime[task_assignment[index]] = SCENARIO_TIME_TO_READY[current_m_id];
                } else {
                    _task_stime[task_assignment[index]] = _task_stime[_task_assign[current_m_id][current_m_id_count-1]] +
                        WORKLOAD_ETIME[_task_assign[current_m_id][current_m_id_count-1]];
                }

                current_m_id_count++;
                tasks_found++;

                _machine_assign[task_assignment[index]] = current_m_id;

                /*#ifdef DEBUG
                    cout << task_assignment[index] << " ";
                #endif*/
            }

            index++;
        }

        _ondemand_task_count = total_tasks - tasks_found;
        for (int i = 0; i < total_tasks-tasks_found; i++) {
            _ondemand_task[i] = task_assignment[index++];
            _ondemand_machine[_ondemand_task[i]] = task_assignment[index++];
            _task_stime[_ondemand_task[i]] = 0;
            _machine_assign[_ondemand_task[i]] = -1;
        }

        _initialized = true;
        _fitness_prebooked = compute_prebooked_fitness();
        _fitness_ondemand = compute_ondemand_fitness();

        #ifdef DEBUG
            this->validar();
        #endif
    }

    unsigned int Solution::size() const
    {
        return SERIALIZATION_SIZE;
    }

    void Solution::validar() const {
        #ifdef DEBUG
            //cout << "[DEBUG] Verifying..." << endl;

            assert(_initialized == true);

            for (int i = 0; i < N_SCENARIO; i++) {
                assert(this->_task_count[i] >= 0);
            }

            int prebooked_tasks = 0;

            for (int i = 0; i < N_SCENARIO; i++) {
                for (int j = 0; j < this->_task_count[i]; j++) {
                    assert(this->_task_assign[i][j] >= 0);
                    assert(this->_task_assign[i][j] < N_WORKLOAD);
                    assert(this->_machine_assign[this->_task_assign[i][j]] == i);
                }

                prebooked_tasks += this->_task_count[i];
            }

            for (int i = 0; i < N_SCENARIO; i++) {
                if (this->_task_count[i] > 0) {
                    #ifdef SUPERDEBUG
                        if (!AlmostEqual2sComplement(
                            this->_task_stime[this->_task_assign[i][0]],
                            SCENARIO_TIME_TO_READY[i], MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR)) {

                            cout << "))(( valid["<< i << ",0] " << this->_task_stime[this->_task_assign[i][0]] << endl;
                        }
                    #endif
                    
                    if (!AlmostEqual2sComplement(
                        this->_task_stime[this->_task_assign[i][0]],
                        SCENARIO_TIME_TO_READY[i], MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR)) {
                            
                        cout << "m_id=" << i << " stime=" << this->_task_stime[this->_task_assign[i][0]] 
                            << " scenario_start=" << SCENARIO_TIME_TO_READY[i] << endl;
                    }
                    
                    assert(AlmostEqual2sComplement(
                        this->_task_stime[this->_task_assign[i][0]],
                        SCENARIO_TIME_TO_READY[i], MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));

                    assert(can_exec_task(i, this->_task_assign[i][0]));

                    for (int j = 1; j < this->_task_count[i]; j++) {
                        #ifdef SUPERDEBUG
                            if (!AlmostEqual2sComplement(
                                this->_task_stime[this->_task_assign[i][j]],
                                this->_task_stime[this->_task_assign[i][j-1]] + WORKLOAD_ETIME[this->_task_assign[i][j-1]],
                                MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR)) {

                                cout << "))(( valid["<< i << "," << j << "] " << this->_task_stime[this->_task_assign[i][j]] << " <-> " <<
                                    this->_task_stime[this->_task_assign[i][j-1]] + WORKLOAD_ETIME[this->_task_assign[i][j-1]] << endl;
                            }
                        #endif

                        assert(AlmostEqual2sComplement(
                            this->_task_stime[this->_task_assign[i][j]],
                            this->_task_stime[this->_task_assign[i][j-1]] + WORKLOAD_ETIME[this->_task_assign[i][j-1]],
                            MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));

                        assert(can_exec_task(i, this->_task_assign[i][j]));
                    }
                }
            }

            assert(this->_ondemand_task_count >= 0);
            assert(this->_ondemand_task_count <= N_WORKLOAD);

            for (int j = 0; j < this->_ondemand_task_count; j++) {
                assert(this->_ondemand_task[j] >= 0);
                assert(this->_ondemand_task[j] < N_WORKLOAD);
                assert(this->_ondemand_machine[this->_ondemand_task[j]] >= 0);
                assert(this->_ondemand_machine[this->_ondemand_task[j]] < N_SCENARIO);
                
                if (this->_machine_assign[this->_ondemand_task[j]] != -1) {
                    cout << "[t_id=" << this->_ondemand_task[j] << "] this->_machine_assign[this->_ondemand_task[" << j <<"]] = " << this->_machine_assign[this->_ondemand_task[j]] << endl;
                }
                assert(this->_machine_assign[this->_ondemand_task[j]] == -1);
                                                
                assert(this->_task_stime[this->_ondemand_task[j]] == 0);

                assert(can_exec_task(this->_ondemand_machine[this->_ondemand_task[j]], this->_ondemand_task[j]));
            }

            if (prebooked_tasks + this->_ondemand_task_count != N_WORKLOAD) {
                this->print();
            }
            assert(prebooked_tasks + this->_ondemand_task_count == N_WORKLOAD);

            assert(AlmostEqual2sComplement(this->compute_prebooked_fitness(), _fitness_prebooked, MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));
            assert(AlmostEqual2sComplement(this->compute_ondemand_fitness(), _fitness_ondemand, MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));

            //cout << "[DEBUG] Solution verified." << endl;
        #endif
    }

    int Solution::get_task_count(const int m_id) const {
        return _task_count[m_id];
    }

    void Solution::set_task_count(const int m_id, const int value) {
        _task_count[m_id] = value;
    }

    int Solution::get_task_assign(const int m_id, const int index) const {
        return _task_assign[m_id][index];
    }

    void Solution::set_task_assign(const int m_id, const int index, const int t_id) {
        _task_assign[m_id][index] = t_id;
    }

    int Solution::get_machine_assign(const int t_id) const {
        return _machine_assign[t_id];
    }

    void Solution::set_machine_assign(const int t_id, const int m_id) {
        _machine_assign[t_id] = m_id;
    }

    int Solution::get_ondemand_task_count() const {
        return _ondemand_task_count;
    }

    void Solution::set_ondemand_task_count(const int value) {
        _ondemand_task_count = value;
    }

    int Solution::get_ondemand_task(const int index) const {
        return _ondemand_task[index];
    }

    void Solution::set_ondemand_task(const int index, const int t_id) {
        _ondemand_task[index] = t_id;
    }

    int Solution::get_ondemand_machine(const int t_id) const {
        return _ondemand_machine[t_id];
    }

    void Solution::set_ondemand_machine(const int t_id, const int m_id) {
        _ondemand_machine[t_id] = m_id;
    }

    float Solution::get_task_stime(const int t_id) const {
        return _task_stime[t_id];
    }

    void Solution::set_task_stime(const int t_id, const float value) {
        _task_stime[t_id] = value;
    }


    float Solution::get_fitness_prebooked() {
        return _fitness_prebooked;
    }

    void Solution::set_fitness_prebooked(float value) {
        _fitness_prebooked = value;
    }

    float Solution::get_fitness_ondemand() {
        return _fitness_ondemand;
    }

    void Solution::set_fitness_ondemand(float value) {
        _fitness_ondemand = value;
    }

    void Solution::local_search() {
        if (this->_reset_count++ > FITNESS_RESET_COUNT) {
            #ifdef SUPERDEBUG
                cout << "&& RESET FITNESS &&" << endl;
            #endif
            this->fitness();
        }

        #ifdef DEBUG
            this->validar();

            float prev_fitness;
            prev_fitness = this->fitness();
        #endif
        
        /*#ifdef SUPERDEBUG
            cout << "Solution::local_search()" << endl;
            this->print();
        #endif*/

        if (_initialized == 1) {
            bool move_to_ondemand;
            move_to_ondemand = false;
            
            int worst_task_id;
            worst_task_id = rand01() * N_WORKLOAD;

            float worst_profit;
            bool worst_ondemand;

            int worst_mach_id;
            worst_mach_id = _machine_assign[worst_task_id];

            if (worst_mach_id == -1) {
                worst_mach_id = _ondemand_machine[worst_task_id];

                worst_profit = (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][1])
                    * WORKLOAD_ETIME[worst_task_id];               

                worst_ondemand = true;
            } else {
                worst_profit = (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][0])
                    * WORKLOAD_ETIME[worst_task_id];

                worst_ondemand = false;
                move_to_ondemand = (MACH_TYPE_RAW[SCENARIO_TYPE[G_WORKLOAD_ONDEMAND_CHEAPEST[WORKLOAD_ID[worst_task_id]]]][1] 
                    <= MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][0]);
            }

            if (!move_to_ondemand) {
                int aux_worst_task_id, aux_worst_mach_id;
                float aux_worst_profit;

                aux_worst_task_id = worst_task_id;
                
                int ls_neigh_size;
                ls_neigh_size = N_WORKLOAD / 10; //15;
                if (ls_neigh_size < 2) ls_neigh_size = 2;

                for (int i = 0; (i < ls_neigh_size) && (!move_to_ondemand); i++) {
                    aux_worst_task_id = (aux_worst_task_id+1) % N_WORKLOAD;
                    aux_worst_mach_id = _machine_assign[aux_worst_task_id];

                    if (aux_worst_mach_id == -1) {
                        aux_worst_mach_id = _ondemand_machine[aux_worst_task_id];

                        aux_worst_profit = (G_WORKLOAD_PRICE[WORKLOAD_ID[aux_worst_task_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[aux_worst_mach_id]][1])
                            * WORKLOAD_ETIME[aux_worst_task_id];
                    } else {
                        aux_worst_profit = (G_WORKLOAD_PRICE[WORKLOAD_ID[aux_worst_task_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[aux_worst_mach_id]][0])
                            * WORKLOAD_ETIME[aux_worst_task_id];
                            
                        move_to_ondemand = (MACH_TYPE_RAW[SCENARIO_TYPE[G_WORKLOAD_ONDEMAND_CHEAPEST[WORKLOAD_ID[aux_worst_task_id]]]][1] 
                            <= MACH_TYPE_RAW[SCENARIO_TYPE[aux_worst_mach_id]][0]);
                        if (move_to_ondemand) {
                            worst_profit = aux_worst_profit;
                            worst_task_id = aux_worst_task_id;
                            worst_mach_id = aux_worst_mach_id;
                            worst_ondemand = false;
                        }
                    }

                    if (aux_worst_profit < worst_profit) {
                        worst_profit = aux_worst_profit;
                        worst_task_id = aux_worst_task_id;
                        worst_mach_id = aux_worst_mach_id;

                        worst_ondemand = (_machine_assign[aux_worst_task_id] == -1);
                    }
                }
            }

            #ifdef SUPERDEBUG
                cout << "Peor task [ONDEMAND="<< worst_ondemand << "]: worst_task_id=" << worst_task_id << " worst_mach_id=" << worst_mach_id << endl;
            #endif

            // Tengo el peor profit
            int cheapest_ondemand_m_id;
            cheapest_ondemand_m_id = G_WORKLOAD_ONDEMAND_CHEAPEST[WORKLOAD_ID[worst_task_id]];

            if ((move_to_ondemand) || 
                ((!worst_ondemand) && (MACH_TYPE_RAW[SCENARIO_TYPE[cheapest_ondemand_m_id]][1] <= MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][0]))) {

                #ifdef SUPERDEBUG
                    cout << "Muevo la tarea a una máquina ondemand." << endl;
                #endif

                // Muevo la tarea a una máquina ondemand.
                int t_pos;
                for (t_pos = 0; (_task_assign[worst_mach_id][t_pos] != worst_task_id) && (t_pos < _task_count[worst_mach_id]); t_pos++) {}

                #ifdef DEBUG
                    assert(_task_assign[worst_mach_id][t_pos] == worst_task_id);
                #endif

                float starting_time;
                starting_time = _task_stime[worst_task_id];

                this->move_task_to_ondemand(worst_task_id);

                _fitness_prebooked -= WORKLOAD_ETIME[worst_task_id] * (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]]
                    - MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][0]);

                int curr_id;
                if (_task_count[worst_mach_id] > t_pos+1) {
                    for (int curr_pos = t_pos; curr_pos+1 < _task_count[worst_mach_id]; curr_pos++) {
                        curr_id = _task_assign[worst_mach_id][curr_pos+1];

                        _task_assign[worst_mach_id][curr_pos] = curr_id;
                        _task_stime[curr_id] = starting_time;

                        starting_time += WORKLOAD_ETIME[curr_id];
                    }
                }

                _task_count[worst_mach_id]--;

                #ifdef DEBUG
                    this->validar();
                #endif

            } else {
                #ifdef SUPERDEBUG
                    cout << "Muevo la tarea a una máquina prebooked." << endl;
                #endif
                
                int dst_m_id;
                dst_m_id = rand01() * N_SCENARIO;

                bool found_best;
                found_best = false;

                int ls_neigh_size;
                ls_neigh_size = N_SCENARIO / 2; //3;
                if (ls_neigh_size < 2) ls_neigh_size = 2;

                for (int i = 0; (i < ls_neigh_size) && (!found_best); i++) {
                    if (can_exec_task(dst_m_id, worst_task_id)) {
                        if (worst_ondemand) {
                            if (MACH_TYPE_RAW[SCENARIO_TYPE[dst_m_id]][0] < MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][1]) {
                                found_best = true;
                            }
                        } else {
                            if (MACH_TYPE_RAW[SCENARIO_TYPE[dst_m_id]][0] < MACH_TYPE_RAW[SCENARIO_TYPE[worst_mach_id]][0]) {
                                found_best = true;
                            }
                        }
                    }

                    if (!found_best) {
                        dst_m_id++;
                        if (dst_m_id >= N_SCENARIO) dst_m_id = 0;
                    }
                }
                
                if (found_best) {
                    #ifdef SUPERDEBUG
                        assert(can_exec_task(dst_m_id, worst_task_id));
                    #endif
                    
                    int dst_m_type = SCENARIO_TYPE[dst_m_id];
                    float dst_stime;

                    int dst_pos = -1;
                    
                    if (_task_count[dst_m_id] > 0) {
                        if (_task_stime[_task_assign[dst_m_id][_task_count[dst_m_id]-1]]
                            + WORKLOAD_ETIME[_task_assign[dst_m_id][_task_count[dst_m_id]-1]]
                                <= WORKLOAD_DTIME[worst_task_id]) {

                            dst_pos = _task_count[dst_m_id];
                            dst_stime = _task_stime[_task_assign[dst_m_id][_task_count[dst_m_id]-1]]
                                + WORKLOAD_ETIME[_task_assign[dst_m_id][_task_count[dst_m_id]-1]];
                        } else {
                            for (dst_pos = _task_count[dst_m_id]-1; dst_pos >= 0; dst_pos--) {
                                if (_task_stime[_task_assign[dst_m_id][dst_pos]] <= WORKLOAD_DTIME[worst_task_id]) {
                                    dst_stime = _task_stime[_task_assign[dst_m_id][dst_pos]];
                                    break;
                                }
                            }
                        }
                    } else {
                        dst_stime = SCENARIO_TIME_TO_READY[dst_m_id];
                    }
                    
                    #ifdef SUPERDEBUG
                        cout << "** Muevo a dst_m_id=" << dst_m_id << " en dst_pos=" << dst_pos << " con dst_stime=" << dst_stime << endl;
                        for (int i = 0; i < _task_count[dst_m_id]; i++) {
                            cout << _task_assign[dst_m_id][i] << " ";
                        }
                        cout << endl;
                    #endif

                    if (dst_pos >= 0) {
                        // Actualizo la máquina destino.
                        if (dst_pos < _task_count[dst_m_id]) {
                            float delta_stime;
                            delta_stime = WORKLOAD_ETIME[worst_task_id];

                            int current_pos = dst_pos;
                            int current_count = _task_count[dst_m_id];

                            for (int i = dst_pos; i < current_count; i++) {
                                if (delta_stime > 0) {
                                    int dst_t_id;
                                    dst_t_id = _task_assign[dst_m_id][i];

                                    if (_task_stime[dst_t_id] + delta_stime <= WORKLOAD_DTIME[dst_t_id]) {
                                        //cout << "++ dst_t_id=" << dst_t_id << " " << this->get_task_stime(dst_t_id) << " + "
                                        //    << delta_stime << "=" << this->get_task_stime(dst_t_id) + delta_stime << endl;
                                        _task_stime[dst_t_id] += delta_stime;
                                        //cout << "++ " << this->get_task_stime(dst_t_id) << endl;

                                        if (i > current_pos) {
                                            _task_assign[dst_m_id][current_pos] = _task_assign[dst_m_id][i];
                                        }

                                        current_pos++;
                                    } else {
                                        #ifdef SUPERDEBUG
                                            cout << "-- new dst_t_id=" << dst_t_id << " stime=" << this->get_task_stime(dst_t_id) 
                                                << " to "<< this->get_task_stime(dst_t_id) + delta_stime << endl;
                                            cout << "-- moving " << dst_t_id << " to ondemand" << endl;
                                        #endif

                                        this->move_task_to_ondemand(dst_t_id);

                                        #ifdef SUPERDEBUG
                                            cout << "-- new fitness on demand=" << this->get_fitness_ondemand() << endl;
                                            cout << "-- new delta=" << delta_stime;
                                        #endif

                                        delta_stime -= WORKLOAD_ETIME[dst_t_id];

                                        #ifdef SUPERDEBUG
                                            cout << " to " << delta_stime << endl;
                                        #endif

                                        _task_count[dst_m_id]--;
                                        _fitness_prebooked -= (G_WORKLOAD_PRICE[WORKLOAD_ID[dst_t_id]]-
                                            MACH_TYPE_RAW[dst_m_type][0]) * WORKLOAD_ETIME[dst_t_id];
                                    }
                                } else {
                                    if (i > current_pos) {
                                        _task_assign[dst_m_id][current_pos] = _task_assign[dst_m_id][i];
                                        _task_stime[_task_assign[dst_m_id][current_pos]] += delta_stime;
                                    }

                                    current_pos++;
                                }
                            }

                            for (int i = _task_count[dst_m_id]-1; i >= dst_pos; i--) {
                                _task_assign[dst_m_id][i+1] = _task_assign[dst_m_id][i];
                            }
                        }

                        // Actualizo la tarea movida.
                        _task_stime[worst_task_id] = dst_stime;
                        _task_assign[dst_m_id][dst_pos] = worst_task_id;
                        _machine_assign[worst_task_id] = dst_m_id;
                        _task_count[dst_m_id]++;
                        
                        #ifdef SUPERDEBUG
                            cout << "_task_assign[" << dst_m_id << "][" << dst_pos << "] = " << worst_task_id << endl;
                            cout << "_machine_assign[" << worst_task_id << "] = " << dst_m_id << endl;
                        #endif

                        // Actualizo el fitness de la máquina destino.
                        _fitness_prebooked += (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]]-
                            MACH_TYPE_RAW[dst_m_type][0]) * WORKLOAD_ETIME[worst_task_id];

                        if (worst_ondemand == 1) {
                            // El peor esta ondemand
                            
                            #ifdef SUPERDEBUG
                                cout << "El peor esta ondemand." << endl;
                            #endif

                            int m_type;
                            float m_type_cost;
                            int t_pos = -1;

                            for (int i = 0; i < _ondemand_task_count; i++) {
                                if (_ondemand_task[i] == worst_task_id) {
                                    t_pos = i;

                                    m_type = SCENARIO_TYPE[_ondemand_machine[worst_task_id]];
                                    m_type_cost = MACH_TYPE_RAW[m_type][1];
                                } else {
                                    if (t_pos >= 0) {
                                        _ondemand_task[i-1] = _ondemand_task[i];
                                    }
                                }
                            }

                            _ondemand_task_count--;

                            // Actualizo el fitness de la máquina origen.
                            if (_ondemand_task_count > 0) {
                                _fitness_ondemand -= (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]]-m_type_cost) * WORKLOAD_ETIME[worst_task_id];
                            } else {
                                _fitness_ondemand = 0;
                            }
                        } else {
                            // El peor esta prebooked
                            
                            #ifdef SUPERDEBUG
                                cout << "El peor esta prebooked." << endl;
                            #endif
                            
                            int m_type;
                            m_type = SCENARIO_TYPE[worst_mach_id];

                            // Actualizo el fitness de la máquina origen.
                            _fitness_prebooked -= (G_WORKLOAD_PRICE[WORKLOAD_ID[worst_task_id]]-MACH_TYPE_RAW[m_type][0]) * WORKLOAD_ETIME[worst_task_id];

                            int t_pos = -1;

                            for (int i = 0; i < _task_count[worst_mach_id]; i++) {
                                if (_task_assign[worst_mach_id][i] == worst_task_id) {
                                    t_pos = i;
                                } else {
                                    if (t_pos >= 0) {
                                        _task_assign[worst_mach_id][i-1] = _task_assign[worst_mach_id][i];

                                        if (i-2 >= 0) {
                                            int prev_prev_id;
                                            prev_prev_id = _task_assign[worst_mach_id][i-2];

                                            _task_stime[_task_assign[worst_mach_id][i]] =
                                                _task_stime[prev_prev_id] + WORKLOAD_ETIME[prev_prev_id];
                                        } else {
                                            _task_stime[_task_assign[worst_mach_id][i]] = SCENARIO_TIME_TO_READY[worst_mach_id];
                                        }
                                    }
                                }
                            }

                            _task_count[worst_mach_id]--;
                        }
                        
                        //this->print();
                    }
                }
            }

            #ifdef SUPERDEBUG
                if (prev_fitness < this->fitness()) {
                    cout << "!!! LOCAL SEARCH [SUCCESS]" << endl;
                    cout << "!!! From " << prev_fitness << " to " << this->fitness() << " (" << this->fitness() - prev_fitness << ")" << endl;
                }
            #endif
        }

        #ifdef DEBUG
            this->validar();
        #endif
    }

    void Solution::mutate(float probability) {
        if (this->_reset_count++ > FITNESS_RESET_COUNT) {
            #ifdef SUPERDEBUG
                cout << "&& RESET FITNESS &&" << endl;
            #endif
            this->fitness();
        }

        #ifdef DEBUG
            this->validar();

            float prev_fitness;
            prev_fitness = this->fitness();
        #endif

        /*
        int t_id;
        t_id = rand01() * N_WORKLOAD;
        if (t_id >= N_WORKLOAD) t_id = N_WORKLOAD-1;

        {
            {*/
        for (int t_id = 0; t_id < N_WORKLOAD; t_id++) {
            if (rand01() <= probability) {

                #ifdef SUPERDEBUG
                    cout << "=== MUTATE ===============================" << endl;
                #endif

                // La tarea t_id salió sorteada para ser movida.
                int m_id;
                m_id = this->get_machine_assign(t_id);

                // Sorteo una máquina para el movimiento.
                int dst_m_id;
                dst_m_id = rand01() * (N_SCENARIO+1);

                if (m_id == dst_m_id) {
                    #ifdef SUPERDEBUG
                        cout << "[MOVEMENT] =====================================================================" << endl;
                        cout << "pre-viejo fitness prebooked=" << this->get_fitness_prebooked() << endl;
                        cout << "pre-viejo fitness ondemand=" << this->get_fitness_ondemand() << endl;
                        cout << "Moviendo en la misma máquina..." << endl;
                    #endif

                    int dst_pos;
                    dst_pos = rand01() * this->get_task_count(dst_m_id);
                    assert(dst_pos < this->get_task_count(dst_m_id));

                    int t_pos;
                    for (t_pos = 0; this->get_task_assign(m_id, t_pos) != t_id; t_pos++) {}

                    // Verifico que el movimiento cumpla el deadline de la tarea t_id.
                    float dst_stime;
                    if (dst_pos == 0) {
                        dst_stime = SCENARIO_TIME_TO_READY[m_id];
                    } else {
                        if (t_pos > dst_pos) {
                            dst_stime = this->get_task_stime(this->get_task_assign(dst_m_id, dst_pos));
                        } else if (t_pos < dst_pos) {
                            int dst_t_id;
                            dst_t_id  = this->get_task_assign(dst_m_id, dst_pos);
                            dst_stime = this->get_task_stime(dst_t_id) + WORKLOAD_ETIME[dst_t_id] - WORKLOAD_ETIME[t_id];
                        } else {
                            // nada que hacer
                            dst_stime = 0;
                        }
                    }

                    if (dst_stime <= WORKLOAD_DTIME[t_id]) {
                        if (t_pos == dst_pos) {
                            // Nada que hacer...
                        } else if (t_pos > dst_pos) {
                            int m_type;
                            m_type = SCENARIO_TYPE[m_id];

                            #ifdef SUPERDEBUG
                                cout << "~~ moviendo t_id=" << t_id << " desde t_pos=" << t_pos << " a dst_pos=" << dst_pos << endl;

                                cout << "[" << m_id << "] ";
                                for (int i = 0; i < this->get_task_count(m_id); i++) {
                                    cout << this->get_task_assign(m_id, i) << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                                }
                                cout << endl;
                            #endif

                            for (int i = t_pos; i > dst_pos; i--) {
                                this->set_task_assign(dst_m_id, i, this->get_task_assign(dst_m_id, i-1));
                            }
                            this->set_task_assign(dst_m_id, dst_pos, t_id);
                            this->set_task_stime(t_id, dst_stime);

                            float delta_stime;
                            delta_stime = WORKLOAD_ETIME[t_id];

                            int current_pos = dst_pos+1;
                            int current_count = this->get_task_count(m_id);

                            for (int i = dst_pos+1; i < current_count; i++) {
                                int dst_t_id;
                                dst_t_id = this->get_task_assign(m_id, i);

                                if (i <= t_pos) {
                                    #ifdef SUPERDEBUG
                                        cout << "-- i <= t_pos" << endl;
                                    #endif
                                    if (this->get_task_stime(dst_t_id) + delta_stime <= WORKLOAD_DTIME[dst_t_id]) {
                                        this->set_task_stime(dst_t_id, this->get_task_stime(dst_t_id) + delta_stime);

                                        if (i > current_pos) {
                                            this->set_task_assign(m_id, current_pos, this->get_task_assign(m_id,i));
                                        }

                                        current_pos++;
                                    } else {
                                        #ifdef SUPERDEBUG
                                            cout << "-- moving dst_t_id=" << dst_t_id << " to ondemand" << endl;
                                        #endif
                                        this->move_task_to_ondemand(dst_t_id);

                                        delta_stime -= WORKLOAD_ETIME[dst_t_id];

                                        this->set_task_count(m_id, this->get_task_count(m_id) - 1);
                                        this->set_fitness_prebooked(this->get_fitness_prebooked() -
                                            (G_WORKLOAD_PRICE[WORKLOAD_ID[dst_t_id]]-MACH_TYPE_RAW[m_type][0]) * WORKLOAD_ETIME[dst_t_id]);
                                    }
                                } else if (i > t_pos) {
                                    #ifdef SUPERDEBUG
                                        cout << "-- i > t_pos" << endl;
                                    #endif
                                    //if (delta_stime < 0) {
                                        this->set_task_stime(dst_t_id,
                                            this->get_task_stime(this->get_task_assign(m_id, current_pos-1)) +
                                                WORKLOAD_ETIME[this->get_task_assign(m_id, current_pos-1)]);
                                    //}

                                    if (i > current_pos) {
                                        this->set_task_assign(m_id, current_pos, this->get_task_assign(m_id,i));
                                    }

                                    current_pos++;
                                }
                            }
                        } else if (t_pos < dst_pos) {
                            #ifdef SUPERDEBUG
                                cout << "~~ moviento t_id=" << t_id << " desde t_pos=" << t_pos << " a dst_pos=" << dst_pos << endl;

                                cout << "[" << m_id << "] ";
                                for (int i = 0; i < this->get_task_count(m_id); i++) {
                                    cout << this->get_task_assign(m_id, i)  << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                                }
                                cout << endl;
                            #endif

                            for (int i = t_pos; i < dst_pos; i++) {
                                this->set_task_assign(m_id, i, this->get_task_assign(m_id, i+1));

                                if (i == 0) {
                                    this->set_task_stime(this->get_task_assign(m_id, i), SCENARIO_TIME_TO_READY[m_id]);
                                } else {
                                    this->set_task_stime(
                                        this->get_task_assign(m_id, i),
                                        this->get_task_stime(this->get_task_assign(m_id, i-1)) + WORKLOAD_ETIME[this->get_task_assign(m_id, i-1)]);
                                }
                            }
                            this->set_task_assign(m_id, dst_pos, t_id);
                            this->set_task_stime(t_id, dst_stime);

                            #ifdef SUPERDEBUG
                                cout << "[" << m_id << "] ";
                                for (int i = 0; i < this->get_task_count(m_id); i++) {
                                    cout << this->get_task_assign(m_id, i) << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                                }
                                cout << endl;
                            #endif
                        }

                        #ifdef SUPERDEBUG
                            cout << "new [" << m_id << "] ";
                            for (int i = 0; i < this->get_task_count(m_id); i++) {
                                cout << this->get_task_assign(m_id, i)  << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                            }
                            cout << endl;

                            cout << "nuevo fitness prebooked=" << this->get_fitness_prebooked() << endl;
                            cout << "nuevo fitness ondemand=" << this->get_fitness_ondemand() << endl;

                            cout << "(debería ser) fitness prebooked=" << this->compute_prebooked_fitness() << endl;
                            cout << "(debería ser) fitness ondemand=" << this->compute_ondemand_fitness() << endl;
                        #endif

                        #ifdef DEBUG
                            this->validar();
                        #endif

                        _reset_count++;

                        #ifdef SUPERDEBUG
                            cout << "[MOVEMENT] =====================================================================" << endl;
                        #endif
                    }
                } else {
                    if (dst_m_id < N_SCENARIO) {
                        // Verifico que el movimiento cumpla los req. de hardware de la tarea t_id.
                        if (can_exec_task(dst_m_id, t_id)) {
                            // Sorteo una posición en la máquina destino.
                            int dst_pos;
                            dst_pos = rand01() * (this->get_task_count(dst_m_id)+1);
                            if (dst_pos > this->get_task_count(dst_m_id)) dst_pos = this->get_task_count(dst_m_id);

                            // Verifico que el movimiento cumpla el deadline de la tarea t_id.
                            float dst_stime;
                            if (dst_pos == 0) {
                                dst_stime = SCENARIO_TIME_TO_READY[dst_m_id];
                            } else if (dst_pos == this->get_task_count(dst_m_id)) {
                                dst_stime = this->get_task_stime(this->get_task_assign(dst_m_id,dst_pos-1))
                                    + WORKLOAD_ETIME[this->get_task_assign(dst_m_id,dst_pos-1)];
                            } else {
                                dst_stime = this->get_task_stime(this->get_task_assign(dst_m_id,dst_pos));
                            }

                            if (dst_stime <= WORKLOAD_DTIME[t_id]) {
                                int dst_m_type;
                                dst_m_type = SCENARIO_TYPE[dst_m_id];

                                #ifdef SUPERDEBUG
                                    cout << "[MOVEMENT] =====================================================================" << endl;
                                    cout << "pre-viejo fitness prebooked=" << this->get_fitness_prebooked() << endl;
                                    cout << "pre-viejo fitness ondemand=" << this->get_fitness_ondemand() << endl;

                                    cout << "Moviendo t_id=" << t_id << " a dst_m_id=" << dst_m_id << " con dst_pos=" << dst_pos << endl;
                                    cout << "[" << dst_m_id << "] ";
                                    for (int i = 0; i < this->get_task_count(dst_m_id); i++) {
                                        cout << this->get_task_assign(dst_m_id, i) << "|" << this->get_task_stime(this->get_task_assign(dst_m_id, i)) << " ";
                                    }
                                    cout << endl;
                                    cout << "viejo fitness prebooked=" << this->get_fitness_prebooked() << endl;
                                    cout << "viejo fitness ondemand=" << this->get_fitness_ondemand() << endl;
                                #endif

                                // Actualizo la máquina destino.
                                if (dst_pos < this->get_task_count(dst_m_id)) {
                                    float delta_stime;
                                    delta_stime = WORKLOAD_ETIME[t_id];

                                    int current_pos = dst_pos;
                                    int current_count = this->get_task_count(dst_m_id);

                                    for (int i = dst_pos; i < current_count; i++) {
                                        if (delta_stime > 0) {
                                            int dst_t_id;
                                            dst_t_id = this->get_task_assign(dst_m_id,i);

                                            if (this->get_task_stime(dst_t_id) + delta_stime <= WORKLOAD_DTIME[dst_t_id]) {
                                                //cout << "++ dst_t_id=" << dst_t_id << " " << this->get_task_stime(dst_t_id) << " + "
                                                //    << delta_stime << "=" << this->get_task_stime(dst_t_id) + delta_stime << endl;
                                                this->set_task_stime(dst_t_id, this->get_task_stime(dst_t_id) + delta_stime);
                                                //cout << "++ " << this->get_task_stime(dst_t_id) << endl;

                                                if (i > current_pos) {
                                                    this->set_task_assign(dst_m_id, current_pos, this->get_task_assign(dst_m_id,i));
                                                }

                                                current_pos++;
                                            } else {
                                                #ifdef SUPERDEBUG
                                                    cout << "-- new dst_t_id=" << dst_t_id << " stime=" << this->get_task_stime(dst_t_id) << " to "<< this->get_task_stime(dst_t_id) + delta_stime << endl;
                                                    cout << "-- moving " << dst_t_id << " to ondemand" << endl;
                                                #endif

                                                this->move_task_to_ondemand(dst_t_id);

                                                #ifdef SUPERDEBUG
                                                    cout << "-- new fitness on demand=" << this->get_fitness_ondemand() << endl;
                                                    cout << "-- new delta=" << delta_stime;
                                                #endif

                                                delta_stime -= WORKLOAD_ETIME[dst_t_id];

                                                #ifdef SUPERDEBUG
                                                    cout << " to " << delta_stime << endl;
                                                #endif

                                                this->set_task_count(dst_m_id, this->get_task_count(dst_m_id) - 1);
                                                this->set_fitness_prebooked(this->get_fitness_prebooked() -
                                                    (G_WORKLOAD_PRICE[WORKLOAD_ID[dst_t_id]]-MACH_TYPE_RAW[dst_m_type][0]) * WORKLOAD_ETIME[dst_t_id]);
                                            }
                                        } else {
                                            if (i > current_pos) {
                                                this->set_task_assign(dst_m_id, current_pos, this->get_task_assign(dst_m_id, i));
                                                this->set_task_stime(this->get_task_assign(dst_m_id, current_pos),
                                                    this->get_task_stime(this->get_task_assign(dst_m_id, current_pos)) + delta_stime);
                                            }

                                            current_pos++;
                                        }
                                    }

                                    for (int i = this->get_task_count(dst_m_id)-1; i >= dst_pos; i--) {
                                        this->set_task_assign(dst_m_id, i+1, this->get_task_assign(dst_m_id, i));
                                    }
                                }

                                // Actualizo la tarea movida.
                                this->set_task_stime(t_id, dst_stime);
                                this->set_task_assign(dst_m_id, dst_pos, t_id);
                                this->set_machine_assign(t_id, dst_m_id);
                                this->set_task_count(dst_m_id, this->get_task_count(dst_m_id)+1);

                                // Actualizo el fitness de la máquina destino.
                                this->set_fitness_prebooked(this->get_fitness_prebooked() +
                                    (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[dst_m_type][0]) * WORKLOAD_ETIME[t_id]);

                                /*#ifdef SUPERDEBUG
                                    cout << "add fitness=" << (G_WORKLOAD_BOOKED_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[dst_m_type][0]) * WORKLOAD_ETIME[t_id]
                                        << "([" << G_WORKLOAD_BOOKED_PRICE[WORKLOAD_ID[t_id]] << "-" << MACH_TYPE_RAW[dst_m_type][0] << "] x" << WORKLOAD_ETIME[t_id] << ")" << endl;
                                    cout << "new [" << dst_m_id << "] ";
                                    for (int i = 0; i < this->get_task_count(dst_m_id); i++) {
                                        cout << this->get_task_assign(dst_m_id, i) << "|" << this->get_task_stime(this->get_task_assign(dst_m_id, i)) << " ";
                                    }
                                    cout << endl;

                                    cout << "nuevo fitness prebooked=" << this->get_fitness_prebooked() << endl;
                                    cout << "nuevo fitness ondemand=" << this->get_fitness_ondemand() << endl;
                                #endif*/

                                // Actualizo la máquina origen.
                                if (m_id >= 0) {
                                    // Tenía una máquina prebooked.

                                    #ifdef SUPERDEBUG
                                        cout << "tenía una máquina prebooked m_id=" << m_id << endl;

                                        cout << "[" << m_id << "] ";
                                        for (int i = 0; i < this->get_task_count(m_id); i++) {
                                            cout << this->get_task_assign(m_id, i) << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                                        }
                                        cout << endl;
                                    #endif

                                    int m_type;
                                    m_type = SCENARIO_TYPE[m_id];

                                    // Actualizo el fitness de la máquina origen.
                                    this->set_fitness_prebooked(this->get_fitness_prebooked() -
                                        (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[m_type][0]) * WORKLOAD_ETIME[t_id]);

                                    int t_pos = -1;

                                    for (int i = 0; i < this->get_task_count(m_id); i++) {
                                        if (this->get_task_assign(m_id,i) == t_id) {
                                            t_pos = i;
                                        } else {
                                            if (t_pos >= 0) {
                                                #ifdef SUPERDEBUG
                                                    cout << "%% move " << i << " to " << i-1 << endl;
                                                #endif
                                                this->set_task_assign(m_id, i-1, this->get_task_assign(m_id, i));

                                                if (i-2 >= 0) {
                                                    int prev_prev_id;
                                                    prev_prev_id = this->get_task_assign(m_id, i-2);

                                                    this->set_task_stime(this->get_task_assign(m_id, i),
                                                        this->get_task_stime(prev_prev_id) + WORKLOAD_ETIME[prev_prev_id]);

                                                    #ifdef SUPERDEBUG
                                                        cout << "%% stime of " << i-2 << "(" << prev_prev_id << ") is "
                                                            << this->get_task_stime(prev_prev_id) << " + " << WORKLOAD_ETIME[prev_prev_id]
                                                            << " = " << this->get_task_stime(prev_prev_id) + WORKLOAD_ETIME[prev_prev_id] << endl;
                                                    #endif
                                                } else {
                                                    this->set_task_stime(this->get_task_assign(m_id, i), SCENARIO_TIME_TO_READY[m_id]);
                                                }
                                            }
                                        }
                                    }

                                    this->set_task_count(m_id, this->get_task_count(m_id) - 1);

                                    #ifdef SUPERDEBUG
                                        cout << "new [" << m_id << "] ";
                                        for (int i = 0; i < this->get_task_count(m_id); i++) {
                                            cout << this->get_task_assign(m_id, i) << "|" << this->get_task_stime(this->get_task_assign(m_id, i)) << " ";
                                        }
                                        cout << endl;
                                    #endif
                                } else {
                                    // Tenía una máquina ondemand.
                                    #ifdef SUPERDEBUG
                                        cout << "** tenía una máquina ondemand" << endl;
                                    #endif

                                    int m_type;
                                    float m_type_cost, m_data_cost;
                                    int t_pos = -1;

                                    for (int i = 0; i < this->get_ondemand_task_count(); i++) {
                                        if (this->get_ondemand_task(i) == t_id) {
                                            t_pos = i;

                                            m_type = SCENARIO_TYPE[this->get_ondemand_machine(t_id)];
                                            m_type_cost = MACH_TYPE_RAW[m_type][1];
                                        } else {
                                            if (t_pos >= 0) {
                                                this->set_ondemand_task(i-1, this->get_ondemand_task(i));
                                            }
                                        }
                                    }

                                    #ifdef SUPERDEBUG
                                        cout << "** " << m_type_cost * WORKLOAD_ETIME[t_id] << "=" << m_type_cost << "x" << WORKLOAD_ETIME[t_id] << endl;
                                        cout << "** old ondemand fitness=" << this->get_fitness_ondemand() << endl;
                                    #endif

                                    this->set_ondemand_task_count(this->get_ondemand_task_count() - 1);

                                    // Actualizo el fitness de la máquina origen.
                                    if (this->get_ondemand_task_count() > 0) {
                                        this->set_fitness_ondemand(this->get_fitness_ondemand() -
                                            (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-m_type_cost) * WORKLOAD_ETIME[t_id]);
                                    } else {
                                        this->set_fitness_ondemand(0);
                                    }
                                    #ifdef SUPERDEBUG
                                        cout << "** new ondemand fitness=" << this->get_fitness_ondemand() << endl;
                                    #endif
                                }

                                #ifdef SUPERDEBUG
                                    cout << "(debería ser) fitness prebooked=" << this->compute_prebooked_fitness() << endl;
                                    cout << "(debería ser) fitness ondemand=" << this->compute_ondemand_fitness() << endl;
                                #endif

                                #ifdef DEBUG
                                    this->validar();
                                #endif

                                _reset_count++;

                                #ifdef SUPERDEBUG
                                    cout << "[MOVEMENT] =====================================================================" << endl;
                                #endif
                            } else {
                                // No pude satisfacer el deadline.
                                // Ignoro el movimiento.
                            }
                        } else {
                            // No pude satisfacer los req. de hardware.
                            // Ignoro el movimiento.
                        }
                    } else {
                        if (m_id != -1) {
                            // Muevo la tarea a una máquina ondemand.
                            int t_pos;
                            for (t_pos = 0; (_task_assign[m_id][t_pos] != t_id) && (t_pos < _task_count[m_id]); t_pos++) {}

                            #ifdef DEBUG
                                assert(_task_assign[m_id][t_pos] == t_id);
                            #endif

                            float starting_time;
                            starting_time = _task_stime[t_id];

                            this->move_task_to_ondemand(t_id);

                            _fitness_prebooked -= WORKLOAD_ETIME[t_id] * (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[m_id]][0]);

                            int curr_id;
                            if (_task_count[m_id] > t_pos+1) {
                                for (int curr_pos = t_pos; curr_pos+1 < _task_count[m_id]; curr_pos++) {
                                    curr_id = _task_assign[m_id][curr_pos+1];

                                    _task_assign[m_id][curr_pos] = curr_id;
                                    _task_stime[curr_id] = starting_time;

                                    starting_time += WORKLOAD_ETIME[curr_id];
                                }
                            }

                            _task_count[m_id]--;
                        }
                    }
                }

                #ifdef SUPERDEBUG
                    if (prev_fitness < this->fitness()) {
                        cout << "!!! MUTATE [SUCCESS]" << endl;
                        cout << "!!! From " << prev_fitness << " to " << this->fitness() << " (" << this->fitness() - prev_fitness << ")" << endl;
                    }
                #endif
            }

            #ifdef DEBUG
                this->validar();
            #endif
        }
    }

    int Solution::get_ondemand_task_id_pos(const int t_id) const {
        for (int i = 0; i < _ondemand_task_count; i++) {
            if (_ondemand_task[i] == t_id) {
                return i;
            }
        }

        return -1;
    }

    void Solution::crossover(const int t_id_bottom, const int t_id_top, const Solution &model) {
        if (this->_reset_count++ > FITNESS_RESET_COUNT) {
            #ifdef SUPERDEBUG
                cout << "&& RESET FITNESS &&" << endl;
            #endif
            this->fitness();
        }

        #ifdef DEBUG
            this->validar();
        #endif

        int model_m_id;
        int m_id;

        for (int t_id = t_id_bottom; t_id <= t_id_top; t_id++) {
            model_m_id = model._machine_assign[t_id];
            m_id = _machine_assign[t_id];

            if ((model_m_id != -1) && (m_id == -1)) {
                int t_count;
                t_count = _task_count[model_m_id];

                if (t_count == 0) {

                } else {
                    bool ready;
                    ready = false;

                    for (int i = t_count; (i >= 0) && !ready; i--) {
                        float stime;

                        if (i == t_count) {
                            stime = _task_stime[_task_assign[model_m_id][i-1]] + WORKLOAD_ETIME[_task_assign[model_m_id][i-1]];
                        } else {
                            stime = _task_stime[_task_assign[model_m_id][i]];
                        }

                        if (stime <= WORKLOAD_DTIME[t_id]) {
                            // Borro t_id de ondemand
                            int ondemand_pos = get_ondemand_task_id_pos(t_id);

                            int c_id;
                            c_id = SCENARIO_TYPE[this->_ondemand_machine[t_id]];

                            float ondemand_delta;
                            ondemand_delta = (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[c_id][1]) * WORKLOAD_ETIME[t_id];

                            _fitness_ondemand -= ondemand_delta;

                            if (ondemand_pos < _ondemand_task_count-1) {
                                _ondemand_task[ondemand_pos] = _ondemand_task[_ondemand_task_count-1];
                            }

                            _ondemand_task_count--;

                            // Agrego t_id a model_m_id
                            if (i == t_count) {
                                #ifdef SUPERDEBUG
                                    cout << "++ t_id quedó último! pos=" << i << endl;
                                    cout << "++ stime=" << stime << endl;
                                    cout << "++ scenario ready=" << SCENARIO_TIME_TO_READY[model_m_id] << endl;

                                    cout << "old [" << model_m_id << "] ";
                                    for (int ii = 0; ii < this->get_task_count(model_m_id); ii++) {
                                        cout << this->get_task_assign(model_m_id, ii) << "|" << this->get_task_stime(this->get_task_assign(model_m_id, ii)) << " ";
                                    }
                                    cout << endl;
                                #endif

                                _task_assign[model_m_id][t_count] = t_id;
                                _machine_assign[t_id] = model_m_id;
                                _task_stime[t_id] = stime;
                                _task_count[model_m_id]++;

                                float prebooked_delta;
                                prebooked_delta = (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[SCENARIO_TYPE[model_m_id]][0]) * WORKLOAD_ETIME[t_id];

                                _fitness_prebooked += prebooked_delta;

                                #ifdef SUPERDEBUG
                                    cout << "new [" << model_m_id << "] ";
                                    for (int ii = 0; ii < this->get_task_count(model_m_id); ii++) {
                                        cout << this->get_task_assign(model_m_id, ii) << "|" << this->get_task_stime(this->get_task_assign(model_m_id, ii)) << " ";
                                    }
                                    cout << endl;
                                #endif
                            } else {
                                #ifdef SUPERDEBUG
                                    cout << "=================================================" << endl;
                                    cout << "++ t_id quedó en la pos=" << i << endl;
                                    cout << "++ stime=" << stime << endl;
                                    cout << "++ scenario ready=" << SCENARIO_TIME_TO_READY[model_m_id] << endl;


                                    cout << "old [" << model_m_id << "] ";
                                    for (int ii = 0; ii < this->get_task_count(model_m_id); ii++) {
                                        cout << this->get_task_assign(model_m_id, ii)
                                            << "|" << this->get_task_stime(this->get_task_assign(model_m_id, ii))
                                            << "|" << WORKLOAD_ETIME[this->get_task_assign(model_m_id, ii)] << " ";
                                    }
                                    cout << endl;
                                #endif

                                float delta_time;
                                delta_time = WORKLOAD_ETIME[t_id];

                                #ifdef SUPERDEBUG
                                    cout << "++ delta_time=" << delta_time << endl;
                                #endif

                                int j_t_id;
                                int j_current;
                                j_current = i;

                                for (int j = i; j < t_count; j++) {
                                    j_t_id = _task_assign[model_m_id][j];

                                    if (_task_stime[j_t_id] + delta_time > WORKLOAD_DTIME[j_t_id]) {
                                        #ifdef SUPERDEBUG
                                            cout << "++ old delta_time =" << delta_time << "-" << WORKLOAD_ETIME[j_t_id] << endl;
                                        #endif
                                        delta_time -= WORKLOAD_ETIME[j_t_id];
                                        /*#ifdef SUPERDEBUG
                                            cout << "++ new delta_time=" << delta_time << endl;
                                            cout << "++ moviendo " << j_t_id << " a ondemand" << endl;
                                            cout << "++ _fitness_prebooked -= [" << G_WORKLOAD_BOOKED_PRICE[WORKLOAD_ID[j_t_id]] << "-" << MACH_TYPE_RAW[SCENARIO_TYPE[model_m_id]][0] << "] x" << WORKLOAD_ETIME[j_t_id]
                                                << "=" << (G_WORKLOAD_BOOKED_PRICE[WORKLOAD_ID[j_t_id]]-MACH_TYPE_RAW[SCENARIO_TYPE[model_m_id]][0]) * WORKLOAD_ETIME[j_t_id] << endl;
                                        #endif*/

                                        move_task_to_ondemand(j_t_id);
                                        _fitness_prebooked -= (G_WORKLOAD_PRICE[WORKLOAD_ID[j_t_id]]-MACH_TYPE_RAW[SCENARIO_TYPE[model_m_id]][0])
                                            * WORKLOAD_ETIME[j_t_id];


                                        _task_count[model_m_id]--;
                                    } else {
                                        _task_assign[model_m_id][j_current] = j_t_id;
                                        _task_stime[_task_assign[model_m_id][j_current]] = _task_stime[j_t_id] + delta_time;

                                        j_current++;
                                    }
                                }

                                #ifdef SUPERDEBUG
                                    cout << "new [" << model_m_id << "] ";
                                    for (int ii = 0; ii < this->get_task_count(model_m_id); ii++) {
                                        cout << this->get_task_assign(model_m_id, ii) << "|" << this->get_task_stime(this->get_task_assign(model_m_id, ii)) << " ";
                                    }
                                    cout << endl;

                                    cout << "++ _fitness_prebooked=" << _fitness_prebooked << endl;
                                    cout << "++ (debería ser) _fitness_prebooked=" << compute_prebooked_fitness() << endl;
                                #endif

                                t_count = _task_count[model_m_id];
                                for (int j = t_count-1; j >= i; j--) {
                                    _task_assign[model_m_id][j+1] = _task_assign[model_m_id][j];
                                }
                                _task_assign[model_m_id][i] = t_id;
                                _machine_assign[t_id] = model_m_id;
                                _task_stime[t_id] = stime;
                                _task_count[model_m_id]++;

                                float prebooked_delta;
                                prebooked_delta = (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]]-MACH_TYPE_RAW[SCENARIO_TYPE[model_m_id]][0]) * WORKLOAD_ETIME[t_id];

                                _fitness_prebooked += prebooked_delta;
                            }

                            ready = true;
                        }
                    }
                }

                #ifdef DEBUG
                    this->validar();
                #endif
            } else if ((model_m_id == -1) && (m_id != -1)) {
                // Muevo la tarea a una máquina ondemand.
                int t_pos;
                for (t_pos = 0; (_task_assign[m_id][t_pos] != t_id) && (t_pos < _task_count[m_id]); t_pos++) {}

                #ifdef DEBUG
                    assert(_task_assign[m_id][t_pos] == t_id);
                #endif

                float starting_time;
                starting_time = _task_stime[t_id];

                this->move_task_to_ondemand(t_id);

                _fitness_prebooked -= WORKLOAD_ETIME[t_id] * (G_WORKLOAD_PRICE[WORKLOAD_ID[t_id]] - MACH_TYPE_RAW[SCENARIO_TYPE[m_id]][0]);

                int curr_id;
                if (_task_count[m_id] > t_pos+1) {
                    for (int curr_pos = t_pos; curr_pos+1 < _task_count[m_id]; curr_pos++) {
                        curr_id = _task_assign[m_id][curr_pos+1];

                        _task_assign[m_id][curr_pos] = curr_id;
                        _task_stime[curr_id] = starting_time;

                        starting_time += WORKLOAD_ETIME[curr_id];
                    }
                }

                _task_count[m_id]--;

                #ifdef DEBUG
                    this->validar();
                #endif
            }
        }

        #ifdef DEBUG
            this->validar();
        #endif
    }

    Solution::~Solution()
    {}

// Intra_operator  --------------------------------------------------------------

    Intra_Operator::Intra_Operator(const unsigned int _number_op):_number_operator(_number_op),probability(NULL)
    {}

    unsigned int Intra_Operator::number_operator() const
    {
        return _number_operator;
    }

    Intra_Operator *Intra_Operator::create(const unsigned int _number_op)
    {
        switch (_number_op)
        {
            case 0: return new Crossover();break;
            case 1: return new Mutation();break;
            case 2: return new Improve();break;
        }
    }

    ostream& operator<< (ostream& os, const Intra_Operator& intra)
    {
        switch (intra.number_operator())
        {
            case 0: os << (Crossover&)intra;break;
            case 1: os << (Mutation&)intra;break;
            case 2: os << (Improve&)intra;break;
        }
        return os;
    }

    Intra_Operator::~Intra_Operator()
    {}

//  Crossover:Intra_operator -------------------------------------------------------------

    Crossover::Crossover():Intra_Operator(0)
    {
        probability = new float[1];
    }

    void Crossover::cross(Solution& sol1, Solution& sol2) const // dadas dos soluciones de la poblacion, las cruza
    {
        if (N_WORKLOAD > 4) {
            /*int limit_top = rand_int((N_WORKLOAD/2)+1, N_WORKLOAD-1);
            int limit_bottom = rand_int(0, limit_top-1);

            for (int i = 0; i < limit_bottom; i++) {
                sol2.crossover(0, limit_bottom-1, sol1);
            }

            for (int i = limit_bottom; i < limit_top; i++) {
                sol1.crossover(limit_bottom, limit_top-1, sol2);
            }

            for (int i = limit_top; i < N_WORKLOAD; i++) {
                sol2.crossover(limit_top, N_WORKLOAD-1, sol1);
            }*/
            
            int limit_top = rand_int((N_WORKLOAD/2)+1, N_WORKLOAD-1);
            int limit_bottom = rand_int(0, limit_top-1);

            sol2.crossover(0, limit_bottom-1, sol1);
            sol1.crossover(limit_bottom, limit_top-1, sol2);
            sol2.crossover(limit_top, N_WORKLOAD-1, sol1);
            
            /*
            int pivot = rand_int(1, N_WORKLOAD-3);
            sol2.crossover(0, pivot, sol1);
            sol1.crossover(pivot+1, N_WORKLOAD-1, sol2);
            * */
        }
    }

    void Crossover::execute(Rarray<Solution*>& sols) const
    {
        for (int i=0;i+1<sols.size();i=i+2)
            if (rand01()<=probability[0]) cross(*sols[i],*sols[i+1]);
    }

    ostream& operator<< (ostream& os, const Crossover&  cross)
    {
         os << "Crossover." << " Probability: "
                    << cross.probability[0]
            << endl;
         return os;
    }

    void Crossover::RefreshState(const StateCenter& _sc) const
    {
        _sc.set_contents_state_variable("_crossover_probability",(char *)probability,1,sizeof(float));
    }

    void Crossover::UpdateFromState(const StateCenter& _sc)
    {
         unsigned long nbytes,length;
         _sc.get_contents_state_variable("_crossover_probability",(char *)probability,nbytes,length);
    }

    void Crossover::setup(char line[MAX_BUFFER])
    {
        int op;
        sscanf(line," %d %f ",&op,&probability[0]);
        assert(probability[0]>=0);
    }

    Crossover::~Crossover()
    {
        delete [] probability;
    }

    //  Mutation: Sub_operator -------------------------------------------------------------

    Mutation::Mutation():Intra_Operator(1)
    {
        probability = new float[2];
    }

    void Mutation::mutate(Solution& sol) const
    {
        sol.mutate(probability[1]);
    }

    void Mutation::execute(Rarray<Solution*>& sols) const
    {
        for (int i=0;i<sols.size();i++)
            if(rand01() <= probability[0])  mutate(*sols[i]);
    }

    ostream& operator<< (ostream& os, const Mutation&  mutation)
    {
        os << "Mutation." << " Probability: " << mutation.probability[0]
           << " Probability1: " << mutation.probability[1]
           << endl;
        return os;
    }

    void Mutation::setup(char line[MAX_BUFFER])
    {
        int op;
        sscanf(line," %d %f %f ",&op,&probability[0],&probability[1]);
        assert(probability[0]>=0);
        assert(probability[1]>=0);
    }

    void Mutation::RefreshState(const StateCenter& _sc) const
    {
        _sc.set_contents_state_variable("_mutation_probability",(char *)probability,2,sizeof(probability));
    }

    void Mutation::UpdateFromState(const StateCenter& _sc)
    {
        unsigned long nbytes,length;
        _sc.get_contents_state_variable("_mutation_probability",(char *)probability,nbytes,length);
    }

    Mutation::~Mutation()
    {
        delete [] probability;
    }

// Movement  -------------------------------------------------------


    Movement::Movement()
    {}

    Movement::~Movement()
    {}

    void Movement::Apply (Solution& sol) const
    {
        sol.local_search();
    }
}

#endif
