W[0]="w.i.100.20.2"
S[0]="s.20.2"

C[0]="0.5"
C[1]="0.7"
C[2]="0.9"

M[0]="0.5"
M[1]="0.7"
M[2]="0.9"

LS[0]="0.1"
LS[1]="0.3"
LS[2]="0.5"

OUTPUT_FILE="test_normality_jb.result"

echo "" > ${OUTPUT_FILE}

for (( c_idx=0; c_idx<3; c_idx++ ))
do
    for (( m_idx=0; m_idx<3; m_idx++ ))
    do
        for (( ls_idx=0; ls_idx<2; ls_idx++ ))
        do
            for (( s_idx=0; s_idx<1; s_idx++ ))
            do
                for (( w_idx=0; w_idx<1; w_idx++ ))
                do
                    ID="c${C[c_idx]}_m${M[m_idx]}_ls${LS[ls_idx]}__${S[s_idx]}__${W[w_idx]}.eval"
                    R_SCRIPT="x = scan('${ID}'); x_r = fBasics::jarqueberaTest(x); x_r@test[['p.value']]"

                    echo "=== ${ID} ================" >> ${OUTPUT_FILE}
                    R -q -e "${R_SCRIPT}" >> ${OUTPUT_FILE}
                done
            done
        done
    done
done
