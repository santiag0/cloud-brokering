#ifndef INC_Utils
#define INC_Utils

// Usable AlmostEqual function
bool AlmostEqual2sComplement(float A, float B, int maxUlps, float maxAbsError);

#endif
