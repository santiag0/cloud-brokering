#INST_PATH="/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
INST_PATH="/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/instances"
CFG_FILE="newGA_eval.cfg"

SN=5
S[0]="10"
S[1]="20"
S[2]="20"
S[3]="50"

WN=5
W[0]="50"
W[1]="100"
W[2]="200"
W[3]="400"

CORES[0]="4"
CORES[1]="6"
CORES[2]="8"
CORES[3]="12"
CORES[4]="16"
CORES[5]="24"

mkdir cfg

RESULT_PATH="result"
mkdir ${RESULT_PATH}

s_idx=1

for ci_idx in {0..5}
do
    for si_idx in {1..2}
    do
        for wi_idx in {1..2}
        do
            index=1
            #si_idx=2
            #wi_idx=2

            SCENARIO_FILE="s.${S[index]}.${si_idx}"
            WORKLOAD_FILE="w.i.${W[index]}.${S[index]}.${wi_idx}"

            ID="s.${S[index]}.${si_idx}__w.${W[index]}.${wi_idx}"

            RESULT_FILE="${ID}_p.${CORES[ci_idx]}.out"
            GASA_CFG_FILE="cfg/${ID}_p.${CORES[ci_idx]}.cfg"

            echo "Parameters" > ${GASA_CFG_FILE}
            echo "1	// Selection Number" >> ${GASA_CFG_FILE}
            echo "3	// Selection Parameter" >> ${GASA_CFG_FILE}
            echo "20	// Number of solutions passed to SA" >> ${GASA_CFG_FILE}
            echo "Configuration-Files" >> ${GASA_CFG_FILE}
            echo "${CFG_FILE}" >> ${GASA_CFG_FILE}
            echo "SA.cfg" >> ${GASA_CFG_FILE}
            echo "Problem-Files" >> ${GASA_CFG_FILE}
            echo "${INST_PATH}/c.pru.1" >> ${GASA_CFG_FILE}
            echo "${INST_PATH}/scenarios/${SCENARIO_FILE}" >> ${GASA_CFG_FILE}
            echo "${INST_PATH}/workloads/${WORKLOAD_FILE}" >> ${GASA_CFG_FILE}
            echo "${RESULT_PATH}/${RESULT_FILE}" >> ${GASA_CFG_FILE}
        done
    done
done
