#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  verify.py
#
#  Copyright 2013 Unknown <santiago@marga>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys
import math

def main():
    W=["w.i.100.20.1","w.i.100.20.2"]
    S=["s.20.1","s.20.2"]

    C=["0.5","0.7","0.9"]
    M=["0.5","0.7","0.9"]
    LS=["0.1","0.3","0.5"]

    BIG_DATA = []

    R_DATA = {}

    if len(sys.argv) != 2:
        print("Error! Usage: {0:s} <result dir>".format(sys.argv[0]))
        exit(-1)

    inst_path = "/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
    #result_path = "/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/result"
    result_path = sys.argv[1]

    cost_file = inst_path + "/c.pru.1"
    scenario_path = inst_path + "/scenarios"
    workload_path = inst_path + "/workloads"

    alg_index = 1

    with open("calibracion_summary.txt","w") as summary:
        summary.write("avg prof\tstdev prof\tavg makespan\tstdev makespan\tavg flowtimw\tstdev flowtime\tavg ondemand\tstdev ondemand\tname\n")
        
        for c_idx in range(3):
            for m_idx in range(3):
                for ls_idx in range(2):
                    for s_idx in range(2):
                        for w_idx in range(2):
                            ID="c{0:s}_m{1:s}_ls{2:s}__{3:s}__{4:s}".format(C[c_idx],M[m_idx],LS[ls_idx],S[s_idx],W[w_idx])

                            scenario_file = scenario_path + "/" + S[s_idx]
                            workload_file = workload_path + "/" + W[w_idx]

                            # 0:booked / 1:ondemand / 2:data / 3:price
                            cost = []

                            # 0:mem / 1:proc / 2:disk / 3:ncores / 4:type
                            scenario = []

                            # 0:mem / 1:proc / 2:disk / 3:ncores / 4:dtrans / 5:etime / 6:dtime / 7:atime
                            workload = []

                            with open(cost_file) as f:
                                for line in f:
                                    if len(line.strip()) > 0:
                                        line_data = line.strip().split(" ")
                                        cost.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), float(line_data[3])))

                            with open(scenario_file) as f:
                                for line in f:
                                    if (len(line.strip()) > 0):
                                        line_data = line.strip().split(" ")
                                        scenario.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), int(line_data[4])))

                            with open(workload_file) as f:
                                for line in f:
                                    if (len(line.strip()) > 0):
                                        line_data = line.strip().split(" ")
                                        workload.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), \
                                            float(line_data[4]), float(line_data[5]), float(line_data[6])+float(line_data[7]), float(line_data[7])))

                            task_price = []
                            for t_id in range(len(workload)):
                                task_price.append(-1)

                                for m_id in range(len(scenario)):
                                    if ((workload[t_id][0] <= scenario[m_id][0]) and (workload[t_id][1] <= scenario[m_id][1]) \
                                    and (workload[t_id][2] <= scenario[m_id][2]) and (workload[t_id][3] <= scenario[m_id][3])):
                                        aux = cost[scenario[m_id][4]][3]

                                        if task_price[t_id] == -1:
                                            task_price[t_id] = aux
                                        elif task_price[t_id] > aux:
                                            task_price[t_id] = aux

                            sum_fitness = 0
                            fitness_list = []
                            
                            sum_makespan = 0
                            makespan_list = []
                            
                            sum_flowtime = 0
                            flowtime_list = []

                            sum_ondemand = 0
                            ondemand_list = []

                            with open(ID + ".eval","w") as output:
                                for iteration in range(30):
                                    solution_file = result_path + "/" + ID + ".out_" + str(iteration)

                                    # 0:t_id / 1:m_id / 2:stime / 3:ondemand
                                    solution = []

                                    with open(solution_file) as f:
                                        for line in f:
                                            if (len(line.strip()) > 0):
                                                line_data = line.strip().split("|")
                                                #solution.append((int(line_data[0]), int(line_data[1]), float(line_data[2]), int(line_data[3])))
                                                solution.append((float(line_data[2]), int(line_data[0]), int(line_data[1]), int(line_data[3])))

                                    solution = sorted(solution)

                                    fitness_booked = 0.0
                                    fitness_ondemand = 0.0

                                    num_wrongly_booked = 0
                                    num_booked = 0
                                    num_ondemand = 0

                                    machine_ct = []
                                    for i in scenario: machine_ct.append(0.0)

                                    flowtime = 0.0

                                    for i in solution:
                                        stime = i[0]
                                        t_id = i[1]
                                        m_id = i[2]
                                        ondemand = i[3]

                                        if (ondemand == 0):
                                            assert(stime <= workload[t_id][6])
                                            #HACK!
                                            #assert(stime >= workload[t_id][7])

                                        assert(workload[t_id][0] <= scenario[m_id][0])
                                        assert(workload[t_id][1] <= scenario[m_id][1])
                                        assert(workload[t_id][2] <= scenario[m_id][2])
                                        assert(workload[t_id][3] <= scenario[m_id][3])

                                        m_type = scenario[m_id][4]

                                        if ondemand == 0:
                                            if abs(machine_ct[m_id]-stime) >= 0.009:
                                                print("m_id="+str(m_id))
                                                print("t_id="+str(t_id))
                                                print("stime="+str(stime))
                                                print("ct="+str(machine_ct[m_id]))
                                                print(solution_file)
                                            
                                            assert(abs(machine_ct[m_id]-stime) < 0.009)
                                            machine_ct[m_id] = stime + workload[t_id][5]
                                            
                                            if task_price[t_id] <= cost[m_type][0]:
                                                num_wrongly_booked = num_wrongly_booked + 1
                                            num_booked = num_booked + 1

                                            aux_fitness = workload[t_id][5] * (task_price[t_id]-cost[m_type][0])
                                            fitness_booked = fitness_booked + aux_fitness
                                            
                                            flowtime = flowtime + workload[t_id][5] + stime
                                        else:
                                            num_ondemand = num_ondemand + 1

                                            aux_fitness = workload[t_id][5] * (task_price[t_id]-cost[m_type][1])
                                            fitness_ondemand = fitness_ondemand + aux_fitness
                                            
                                            flowtime = flowtime + workload[t_id][5]

                                    fitness = fitness_booked+fitness_ondemand

                                    sum_fitness = sum_fitness + fitness
                                    fitness_list.append(fitness)

                                    output.write("{0:.4f}\n".format(fitness))
                                    
                                    makespan = max(machine_ct)
                                    sum_makespan = sum_makespan + makespan
                                    makespan_list.append(makespan)
                                    
                                    sum_flowtime = sum_flowtime + flowtime
                                    flowtime_list.append(flowtime)
                                    
                                    sum_ondemand = sum_ondemand + num_ondemand
                                    ondemand_list.append(num_ondemand)
                                    
                                    print("Fitness={0:.4f}".format(fitness))
                                    print("Makespan={0:.4f}".format(makespan))
                                    print("Flowtime={0:.4f}".format(flowtime))
                                    print("Ondemand={0:d}".format(num_ondemand))
                                    print("")
                                    
                            
                            BIG_DATA.append((ID,fitness_list))
                                    
                            avg_fitness = sum_fitness / len(fitness_list)
                            
                            aux_stdev_fitness = 0
                            for i in fitness_list:
                                aux_stdev_fitness = aux_stdev_fitness + math.pow(i - avg_fitness,2)
                            stdev_fitness = math.sqrt(aux_stdev_fitness / (len(fitness_list)-1))
                            
                            avg_makespan = sum_makespan / len(makespan_list)
                            
                            aux_stdev_makespan = 0
                            for i in makespan_list:
                                aux_stdev_makespan = aux_stdev_makespan + math.pow(i - avg_makespan,2)
                            stdev_makespan = math.sqrt(aux_stdev_makespan / (len(makespan_list)-1))
                            
                            avg_flowtime = sum_flowtime / len(flowtime_list)
                            
                            aux_stdev_flowtime = 0
                            for i in flowtime_list:
                                aux_stdev_flowtime = aux_stdev_flowtime + math.pow(i - avg_flowtime,2)
                            stdev_flowtime = math.sqrt(aux_stdev_flowtime / (len(flowtime_list)-1))
                           
                            avg_ondemand = sum_ondemand / len(ondemand_list)
                            
                            aux_stdev_ondemand = 0
                            for i in ondemand_list:
                                aux_stdev_ondemand = aux_stdev_ondemand + math.pow(i - avg_ondemand,2)
                            stdev_ondemand = math.sqrt(aux_stdev_ondemand / (len(ondemand_list)-1))
                            
                            ALGORITHM_NAME="c{0:s}_m{1:s}_ls{2:s}".format(C[c_idx],M[m_idx],LS[ls_idx])
                            INSTANCE_NAME=S[s_idx] + "__" + W[w_idx]
                            
                            summary.write("{0:.4f}\t{1:.4f}\t{2:.4f}\t{3:.4f}\t{4:.4f}\t{5:.4f}\t{6:.4f}\t{7:.4f}\t{8:s}\t{9:d}\t{10:s}\n".format( \
                                avg_fitness,stdev_fitness,avg_makespan,stdev_makespan,avg_flowtime,stdev_flowtime,avg_ondemand,stdev_ondemand,ALGORITHM_NAME,alg_index,INSTANCE_NAME))
                                
                            alg_index = alg_index + 1
                            
                            if not ALGORITHM_NAME in R_DATA:
                                R_DATA[ALGORITHM_NAME] = {}
                                
                            if not INSTANCE_NAME in R_DATA[ALGORITHM_NAME]:
                                R_DATA[ALGORITHM_NAME][INSTANCE_NAME] = fitness_list
                                
    #with open("calibracion_big_table.txt","w") as big_table:
    #    for r in range(30):
    #        for (ID,l) in BIG_DATA:
    #            print("{0:.4f}".format(l[r]), end=" ")
    #            big_table.write("{0:.4f} ".format(l[r]))
    #            #big_table.write("{0:.4f} ".format(l[r]))
    #            #big_table.write("{0:.4f} {1:s} {2:d}\n".format(l[r], ID, r))
    #            
    #        print("")
    #        #big_table.write("\n")

    with open("calibracion_big_table.txt","w") as big_table:
        for alg in R_DATA:
            for pinst in R_DATA[alg]:
                for obs_idx in range(len(R_DATA[alg][pinst])): 
                    #big_table.write("{0:.4f} {1:s} {2:s}\n".format(R_DATA[alg][pinst][obs_idx], pinst, alg))
                    big_table.write("{0:.4f} ".format(R_DATA[alg][pinst][obs_idx]))

    return 0

if __name__ == '__main__':
    main()

