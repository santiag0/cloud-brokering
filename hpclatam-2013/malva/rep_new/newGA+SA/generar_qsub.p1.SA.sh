SN=5
S[0]="10"
S[1]="20"
S[2]="30"
S[3]="50"

WN=5
W[0]="50"
W[1]="100"
W[2]="200"
W[3]="400"
WS[0]="10"
WS[1]="20"
WS[2]="20"
WS[3]="50"

QSUB_DIR="qsub.p1_SA"
RESULT_DIR="result.p1_SA"
CFG_DIR="cfg_SA"

mkdir ${QSUB_DIR}
mkdir ${RESULT_DIR}

EXEC="./MainSeq1"

for (( s_idx=0; s_idx <= 3; s_idx++ ))
do
    RUN_ALL_SCRIPT="run_all_p1_SA.${S[s_idx]}.sh"
    echo "" > ${RUN_ALL_SCRIPT}

    for (( w_idx=0; w_idx <= 3; w_idx++ ))
    do
        for (( si_idx=1; si_idx<=SN; si_idx++ ))
        do
            for (( wi_idx=1; wi_idx<=WN; wi_idx++ ))
            do
                SCENARIO_FILE="s.${S[s_idx]}.${si_idx}"
                WORKLOAD_FILE="w.i.${W[w_idx]}.${WS[w_idx]}.${wi_idx}"

                ID="s.${S[s_idx]}.${si_idx}__w.${W[w_idx]}.${wi_idx}"
       
                GASA_CFG_FILE="${CFG_DIR}/${ID}.cfg"
                QSUB_FILE="${QSUB_DIR}/${ID}.qsub"
                RESULT_FILE="${ID}.out"
            
                echo "#!/bin/bash" > ${QSUB_FILE}
                echo "# Nombre del trabajo" >> ${QSUB_FILE}
                echo "#PBS -N gasa_${S[s_idx]}.${si_idx}.${W[w_idx]}.${wi_idx}" >> ${QSUB_FILE}
                echo "# Requerimientos" >> ${QSUB_FILE}
                echo "#PBS -l nodes=1:cpu24,walltime=04:00:00" >> ${QSUB_FILE}
                echo "# Cola" >> ${QSUB_FILE}
                echo "#PBS -q medium_jobs" >> ${QSUB_FILE}
                echo "# Working dir" >> ${QSUB_FILE}
                echo "#PBS -d /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/" >> ${QSUB_FILE}
                echo "# Correo electronico" >> ${QSUB_FILE}
                echo "###PBS -M siturria@fing.edu.uy" >> ${QSUB_FILE}
                echo "# Email" >> ${QSUB_FILE}
                echo "#PBS -m abe" >> ${QSUB_FILE}
                echo "# n: no mail will be sent." >> ${QSUB_FILE}
                echo "# a: mail is sent when the job is aborted by the batch system." >> ${QSUB_FILE}
                echo "# b: mail is sent when the job begins execution." >> ${QSUB_FILE}
                echo "# e: mail is sent when the job terminates." >> ${QSUB_FILE}
                echo "# Output path" >> ${QSUB_FILE}
                echo "#PBS -e /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/${QSUB_DIR}/" >> ${QSUB_FILE}
                echo "#PBS -o /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/${QSUB_DIR}/" >> ${QSUB_FILE}
                echo "#PBS -V" >> ${QSUB_FILE}
                echo "echo Job Name: \$PBS_JOBNAME" >> ${QSUB_FILE}
                echo "echo Working directory: \$PBS_O_WORKDIR" >> ${QSUB_FILE}
                echo "echo Queue: \$PBS_QUEUE" >> ${QSUB_FILE}
                echo "echo Cantidad de tasks: \$PBS_TASKNUM" >> ${QSUB_FILE}
                echo "echo Home: \$PBS_O_HOME" >> ${QSUB_FILE}
                echo "echo Puerto del MOM: \$PBS_MOMPORT" >> ${QSUB_FILE}
                echo "echo Nombre del usuario: \$PBS_O_LOGNAME" >> ${QSUB_FILE}
                echo "echo Idioma: \$PBS_O_LANG" >> ${QSUB_FILE}
                echo "echo Cookie: \$PBS_JOBCOOKIE" >> ${QSUB_FILE}
                echo "echo Offset de numero de nodos: \$PBS_NODENUM" >> ${QSUB_FILE}
                echo "echo Shell: \$PBS_O_SHELL" >> ${QSUB_FILE}
                echo "#echo JobID: \$PBS_O_JOBID" >> ${QSUB_FILE}
                echo "echo Host: \$PBS_O_HOST" >> ${QSUB_FILE}
                echo "echo Cola de ejecucion: \$PBS_QUEUE" >> ${QSUB_FILE}
                echo "echo Archivo de nodos: \$PBS_NODEFILE" >> ${QSUB_FILE}
                echo "echo Path: \$PBS_O_PATH" >> ${QSUB_FILE}
                echo "cd \$PBS_O_WORKDIR" >> ${QSUB_FILE}
                echo "echo Current path: " >> ${QSUB_FILE}
                echo "pwd" >> ${QSUB_FILE}
                echo "echo Nodos:" >> ${QSUB_FILE}
                echo "cat \$PBS_NODEFILE" >> ${QSUB_FILE}
            
                echo "for (( i=0; i<50; i++ ))" >> ${QSUB_FILE}
                echo "do" >> ${QSUB_FILE}
                echo "${EXEC} ${GASA_CFG_FILE}" >> ${QSUB_FILE}
                echo "mv ${RESULT_DIR}/${RESULT_FILE} ${RESULT_DIR}/${RESULT_FILE}_\${i}" >> ${QSUB_FILE}
                echo "done" >> ${QSUB_FILE}
            
                echo "qsub ${QSUB_FILE}" >> ${RUN_ALL_SCRIPT}
                echo "sleep 1" >> ${RUN_ALL_SCRIPT}
            done
        done
    done
    
    chmod +x ${RUN_ALL_SCRIPT}
done
