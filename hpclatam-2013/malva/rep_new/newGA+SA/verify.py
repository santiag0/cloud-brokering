#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  verify.py
#
#  Copyright 2013 Unknown <santiago@marga>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys

def main():
    #inst_path = "/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
    inst_path = "/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/instances"
   
    if (len(sys.argv) != 4):
        print("Error! Usage {0:s} <scenario> <workload> <result>".format(sys.argv[0]))
        exit(-1)
    
    cost_path = inst_path + "/c.pru.1"
    scenario_path = inst_path + "/scenarios/" + sys.argv[1]
    workload_path = inst_path + "/workloads/" + sys.argv[2]
    solution_path = sys.argv[3]

    # 0:booked / 1:ondemand / 2:data / 3:price
    cost = []

    # 0:mem / 1:proc / 2:disk / 3:ncores / 4:type
    scenario = []

    # 0:mem / 1:proc / 2:disk / 3:ncores / 4:dtrans / 5:etime / 6:dtime / 7:atime
    workload = []

    # 0:t_id / 1:m_id / 2:stime / 3:ondemand
    solution = []

    with open(cost_path) as f:
        for line in f:
            if len(line.strip()) > 0:
                line_data = line.strip().split(" ")
                cost.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), float(line_data[3])))

    with open(scenario_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split(" ")
                scenario.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), int(line_data[4])))

    with open(workload_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split(" ")
                workload.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), \
                    float(line_data[4]), float(line_data[5]), float(line_data[6])+float(line_data[7]), float(line_data[7])))

    with open(solution_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split("|")
                solution.append((int(line_data[0]), int(line_data[1]), float(line_data[2]), int(line_data[3])))

    fitness_booked = 0.0
    fitness_ondemand = 0.0

    task_price = []
    for t_id in range(len(workload)):
        task_price.append(-1)

        for m_id in range(len(scenario)):
            if ((workload[t_id][0] <= scenario[m_id][0]) and (workload[t_id][1] <= scenario[m_id][1]) \
            and (workload[t_id][2] <= scenario[m_id][2]) and (workload[t_id][3] <= scenario[m_id][3])):
                aux = cost[scenario[m_id][4]][3]

                if task_price[t_id] == -1:
                    task_price[t_id] = aux
                elif task_price[t_id] > aux:
                    task_price[t_id] = aux

    #print(task_price)

    num_wrongly_booked = 0
    num_booked = 0
    num_ondemand = 0

    sum_makespan = 0.0
    makespan_list = []
    for i in range(len(scenario)): makespan_list.append(0.0)
    
    sum_flowtime = 0.0
    flowtime_list = []
    for i in range(len(scenario)): flowtime_list.append(0.0)

    for i in solution:
        t_id = i[0]
        stime = i[2]

        #print(">> t_id={0:d}".format(t_id))

        m_id = i[1]
        #print("   m_id={0:d}".format(m_id))

        ondemand = i[3]

        if (ondemand == 0):
            #print("   prebooked")

            #print("   deadline stime={0:.2f} dtime={1:.2f}".format(stime,workload[t_id][6]))
            assert(stime <= workload[t_id][6])

            #print("   arrival  stime={0:.2f} atime={1:.2f}".format(stime,workload[t_id][7]))
            ##HACK!
            ##assert(stime >= workload[t_id][7])
        else:
            #print("   ondemand")
            pass

        #print("   etime  w={0:.2f}".format(workload[t_id][5]))
        #print("   mem    w={0:.2f} s={1:.2f}".format(workload[t_id][0],scenario[m_id][0]))
        #print("   proc   w={0:.2f} s={1:.2f}".format(workload[t_id][1],scenario[m_id][1]))
        #print("   disk   w={0:.2f} s={1:.2f}".format(workload[t_id][2],scenario[m_id][2]))
        #print("   ncores w={0:.2f} s={1:.2f}".format(workload[t_id][3],scenario[m_id][3]))

        assert(workload[t_id][0] <= scenario[m_id][0])
        assert(workload[t_id][1] <= scenario[m_id][1])
        assert(workload[t_id][2] <= scenario[m_id][2])
        assert(workload[t_id][3] <= scenario[m_id][3])

        #print("   price  v={0:.4f}".format(task_price[t_id]))

        m_type = scenario[m_id][4]

        if ondemand == 0:
            if task_price[t_id] <= cost[m_type][0]:
                num_wrongly_booked = num_wrongly_booked + 1
            num_booked = num_booked + 1

            aux_fitness = workload[t_id][5] * (task_price[t_id]-cost[m_type][0])
            fitness_booked = fitness_booked + aux_fitness

            flowtime_list[m_id] = flowtime_list[m_id] + workload[t_id][5] + stime
            sum_flowtime = sum_flowtime + workload[t_id][5] + stime

            if (makespan_list[m_id] < workload[t_id][5] + stime):
                makespan_list[m_id] = workload[t_id][5] + stime
                
            if (sum_makespan < workload[t_id][5] + stime):
                sum_makespan = workload[t_id][5] + stime

            #print("   booked cost v={0:.4f}".format(cost[m_type][0]))
            
            if m_id == 2:
                print(">> {0:d} {1:.2f} {2:.2f} {3:.2f}".format(t_id,workload[t_id][5],stime,workload[t_id][5] + stime))
        else:
            num_ondemand = num_ondemand + 1

            aux_fitness = workload[t_id][5] * (task_price[t_id]-cost[m_type][1])
            fitness_ondemand = fitness_ondemand + aux_fitness

            sum_flowtime = sum_flowtime + workload[t_id][5]
            
            if (sum_makespan < workload[t_id][5]):
                sum_makespan = workload[t_id][5]

            #print("   ondemand cost v={0:.4f}".format(cost[m_type][1]))

    print("num_wrongly_booked={0:d}".format(num_wrongly_booked))
    print("num_booked={0:d}".format(num_booked))
    print("num_ondemand={0:d}".format(num_ondemand))

    print("makespan={0:.2f}".format(sum_makespan))
    print("flowtime={0:.2f}".format(sum_flowtime))
    
    for i in range(len(scenario)):
        print("makespan[{0:d}]={1:.2f}".format(i,makespan_list[i]))
        
    for i in range(len(scenario)):
        print("flowtime[{0:d}]={1:.2f}".format(i,flowtime_list[i]))

    print("fitness_booked={0:.2f}".format(fitness_booked))
    print("fitness_ondemand={0:.2f}".format(fitness_ondemand))
    print("fitness={0:.2f}".format(fitness_booked+fitness_ondemand))

    return 0

if __name__ == '__main__':
    main()

