#!/usr/bin/env python2
# -*- coding: utf-8 -*-
#
#  verify.py
#
#  Copyright 2013 Unknown <santiago@marga>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import sys

def main():
    if (len(sys.argv) != 4):
        print("Error! Usage: {0:s} <relative scenario file path> <relative workload file path> <solution file>".format(sys.argv[0]))
        exit(-1)

    inst_path = "/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"

    cost_path = inst_path + "/c.pru.1"
    scenario_path = inst_path + "/scenarios/" + sys.argv[1]
    workload_path = inst_path + "/workloads/" + sys.argv[2]
    solution_path = sys.argv[3]

    # 0:booked / 1:ondemand / 2:data / 3:price
    cost = []

    # 0:mem / 1:proc / 2:disk / 3:ncores / 4:type
    scenario = []

    # 0:mem / 1:proc / 2:disk / 3:ncores / 4:dtrans / 5:etime / 6:dtime / 7:atime
    workload = []

    # 0:t_id / 1:m_id / 2:stime
    solution = []

    with open(cost_path) as f:
        for line in f:
            if len(line.strip()) > 0:
                line_data = line.strip().split(" ")
                cost.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), float(line_data[3])))

    with open(scenario_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split(" ")
                scenario.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), int(line_data[4])))

    with open(workload_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split(" ")
                workload.append((float(line_data[0]), float(line_data[1]), float(line_data[2]), int(line_data[3]), \
                    float(line_data[4]), float(line_data[5]), float(line_data[6])+float(line_data[7]), float(line_data[7])))

    with open(solution_path) as f:
        for line in f:
            if (len(line.strip()) > 0):
                line_data = line.strip().split(" ")
                solution.append((int(line_data[0]), int(line_data[1]), float(line_data[2])))

    sum_fitness = 0
    fitness_list = []

    sum_makespan = 0
    sum_flowtime = 0

    fitness_booked = 0.0
    fitness_ondemand = 0.0

    task_ondemand_cost = []
    task_price = []

    for t_id in range(len(workload)):
        task_price.append(-1)
        task_ondemand_cost.append(-1)

        for m_id in range(len(scenario)):
            if ((workload[t_id][0] <= scenario[m_id][0]) and (workload[t_id][1] <= scenario[m_id][1]) \
            and (workload[t_id][2] <= scenario[m_id][2]) and (workload[t_id][3] <= scenario[m_id][3])):
                aux = cost[scenario[m_id][4]][3]
                aux2 = cost[scenario[m_id][4]][1]

                if task_price[t_id] == -1:
                    task_price[t_id] = aux
                elif task_price[t_id] > aux:
                    task_price[t_id] = aux

                if task_ondemand_cost[t_id] == -1:
                    task_ondemand_cost[t_id] = aux2
                elif task_ondemand_cost[t_id] > aux2:
                    task_ondemand_cost[t_id] = aux2

    #print(task_price)

    num_wrongly_booked = 0
    num_booked = 0
    num_ondemand = 0

    for i in solution:
        t_id = i[0]
        m_id = i[1]
        stime = i[2]

        print(">> t_id={0:d}".format(t_id))
        print("   m_id={0:d}".format(m_id))
        print("   price  v={0:.4f}".format(task_price[t_id]))

        if (m_id == -1):
            print("   ondemand")

            num_ondemand = num_ondemand + 1

            aux_fitness = workload[t_id][5] * (task_price[t_id]-task_ondemand_cost[t_id])
            fitness_ondemand = fitness_ondemand + aux_fitness

            if (workload[t_id][5] > sum_makespan): sum_makespan = workload[t_id][5]
            sum_flowtime = sum_flowtime + workload[t_id][5]

            print("   ondemand cost v={0:.4f}".format(cost[m_type][1]))
        else:
            print("   mem    w={0:.2f} s={1:.2f}".format(workload[t_id][0],scenario[m_id][0]))
            print("   proc   w={0:.2f} s={1:.2f}".format(workload[t_id][1],scenario[m_id][1]))
            print("   disk   w={0:.2f} s={1:.2f}".format(workload[t_id][2],scenario[m_id][2]))
            print("   ncores w={0:.2f} s={1:.2f}".format(workload[t_id][3],scenario[m_id][3]))

            assert(workload[t_id][0] <= scenario[m_id][0])
            assert(workload[t_id][1] <= scenario[m_id][1])
            assert(workload[t_id][2] <= scenario[m_id][2])
            assert(workload[t_id][3] <= scenario[m_id][3])

            m_type = scenario[m_id][4]

            if task_price[t_id] <= cost[m_type][0]:
                num_wrongly_booked = num_wrongly_booked + 1
            num_booked = num_booked + 1

            aux_fitness = workload[t_id][5] * (task_price[t_id]-cost[m_type][0])

            fitness_booked = fitness_booked + aux_fitness

            if (workload[t_id][5] + stime > sum_makespan): sum_makespan = workload[t_id][5] + stime
            sum_flowtime = sum_flowtime + workload[t_id][5] + stime

            print("   booked cost v={0:.4f}".format(cost[m_type][0]))

    print("num_wrongly_booked={0:d}".format(num_wrongly_booked))
    print("num_booked={0:d}".format(num_booked))
    print("num_ondemand={0:d}".format(num_ondemand))

    print("makespan={0:.2f}".format(sum_makespan))
    print("flowtime={0:.2f}".format(sum_flowtime))

    print("fitness_booked={0:.2f}".format(fitness_booked))
    print("fitness_ondemand={0:.2f}".format(fitness_ondemand))
    print("fitness={0:.2f}".format(fitness_booked+fitness_ondemand))

    return 0

if __name__ == '__main__':
    main()

