#define F_MAX_BUFFER        2048

#define MAXN_MACH_TYPE      20
#define MAXN_SCENARIO       50 /* el último escenario es para la maquina fake */
#define MAXN_WORKLOAD       500
#define MAXN_G_WORKLOAD     2000

#define ONDEMAND_WORKLOAD   MAXN_WORKLOAD
#define ONDEMAND_SCENARIO   MAXN_WORKLOAD

#define SIZEOF_INT          4
#define SIZEOF_FLOAT        4
#define SERIALIZATION_SIZE  ((MAXN_WORKLOAD+MAXN_SCENARIO+1) * SIZEOF_INT) + (MAXN_WORKLOAD * SIZEOF_INT)
#define SERIALIZATION_SEP   -1

#define MAX_ABSOLUTE_ERROR  1
#define MAX_NFLOAT_ERROR    5000
#define FITNESS_RESET_COUNT 15

#define C_CURRENT_TIME      0
//#define C_TIME_STEP         2147483647
#define C_TIME_STEP         1000
//#define C_TIME_STEP         56
//#define C_TIME_STEP         62
//#define C_TIME_STEP         5

//#define WORKLOAD_DEADLINE_SORT
#define INIT_RAND_TASK
#define INIT_BEST_FIT

#define HACK_NO_ARRIVAL
//#define NO_RANDOM_SEED
//#define DONT_SEND_STATE
