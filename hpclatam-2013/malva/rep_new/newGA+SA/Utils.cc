#include <assert.h>
#include <stdlib.h>
#include <iostream>

#include "Utils.hh"

// Usable AlmostEqual function
bool AlmostEqual2sComplement(float A, float B, int maxUlps, float maxAbsError)
{   
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
    
    if ((A == 0) && ((B < 0.01) && (B > -0.01))) return true;
    if ((B == 0) && ((A < 0.01) && (A > -0.01))) return true;
    
    int aInt = *(int*)&A;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x80000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int bInt = *(int*)&B;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;
    int intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return true;
 
    if (A-B <= maxAbsError)
        return true;
 
    //#ifdef SUPERDEBUG
        std::cout << "abs error = " << A << "-" << B << " = " << (A-B) << " (diff=" << intDiff << ")" << std::endl;
    //#endif
        
    return false;
}
