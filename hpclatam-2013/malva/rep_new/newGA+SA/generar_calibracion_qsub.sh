W[0]="w.i.100.20.1"
S[0]="s.20.1"

W[1]="w.i.100.20.2"
S[1]="s.20.2"

C[0]="0.5"
C[1]="0.7"
C[2]="0.9"

M[0]="0.5"
M[1]="0.7"
M[2]="0.9"

LS[0]="0.1"
LS[1]="0.3"
LS[2]="0.5"

mkdir qsub

RUN_ALL_SCRIPT="run_all.sh"
echo "" > ${RUN_ALL_SCRIPT}

for (( c_idx=0; c_idx<3; c_idx++ ))
do
    for (( m_idx=0; m_idx<3; m_idx++ ))
    do
        for (( ls_idx=0; ls_idx<2; ls_idx++ ))
        do
            for (( s_idx=0; s_idx<2; s_idx++ ))
            do
                for (( w_idx=0; w_idx<2; w_idx++ ))
                do
                    ID="c${C[c_idx]}_m${M[m_idx]}_ls${LS[ls_idx]}__${S[s_idx]}__${W[w_idx]}"
                
                    GASA_CFG_FILE="cfg/newGASA_${ID}.cfg"
                    QSUB_FILE="qsub/${ID}.qsub"
                    RESULT_FILE="${ID}.out"
                    
                    echo "#!/bin/bash" > ${QSUB_FILE}
                    echo "# Nombre del trabajo" >> ${QSUB_FILE}
                    echo "#PBS -N gasa_${c_idx}${m_idx}${ls_idx}${s_idx}${w_idx}" >> ${QSUB_FILE}
                    echo "# Requerimientos" >> ${QSUB_FILE}
                    echo "#PBS -l nodes=1:cpu24,walltime=02:00:00" >> ${QSUB_FILE}
                    echo "# Cola" >> ${QSUB_FILE}
                    echo "#PBS -q small_jobs" >> ${QSUB_FILE}
                    echo "# Working dir" >> ${QSUB_FILE}
                    echo "#PBS -d /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/" >> ${QSUB_FILE}
                    echo "# Correo electronico" >> ${QSUB_FILE}
                    echo "###PBS -M siturria@fing.edu.uy" >> ${QSUB_FILE}
                    echo "# Email" >> ${QSUB_FILE}
                    echo "#PBS -m abe" >> ${QSUB_FILE}
                    echo "# n: no mail will be sent." >> ${QSUB_FILE}
                    echo "# a: mail is sent when the job is aborted by the batch system." >> ${QSUB_FILE}
                    echo "# b: mail is sent when the job begins execution." >> ${QSUB_FILE}
                    echo "# e: mail is sent when the job terminates." >> ${QSUB_FILE}
                    echo "# Output path" >> ${QSUB_FILE}
                    echo "#PBS -e /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/qsub/" >> ${QSUB_FILE}
                    echo "#PBS -o /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/qsub/" >> ${QSUB_FILE}
                    echo "#PBS -V" >> ${QSUB_FILE}
                    echo "echo Job Name: \$PBS_JOBNAME" >> ${QSUB_FILE}
                    echo "echo Working directory: \$PBS_O_WORKDIR" >> ${QSUB_FILE}
                    echo "echo Queue: \$PBS_QUEUE" >> ${QSUB_FILE}
                    echo "echo Cantidad de tasks: \$PBS_TASKNUM" >> ${QSUB_FILE}
                    echo "echo Home: \$PBS_O_HOME" >> ${QSUB_FILE}
                    echo "echo Puerto del MOM: \$PBS_MOMPORT" >> ${QSUB_FILE}
                    echo "echo Nombre del usuario: \$PBS_O_LOGNAME" >> ${QSUB_FILE}
                    echo "echo Idioma: \$PBS_O_LANG" >> ${QSUB_FILE}
                    echo "echo Cookie: \$PBS_JOBCOOKIE" >> ${QSUB_FILE}
                    echo "echo Offset de numero de nodos: \$PBS_NODENUM" >> ${QSUB_FILE}
                    echo "echo Shell: \$PBS_O_SHELL" >> ${QSUB_FILE}
                    echo "#echo JobID: \$PBS_O_JOBID" >> ${QSUB_FILE}
                    echo "echo Host: \$PBS_O_HOST" >> ${QSUB_FILE}
                    echo "echo Cola de ejecucion: \$PBS_QUEUE" >> ${QSUB_FILE}
                    echo "echo Archivo de nodos: \$PBS_NODEFILE" >> ${QSUB_FILE}
                    echo "echo Path: \$PBS_O_PATH" >> ${QSUB_FILE}
                    echo "cd \$PBS_O_WORKDIR" >> ${QSUB_FILE}
                    echo "echo Current path: " >> ${QSUB_FILE}
                    echo "pwd" >> ${QSUB_FILE}
                    echo "echo Nodos:" >> ${QSUB_FILE}
                    echo "cat \$PBS_NODEFILE" >> ${QSUB_FILE}
                    
                    echo "for (( i=0; i<30; i++ ))" >> ${QSUB_FILE}
                    echo "do" >> ${QSUB_FILE}
                    echo "./MainSeq1 ${GASA_CFG_FILE}" >> ${QSUB_FILE}
                    echo "mv result/${RESULT_FILE} result/${RESULT_FILE}_\${i}" >> ${QSUB_FILE}
                    echo "done" >> ${QSUB_FILE}
                    
                    echo "qsub ${QSUB_FILE}" >> ${RUN_ALL_SCRIPT}
                    echo "sleep 1" >> ${RUN_ALL_SCRIPT}
                done
            done
        done
    done
done

chmod +x run_all.sh
