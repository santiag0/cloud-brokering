SN=5
S[0]="10"
S[1]="20"
S[2]="20"
S[3]="50"

WN=5
W[0]="50"
W[1]="100"
W[2]="200"
W[3]="400"

mkdir qsub

RUN_ALL_SCRIPT="run_all.sh"
echo "" > ${RUN_ALL_SCRIPT}

EXEC="/home/siturria/bin/mpich2-1.2.1p1/bin/mpiexec.hydra -rmk pbs ./MainLan1"

for index in {0..3}
do
    for (( si_idx=1; si_idx<=SN; si_idx++ ))
    do
        for (( wi_idx=1; wi_idx<=WN; wi_idx++ ))
        do
            SCENARIO_FILE="s.${S[index]}.${si_idx}"
            WORKLOAD_FILE="w.i.${W[index]}.${S[index]}.${wi_idx}"

            ID="s.${S[index]}.${si_idx}__w.${W[index]}.${wi_idx}"
       
            GASA_CFG_FILE="cfg/${ID}.cfg"
            QSUB_FILE="qsub/${ID}.qsub"
            RESULT_FILE="${ID}.out"
            
            echo "#!/bin/bash" > ${QSUB_FILE}
            echo "# Nombre del trabajo" >> ${QSUB_FILE}
            echo "#PBS -N gasa_${S[index]}.${si_idx}.${W[index]}.${wi_idx}" >> ${QSUB_FILE}
            echo "# Requerimientos" >> ${QSUB_FILE}
            echo "#PBS -l nodes=1:cpu24:ppn=6,walltime=04:00:00" >> ${QSUB_FILE}
            echo "# Cola" >> ${QSUB_FILE}
            echo "#PBS -q small_jobs" >> ${QSUB_FILE}
            echo "# Working dir" >> ${QSUB_FILE}
            echo "#PBS -d /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/" >> ${QSUB_FILE}
            echo "# Correo electronico" >> ${QSUB_FILE}
            echo "###PBS -M siturria@fing.edu.uy" >> ${QSUB_FILE}
            echo "# Email" >> ${QSUB_FILE}
            echo "#PBS -m abe" >> ${QSUB_FILE}
            echo "# n: no mail will be sent." >> ${QSUB_FILE}
            echo "# a: mail is sent when the job is aborted by the batch system." >> ${QSUB_FILE}
            echo "# b: mail is sent when the job begins execution." >> ${QSUB_FILE}
            echo "# e: mail is sent when the job terminates." >> ${QSUB_FILE}
            echo "# Output path" >> ${QSUB_FILE}
            echo "#PBS -e /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/qsub/" >> ${QSUB_FILE}
            echo "#PBS -o /home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/qsub/" >> ${QSUB_FILE}
            echo "#PBS -V" >> ${QSUB_FILE}
            echo "echo Job Name: \$PBS_JOBNAME" >> ${QSUB_FILE}
            echo "echo Working directory: \$PBS_O_WORKDIR" >> ${QSUB_FILE}
            echo "echo Queue: \$PBS_QUEUE" >> ${QSUB_FILE}
            echo "echo Cantidad de tasks: \$PBS_TASKNUM" >> ${QSUB_FILE}
            echo "echo Home: \$PBS_O_HOME" >> ${QSUB_FILE}
            echo "echo Puerto del MOM: \$PBS_MOMPORT" >> ${QSUB_FILE}
            echo "echo Nombre del usuario: \$PBS_O_LOGNAME" >> ${QSUB_FILE}
            echo "echo Idioma: \$PBS_O_LANG" >> ${QSUB_FILE}
            echo "echo Cookie: \$PBS_JOBCOOKIE" >> ${QSUB_FILE}
            echo "echo Offset de numero de nodos: \$PBS_NODENUM" >> ${QSUB_FILE}
            echo "echo Shell: \$PBS_O_SHELL" >> ${QSUB_FILE}
            echo "#echo JobID: \$PBS_O_JOBID" >> ${QSUB_FILE}
            echo "echo Host: \$PBS_O_HOST" >> ${QSUB_FILE}
            echo "echo Cola de ejecucion: \$PBS_QUEUE" >> ${QSUB_FILE}
            echo "echo Archivo de nodos: \$PBS_NODEFILE" >> ${QSUB_FILE}
            echo "echo Path: \$PBS_O_PATH" >> ${QSUB_FILE}
            echo "cd \$PBS_O_WORKDIR" >> ${QSUB_FILE}
            echo "echo Current path: " >> ${QSUB_FILE}
            echo "pwd" >> ${QSUB_FILE}
            echo "echo Nodos:" >> ${QSUB_FILE}
            echo "cat \$PBS_NODEFILE" >> ${QSUB_FILE}
            
            echo "for (( i=0; i<50; i++ ))" >> ${QSUB_FILE}
            echo "do" >> ${QSUB_FILE}
            echo "${EXEC} ${GASA_CFG_FILE}" >> ${QSUB_FILE}
            echo "mv result/${RESULT_FILE} result/${RESULT_FILE}_\${i}" >> ${QSUB_FILE}
            echo "done" >> ${QSUB_FILE}
            
            echo "qsub ${QSUB_FILE}" >> ${RUN_ALL_SCRIPT}
            echo "sleep 1" >> ${RUN_ALL_SCRIPT}
        done
    done
done

chmod +x run_all.sh
