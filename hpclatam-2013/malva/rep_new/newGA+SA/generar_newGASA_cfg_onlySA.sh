#INST_PATH="/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
INST_PATH="/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/instances"
#CFG_FILE="/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/malva/rep_new/newGA+SA/newGA_eval.cfg"
CFG_FILE="newGA_onlySA.cfg"

SN=5
S[0]="10"
S[1]="20"
S[2]="30"
S[3]="50"

WN=5
W[0]="50"
W[1]="100"
W[2]="200"
W[3]="400"
WS[0]="10"
WS[1]="20"
WS[2]="20"
WS[3]="50"

mkdir cfg_SA

RESULT_PATH="result_SA"
mkdir ${RESULT_PATH}

for (( s_idx=0; s_idx <= 3; s_idx++ ))
do
        for (( w_idx=0; w_idx <= 3; w_idx++ ))
        do
                for (( wi_idx=1; wi_idx <= WN; wi_idx++ ))
                do
                    for (( si_idx=1; si_idx <= WN; si_idx++ ))
                    do
                        SCENARIO_FILE="s.${S[s_idx]}.${si_idx}"
                        WORKLOAD_FILE="w.i.${W[w_idx]}.${WS[w_idx]}.${wi_idx}"

                        ID="s.${S[s_idx]}.${si_idx}__w.${W[w_idx]}.${wi_idx}"

                        RESULT_FILE="${ID}.out"
                        GASA_CFG_FILE="cfg_SA/${ID}.cfg"

                        echo "Parameters" > ${GASA_CFG_FILE}
                        echo "1 // Selection Number" >> ${GASA_CFG_FILE}
                        echo "3 // Selection Parameter" >> ${GASA_CFG_FILE}
                        echo "20        // Number of solutions passed to SA" >> ${GASA_CFG_FILE}
                        echo "Configuration-Files" >> ${GASA_CFG_FILE}
                        echo "${CFG_FILE}" >> ${GASA_CFG_FILE}
                        echo "SA.cfg" >> ${GASA_CFG_FILE}
                        echo "Problem-Files" >> ${GASA_CFG_FILE}
                        echo "${INST_PATH}/c.pru.1" >> ${GASA_CFG_FILE}
                        echo "${INST_PATH}/scenarios/${SCENARIO_FILE}" >> ${GASA_CFG_FILE}
                        echo "${INST_PATH}/workloads/${WORKLOAD_FILE}" >> ${GASA_CFG_FILE}
                        echo "${RESULT_PATH}/${RESULT_FILE}" >> ${GASA_CFG_FILE}
                        done
                done
    done
done

