#INST_PATH="/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
INST_PATH="/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/instances"

W[0]="w.i.100.20.1"
S[0]="s.20.1"

W[1]="w.i.100.20.2"
S[1]="s.20.2"

C[0]="0.5"
C[1]="0.7"
C[2]="0.9"

M[0]="0.5"
M[1]="0.7"
M[2]="0.9"

LS[0]="0.1"
LS[1]="0.3"
LS[2]="0.5"

RESULT_PATH="result"
mkdir ${RESULT_PATH}

for (( c_idx=0; c_idx<3; c_idx++ ))
do
    for (( m_idx=0; m_idx<3; m_idx++ ))
    do
        for (( ls_idx=0; ls_idx<2; ls_idx++ ))
        do
            for (( s_idx=0; s_idx<2; s_idx++ ))
            do
                for (( w_idx=0; w_idx<2; w_idx++ ))
                do
                    ID="c${C[c_idx]}_m${M[m_idx]}_ls${LS[ls_idx]}__${S[s_idx]}__${W[w_idx]}"
                
                    RESULT_FILE="${ID}.out"
                    CFG_FILE="cfg/newGA_c${C[c_idx]}_m${M[m_idx]}_ls${LS[ls_idx]}.cfg"
                    GASA_CFG_FILE="cfg/newGASA_${ID}.cfg"
                    
                    echo "Parameters" > ${GASA_CFG_FILE}
                    echo "1	// Selection Number" >> ${GASA_CFG_FILE}
                    echo "3	// Selection Parameter" >> ${GASA_CFG_FILE}
                    echo "20	// Number of solutions passed to SA" >> ${GASA_CFG_FILE}
                    echo "Configuration-Files" >> ${GASA_CFG_FILE}
                    echo "${CFG_FILE}" >> ${GASA_CFG_FILE}
                    echo "SA.cfg" >> ${GASA_CFG_FILE}
                    echo "Problem-Files" >> ${GASA_CFG_FILE}
                    echo "${INST_PATH}/c.pru.1" >> ${GASA_CFG_FILE}
                    echo "${INST_PATH}/scenarios/${S[s_idx]}" >> ${GASA_CFG_FILE}
                    echo "${INST_PATH}/workloads/${W[w_idx]}" >> ${GASA_CFG_FILE}
                    echo "${RESULT_PATH}/${RESULT_FILE}" >> ${GASA_CFG_FILE}
                done
            done
        done
    done
done
