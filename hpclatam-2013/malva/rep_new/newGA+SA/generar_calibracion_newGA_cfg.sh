
C[0]="0.5"
C[1]="0.7"
C[2]="0.9"

M[0]="0.5"
M[1]="0.7"
M[2]="0.9"

LS[0]="0.1"
LS[1]="0.3"
LS[2]="0.5"

mkdir cfg
            
for (( c_idx=0; c_idx<3; c_idx++ ))
do
    for (( m_idx=0; m_idx<3; m_idx++ ))
    do
        for (( ls_idx=0; ls_idx<2; ls_idx++ ))
        do
            CFG_FILE="cfg/newGA_c${C[c_idx]}_m${M[m_idx]}_ls${LS[ls_idx]}.cfg"
        
            echo "1		// number of independent runs" > ${CFG_FILE}
            echo "4000   	// number of generations" >> ${CFG_FILE}
            echo "50		// number of individuals" >> ${CFG_FILE}
            echo "50		// size of offsprings in each generation" >> ${CFG_FILE}
            echo "1		// if replaces parents for offsprings, or only offsprings may be new parents" >> ${CFG_FILE}
            echo "0		// display state ?" >> ${CFG_FILE}
            echo "Selections	// selections to apply" >> ${CFG_FILE}
            echo "1 3		// selection of parents" >> ${CFG_FILE}
            echo "2 0		// selection of offsprings" >> ${CFG_FILE}
            echo "Intra-Operators	// operators to apply in the population" >> ${CFG_FILE}
            echo "0 ${C[c_idx]}		// crossover & its probability" >> ${CFG_FILE}
            echo "1 ${M[m_idx]} 0.1	// mutation & its probability" >> ${CFG_FILE}
            echo "2 ${LS[ls_idx]}		// hybrid operator & its probability" >> ${CFG_FILE}
            echo "Inter-Operators // operators to apply between this population and anothers" >> ${CFG_FILE}
            echo "0 50 5 1 3 1 5  // operator number, operator rate, number of individuals, selection of indidivual to send and remplace" >> ${CFG_FILE}
            echo "LAN-configuration" >> ${CFG_FILE}
            echo "500		// refresh global state" >> ${CFG_FILE}
            echo "1		// 0: running in asynchronized mode / 1: running in synchronized mode" >> ${CFG_FILE}
            echo "50		// interval of generations to check solutions from other populations" >> ${CFG_FILE}
        done
    done
done
