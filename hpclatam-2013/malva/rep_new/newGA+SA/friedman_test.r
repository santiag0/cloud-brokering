# Ver: http://stats.stackexchange.com/questions/10309/friedmans-test-and-post-hoc-analysis
bt <- scan("calibracion_big_table.txt");
Calibration <- data.frame(Profit = bt, Algorithm = factor(rep(1:18, 30)), Execution = factor(rep(1:30, rep(18, 30))));
library(agricolae) 
with(Calibration, friedman(Execution, Algorithm, Profit, group=FALSE)) 
