// Shortest Request to Cheapest Instance scheduler for VMPP.
// Assign the shortest request to the cheapest booked instance available.
// Parameters: <workload> <scenario> <workload_size> <scenario_size>
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <pthread.h>

#define NEG_INFT 		-9999999.0
#define INFT 			9999999999.0
#define NO_ASIG 		-1
#define SIZE_NOM_ARCH 	80

//#define DEBUG
//#define RANDOM
//#define QUIET

#define MAX_NFLOAT_ERROR    5000
#define MAX_ABSOLUTE_ERROR  1

/*
#define THREAD_COUNT 		2
#define ITER_COUNT 			1000000
#define TASK_NHOOD_FACTOR	0.2
#define MACH_NHOOD_FACTOR	0.2
* */

int THREAD_COUNT;
int ITER_COUNT;
float TASK_NHOOD_FACTOR;
float MACH_NHOOD_FACTOR;

using namespace std;

typedef struct _nodoT {
   int task_id;
   struct _nodoT *next;
} NodoT;

typedef struct _type_asig{
   int mach;
   float ST;
} asig_type;

typedef NodoT *pNodoT;
typedef NodoT *ListaT;

typedef struct _task {
    int id_task;
    float memory;
    float CPU;
    float storage;
    int num_cores;
    float data_transfer;
    float duration;
    float deadline;
    float arrival_time;
} task;

typedef struct _machine {
    int id_machine;
    float memory;
    float CPU;
    float storage;
    int num_cores;
    int type;
} machine;

// Number of instance types = 8
#define num_IT 8

// Costs: all costs hourly, as of April 2013

// Cost, prebooked intances (hourly basis - 3 years)
float C[num_IT] = {0.027,0.054,0.09,0.108,0.136,0.18,0.316,0.36};

// Cost, prebooked intances (hourly basis - 1 years)
float C1Y[num_IT] = {0.034,0.068,0.09,0.136,0.169,0.18,0.36,0.36};

// Cost, on-demand intances (hourly basis)
float COD[num_IT] = {0.06,0.12,0.12,0.24,0.24,0.41,0.48,0.58};

// Price charged by the broker: 20% less than the cost for on-demand intances (hourly basis)
float price[num_IT] = {0.048,0.096,0.096,0.192,0.192,0.328,0.384,0.464};

// Fixed cost price 3Y
float FC[num_IT] = {96.0,192.0,0.0,384.0,398.0,0.0,972.0,0};

// data transfer cost (per GB)
float DT[num_IT] = {0.12,0.12,0.0894,0.12,0.12,0.0894,0.12,0.0894};

// Scenario properties, needed to compute the cheapest instance
float Memory[num_IT] = {1.7,3.75,3.5,7.5,17.1,7.0,7.0,14.0};
float Storage[num_IT] = {160.0,410.0,489.0,850.0,420.0,999.0,1690.0,2039.0};
float CPU[num_IT] = {1.0,2.0,1.6,2.0,3.25,1.6,2.5,1.6};
float Cores[num_IT] = {1,1,2,2,2,4,8,8};

// Scenario description
int NT, NM;
machine *ML;
task *TL;
int *BF;

// Solution thread-oriented variables
float **thread_mach;
asig_type **thread_sol;
int ***thread_solM;
int **thread_MC;
float *thread_profit;

float *thread_best_profit;
int *thread_best_iter;
asig_type **thread_best_sol;

unsigned int *seeds;

void printlista(ListaT l);
void insertlista(ListaT *l, int t);
void crearlista(ListaT *l, int NT);
void deletelista(ListaT *l, int t);
void local_search(int thread_id);
float compute_profit(int thread_id);
void display_solution(int thread_id);
void validar(int thread_id);
void * compute_thread(void *data);
bool AlmostEqual2sComplement(float A, float B, int maxUlps, float maxAbsError);

float run_init_heuristic(int thread_id, float *mach, asig_type *sol, int **solM, int *MC) {
	int i, j;

	int nro_asig=0;
	for (i=0;i<NT;i++){
		sol[i].mach = NO_ASIG;
		sol[i].ST = -1.0;
	}

	float total_profit = 0.0;
	float delta_profit = 0.0;

	ListaT l = NULL;

	crearlista(&l,NT);

	float profit_task, max_profit, min_time;
	int current_task, best_task, MMP_task, best_machine, best_machine_task;
	int violated = 0;

	ListaT temp_l = NULL;

	while ((nro_asig < NT) && (l != NULL)){
		// Select unassigned task with maximum profit
		MMP_task = -1;
		best_machine = -1;
		min_time = INFT;
		temp_l = l;

		// Loop in tasks
		while (temp_l != NULL){
			max_profit = NEG_INFT;
			best_machine_task = -1;
			best_task = -1;
			current_task = temp_l->task_id;

			// Loop on machines search for maximum profit
			for (j=0;j<NM;j++){
				if ( (TL[current_task].memory <= ML[j].memory) & (TL[current_task].CPU <= ML[j].CPU) & (TL[current_task].storage <= ML[j].storage) & (TL[current_task].num_cores <= ML[j].num_cores) ){
					if (mach[j] <  TL[current_task].deadline) {
						// deadline fullfilled
						profit_task = TL[current_task].duration * (price[BF[current_task]] - C[ML[j].type]);
					} else {
						// deadline violated
						profit_task = TL[current_task].duration * (price[BF[current_task]] - COD[BF[current_task]]);
					}
					if ( profit_task > max_profit ) {
						max_profit = profit_task;
						best_machine_task = j;
						best_task = current_task;
					}
				}
			}

			if ( mach[best_machine_task] < min_time ) {
				min_time = mach[best_machine_task];
				best_machine = best_machine_task;
				MMP_task = best_task;
			}

			temp_l = temp_l->next;
		}

		if (mach[best_machine] <= TL[MMP_task].deadline) {
			// deadline is not violated
			sol[MMP_task].mach = best_machine;
			sol[MMP_task].ST = mach[best_machine];
			mach[best_machine]+=TL[MMP_task].duration;
			delta_profit = TL[MMP_task].duration * (price[BF[MMP_task]] - C[ML[best_machine].type]);
			
			solM[best_machine][MC[best_machine]] = MMP_task;
			MC[best_machine]++;
		} else {
			// deadline violated
			delta_profit = TL[MMP_task].duration * (price[BF[MMP_task]] - COD[BF[MMP_task]]);
			violated++;
			
			sol[MMP_task].mach = -1;
			sol[MMP_task].ST = 0;	
		}
		total_profit += delta_profit;

		// l = l->next;
		deletelista(&l,MMP_task);

	}
	
	return total_profit;
}

int main(int argc, char *argv[]){
	if (argc < 9){
			printf("Sintaxis: %s <workload> <scenario> <workload_size> <scenario_size> <thread_num> <iter_num> <task_nhood_factor> <mach_nhood_factor>\n", argv[0]);
			exit(1);
	}

	FILE *wf, *sf;

	char *workload_file, *scenario_file;
	workload_file = (char *)malloc(sizeof(char)*120);
	scenario_file = (char *)malloc(sizeof(char)*120);

	strcpy(workload_file,argv[1]);
	strcpy(scenario_file,argv[2]);

	#ifndef QUIET
	printf("Workload: %s, scenario: %s\n",workload_file,scenario_file);
	#endif

	if((wf=fopen(workload_file, "r"))==NULL){
		printf("Cannot read workload file: %s\n",workload_file);
		exit(1);
	}
	if((sf=fopen(scenario_file, "r"))==NULL){
		printf("Cannot read scenario file: %s\n",scenario_file);
		exit(1);
	}

	NT = atoi(argv[3]);
	NM = atoi(argv[4]);
	#ifndef QUIET
	printf("NT: %d, NM: %d\n",NT,NM);
	#endif

	THREAD_COUNT = atoi(argv[5]);
	ITER_COUNT = atoi(argv[6]);
	TASK_NHOOD_FACTOR = atof(argv[7]);
	MACH_NHOOD_FACTOR = atof(argv[8]);

	#ifndef QUIET
	printf("Thread count                : %d\n", THREAD_COUNT);
	printf("Iteration count             : %d\n", ITER_COUNT);
	printf("Task neighbourhood factor   : %.2f\n", TASK_NHOOD_FACTOR);
	printf("Machine neighbourhood factor: %.2f\n", MACH_NHOOD_FACTOR);
	#endif

	BF = (int *) malloc(sizeof(int)*NT);
	if (BF == NULL){
		printf("Error in BF malloc, dimension %d\n",NT);
		exit(2);
	}

	// Read instance files
	int i,j;

	ML = (machine *) malloc(sizeof(machine)*NM);
	if (ML == NULL){
		printf("Error in ML malloc, dimension %d\n",NM);
		exit(2);
	}

	for (j=0;j<NM;j++){
		ML[j].id_machine = j;
		fscanf(sf,"%f %f %f %d %d",&(ML[j].memory),&(ML[j].CPU),&(ML[j].storage),&(ML[j].num_cores),&(ML[j].type));
	}

	fclose(sf);

	// Machine array, stores MET
	TL = (task *) malloc(sizeof(task)*NT);
	if (TL == NULL){
		printf("Error in TL malloc, dimension %d\n",NT);
		exit(2);
	}

	for (i = 0; i < NT; i++){
		TL[i].id_task = i;
		fscanf(wf,"%f %f %f %d %f %f %f %f",&(TL[i].memory),&(TL[i].CPU),&(TL[i].storage),&(TL[i].num_cores),&(TL[i].data_transfer),&(TL[i].duration),&(TL[i].deadline),&(TL[i].arrival_time));

		// Search for Best Fit machine
		BF[i] = NO_ASIG;
		for (j = 0; j < num_IT; j++){
			if ( (Cores[j] >= TL[i].num_cores) && (CPU[j] >= TL[i].CPU) && (Storage[j] >= TL[i].storage) && (Memory[j] >= TL[i].memory) ){
				BF[i] = j;
				break;
			}
		}
		if (BF[i] == NO_ASIG){
			printf("No BF assignment for request %d\n",i);
			printf("task %d, requires: mem: %.2f, CPU: %.2f, storage: %.2f, num_cores_ %d\n",i,TL[i].memory,TL[i].CPU,TL[i].storage,TL[i].num_cores);
			exit(3);
		}
	}

	fclose(wf);

	thread_mach = (float **) malloc(sizeof(float *) * THREAD_COUNT);
	thread_solM = (int ***) malloc(sizeof(int *) * THREAD_COUNT);
	thread_MC = (int **) malloc(sizeof(int *) * THREAD_COUNT);
	thread_sol = (asig_type **) malloc(sizeof(asig_type *) * THREAD_COUNT);
	thread_profit = (float *) malloc(sizeof(float) * THREAD_COUNT);
	
	thread_best_sol = (asig_type **) malloc(sizeof(asig_type *) * THREAD_COUNT);
	thread_best_profit = (float *) malloc(sizeof(float) * THREAD_COUNT);
	thread_best_iter = (int *) malloc(sizeof(int) * THREAD_COUNT);

	seeds = (unsigned int *) malloc(sizeof(int) * THREAD_COUNT);

	for (i = 0; i < THREAD_COUNT; i++) {
		thread_mach[i] = (float *) malloc(sizeof(float)*NM);
		if (thread_mach[i] == NULL){
			printf("Error al reservar memoria para mach, dimension %d\n",NT);
			exit(2);
		}

		for (j = 0; j < NM; j++){
			thread_mach[i][j] = 0.0;
		}

		// Estructura bi-dimensional con la cola de tareas de cada máquina.
		thread_solM[i] = (int **) malloc(sizeof(int *) * NM);
		if (thread_solM[i] == NULL) {
			printf("Error in solution matrix malloc, dimension %d\n", NM);
			exit(2);
		}
		for (j = 0; j < NM; j++) {
			thread_solM[i][j] = (int *) malloc(sizeof(int) * NT);
			
			if (thread_solM[i][j] == NULL) {
				printf("Error in solution matrix [%d,%d] malloc, dimension %d\n", i, j, NT);
				exit(2);
			}
		}

		// Cantidad de tareas en cada máquina.
		thread_MC[i] = (int *) malloc(sizeof(int) * NM);
		if (thread_MC[i] == NULL) {
			printf("Error in task count malloc, dimension %d\n", NM);
			exit(2);
		}
		for (j = 0; j < NM; j++) {
			thread_MC[i][j] = 0;
		}

		// Array of task-machine assignments
		thread_sol[i] = (asig_type *) malloc(sizeof(asig_type) * NT);
		if (thread_sol[i] == NULL){
			printf("Error in solution malloc, dimension %d\n",NT);
			exit(2);
		}
				
		thread_best_sol[i] = (asig_type *) malloc(sizeof(asig_type) * NT);
		if (thread_best_sol[i] == NULL) {
			printf("Error in solution malloc, dimension %d\n", NT);
			exit(2);
		}
				
		#ifdef RANDOM
			seeds[i] = time(NULL) + i;
		#else
			seeds[i] = i;
		#endif
	}

	thread_profit[0] = run_init_heuristic(0, thread_mach[0], thread_sol[0], thread_solM[0], thread_MC[0]);

	#ifdef DEBUG
		cout << "Profit thread 0 = " << compute_profit(0) << endl;
		display_solution(0);
	#endif

	for (i = 1; i < THREAD_COUNT; i++) {
		memcpy(thread_mach[i], thread_mach[0], sizeof(int) * NM);
		memcpy(thread_sol[i], thread_sol[0], sizeof(asig_type) * NT);
		memcpy(thread_best_sol[i], thread_sol[0], sizeof(asig_type) * NT);
		
		for (j = 0; j < NM; j++) {
			memcpy(thread_solM[i][j], thread_solM[0][j], sizeof(int) * NT);
		}
		
		memcpy(thread_MC[i], thread_MC[0], sizeof(int) * NM);
		
		thread_profit[i] = thread_profit[0];
		thread_best_profit[i] = thread_profit[0];
		thread_best_iter[i] = 0;

		#ifdef DEBUG
			cout << "Profit thread " << i << " = " << compute_profit(i) << endl;
			display_solution(i);
			validar(i);	
			assert(AlmostEqual2sComplement(compute_profit(i), thread_profit[0], MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));
		#endif
	}

	float starting_profit = thread_profit[0];
	
	#ifndef QUIET
	printf(">> Starting profit: %.2f (%.2f)\n", starting_profit, compute_profit(0));
	display_solution(0);
	validar(0);
	#endif

	int best_thread = 0;

	if (THREAD_COUNT > 1) {
		#ifndef QUIET
		cout << "Initializing threads... " << endl;
		#endif
		pthread_t threads[THREAD_COUNT];
		pthread_attr_t attr;
		void *status;

		/* Initialize and set thread detached attribute */
		pthread_attr_init(&attr);
		pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

		int rc;
		for(long t = 0; t < THREAD_COUNT; t++){
			#ifndef QUIET
			printf("Creating thread %d\n", t);
			#endif
			
			rc = pthread_create(&threads[t], &attr, compute_thread, (void *)t);
			if (rc){
				printf("ERROR; return code from pthread_create() is %d\n", rc);
				exit(-1);
			}
		}

		/* Free attribute and wait for the other threads */	
		for(int t = 0; t < THREAD_COUNT; t++) {
			rc = pthread_join(threads[t], &status);
		
			if (rc) {
				printf("ERROR; return code from pthread_join() is %d\n", rc);
				exit(-1);
			}

			#ifndef QUIET
			printf("Main: completed join with thread %ld having a status of %ld\n",t,(long)status);
			validar(t);
			#endif		
			
			if (thread_best_profit[t] > thread_best_profit[best_thread]) {
				best_thread = t;
			}
			
			#ifndef QUIET
			printf(">> Final profit of thread %d: %.2f (%.2f)\n", t, thread_best_profit[best_thread], compute_profit(best_thread));	
			#endif
		}
		
		pthread_attr_destroy(&attr);
	} else {
		compute_thread((void *)0);
		
		#ifndef QUIET
		validar(0);
		#endif
		
		best_thread = 0;
	}
	
	// Update best thread current solution
	memcpy(thread_sol[best_thread], thread_best_sol[best_thread], sizeof(asig_type) * NT);
	
	// Update best thread mach array
	for (i = 0; i < NM; i++) {
		thread_mach[best_thread][i] = 0;
		thread_MC[best_thread][i] = 0;
	}
	
	for (i = 0; i < NT; i++) {
		int assigned_mach = thread_best_sol[best_thread][i].mach;
		float starting_time = thread_best_sol[best_thread][i].ST;
		
		if (assigned_mach >= 0) {
			thread_MC[best_thread][assigned_mach]++;
			
			if (starting_time + TL[i].duration > thread_mach[best_thread][assigned_mach]) {
				thread_mach[best_thread][assigned_mach] = starting_time + TL[i].duration;
			}
		}
	}
	
	#ifndef QUIET
	printf(">> Final profit -> best=%.2f current=%.2f\n", thread_best_profit[best_thread], thread_profit[best_thread]);
	#endif
	
	// Cannot display solution since thread_solM matrix is not up to date
	// display_solution(best_thread);
	
	float makespan = 0.0;
	for (j = 0; j < NM; j++){
		#ifndef QUIET
		printf("M[%d]: %.2f\n", j, thread_mach[best_thread][j]);
		#endif
		
		if (thread_mach[best_thread][j] > makespan){
			makespan = thread_mach[best_thread][j];
		}
	}

	float flowtime = 0.0;
	int violated = 0;

	#ifndef QUIET
	printf("Solution: [\n");
	#endif
	for (i=0; i < NT; i++){
		#ifndef QUIET
		fprintf(stdout,"%d %d %.2f \n", i, 
			thread_sol[best_thread][i].mach, thread_sol[best_thread][i].ST);
		#endif
		
		if (thread_sol[best_thread][i].ST > 0) {
			flowtime += (thread_sol[best_thread][i].ST + TL[i].duration);
		} else {
			flowtime += TL[i].duration;
		}
		
		if (thread_sol[best_thread][i].mach == -1) {
			violated++;
		}
	}
	#ifndef QUIET
	printf("]\n\n");
	#endif

	#ifndef QUIET
	printf("%s Profit improved from: %.2f to %.2f (best %.2f in iteration %d), makespan: %.2f, flowtime: %.2f, violated: %d\n", argv[0],
		starting_profit, thread_profit[best_thread], thread_best_profit[best_thread], thread_best_iter[best_thread],
		makespan, flowtime, violated);
	#else
	printf("%s\t%f\t%f\t%d\t%f\t%f\t%d\n", argv[0], starting_profit, thread_best_profit[best_thread], thread_best_iter[best_thread], makespan, flowtime, violated);
	#endif
}

void* compute_thread(void* data) {
	int thread_id = (long)data;
	
	#ifdef DEBUG
		cout << "THREAD ID = " << thread_id << endl;
	#endif
	
	for (int i = 0; i < ITER_COUNT; i++) {	
		#ifdef DEBUG
			printf(">> Iteration %d\n", i);
			printf("total_profit %f\n", thread_profit[thread_id]);
		#endif
		#ifdef DEBUG
			display_solution(thread_id);
		#endif
		local_search(thread_id);
		
		if (thread_profit[thread_id] > thread_best_profit[thread_id]) {
			thread_best_profit[thread_id] = thread_profit[thread_id];
			thread_best_iter[thread_id] = i;
			
			memcpy(thread_best_sol[thread_id], thread_sol[thread_id], sizeof(asig_type) * NT);
		}
		
		#ifdef DEBUG
			display_solution(thread_id);
			validar(thread_id);
			thread_profit[thread_id] = compute_profit(thread_id);
		#endif
	}
}

void display_solution(int thread_id) {	
	printf(">> Detailed queue:\n");
	for (int i = 0; i < NM; i++) {
		printf("[%d] ", i);
		
		for (int j = 0; j < thread_MC[thread_id][i]; j++) {
			int aux_tid;
			aux_tid = thread_solM[thread_id][i][j];
			
			printf("<%d:%.2f> ", aux_tid, TL[aux_tid].duration * (price[ML[i].type] - C[ML[i].type]));
			
			assert(thread_sol[thread_id][aux_tid].mach == i);
			assert(thread_sol[thread_id][aux_tid].ST <= TL[aux_tid].deadline);
		}
		
		printf("\n");
	}
	printf("[OD] ");
	
	for (int i = 0; i < NT; i++) {
		if (thread_sol[thread_id][i].mach == -1) {
			printf("<%d:%.2f> ", i, TL[i].duration * (price[BF[i]] - COD[BF[i]]));
		}
	}
	
	printf("\n\n");	
}

float compute_profit(int thread_id) {	
	float aux_profit = 0.0;
	float aux_delta_profit;
	int mach_id;
	
	for (int i=0; i < NT; i++) {
		if (thread_sol[thread_id][i].mach >= 0) {
			// is booked
			aux_delta_profit = TL[i].duration * (price[ML[thread_sol[thread_id][i].mach].type] - C[ML[thread_sol[thread_id][i].mach].type]);
		} else {
			// is on-demand
			aux_delta_profit = TL[i].duration * (price[BF[i]] - COD[BF[i]]);
		}
		
		aux_profit += aux_delta_profit;
	}
	
	return aux_profit;
}

void move_task_to_ondemand(int thread_id, int t_id) {	
	thread_sol[thread_id][t_id].mach = -1;
	thread_sol[thread_id][t_id].ST = 0;

	thread_profit[thread_id] += TL[t_id].duration * (price[BF[t_id]] - COD[BF[t_id]]);
	
	#ifdef DEBUG
		cout << endl << "[[move_task_to_ondemand]] total_profit add to ondemand " << thread_profit[thread_id] << "[" <<
			(price[BF[t_id]] - COD[BF[t_id]]) * TL[t_id].duration << "]" << endl << endl;
	#endif
}

bool can_exec_task(int m_id, int t_id) {
	int m_type = ML[m_id].type;
	
	if ((Cores[m_type] >= TL[t_id].num_cores) && 
		(CPU[m_type] >= TL[t_id].CPU) && 
		(Storage[m_type] >= TL[t_id].storage) && 
		(Memory[m_type] >= TL[t_id].memory)) {
			
		return true;
	} else {
		return false;
	}	
}

// Usable AlmostEqual function
bool AlmostEqual2sComplement(float A, float B, int maxUlps, float maxAbsError) {   
    // Make sure maxUlps is non-negative and small enough that the
    // default NAN won't compare as equal to anything.
    assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024);
    
    if ((A == 0) && ((B < 0.01) && (B > -0.01))) return true;
    if ((B == 0) && ((A < 0.01) && (A > -0.01))) return true;
    
    int aInt = *(int*)&A;
    // Make aInt lexicographically ordered as a twos-complement int
    if (aInt < 0)
        aInt = 0x80000000 - aInt;
    // Make bInt lexicographically ordered as a twos-complement int
    int bInt = *(int*)&B;
    if (bInt < 0)
        bInt = 0x80000000 - bInt;
    int intDiff = abs(aInt - bInt);
    if (intDiff <= maxUlps)
        return true;
 
    if (A-B <= maxAbsError)
        return true;
 
    #ifdef DEBUG
        std::cout << "abs error = " << A << "-" << B << " = " << (A-B) << " (diff=" << intDiff << ")" << std::endl;
    #endif
        
    return false;
}

void validar(int thread_id) {
	float *mach = thread_mach[thread_id];
	asig_type *sol = thread_sol[thread_id];
	int **solM = thread_solM[thread_id];
	int *MC = thread_MC[thread_id];
	float total_profit = thread_profit[thread_id];
		
	cout << "[DEBUG] Verifying..." << endl;

	for (int i = 0; i < NM; i++) {
		assert(MC[i] >= 0);
	}

	int prebooked_tasks = 0;
	int prebooked_tasks_aux = 0;
	int ondemand_tasks = 0;

	for (int i = 0; i < NM; i++) {
		for (int j = 0; j < MC[i]; j++) {
			assert(solM[i][j] >= 0);
			assert(solM[i][j] < NT);
			assert(sol[solM[i][j]].mach == i);
		}

		prebooked_tasks += MC[i];
	}

	for (int i = 0; i < NM; i++) {
		if (MC[i] > 0) {
			#ifdef DEBUG
				if (!AlmostEqual2sComplement(sol[solM[i][0]].ST, 0, MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR)) {
					cout << "))(( valid["<< i << ",0] " << sol[solM[i][0]].ST << endl;
				}
			#endif
			
			if (!AlmostEqual2sComplement(sol[solM[i][0]].ST, 0, MAX_NFLOAT_ERROR, MAX_ABSOLUTE_ERROR)) {
				#ifdef DEBUG
					cout << "m_id=" << i << " stime=" << sol[solM[i][0]].ST 
						<< " scenario_start=" << 0 << endl;
				#endif
			}
			
			assert(AlmostEqual2sComplement(sol[solM[i][0]].ST, 0, MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));
			assert(can_exec_task(i, solM[i][0]));

			for (int j = 1; j < MC[i]; j++) {
				#ifdef DEBUG
					if (!AlmostEqual2sComplement(
						sol[solM[i][j]].ST,
						sol[solM[i][j-1]].ST + TL[solM[i][j-1]].duration,
						MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR)) {

						cout << "))(( valid["<< i << "," << j << "] " << sol[solM[i][j]].ST << " <-> " <<
							sol[solM[i][j-1]].ST + TL[solM[i][j-1]].duration << endl;
					}
				#endif

				assert(AlmostEqual2sComplement(
					sol[solM[i][j]].ST,
					sol[solM[i][j-1]].ST + TL[solM[i][j-1]].duration,
					MAX_NFLOAT_ERROR,MAX_ABSOLUTE_ERROR));

				assert(can_exec_task(i, solM[i][j]));
			}
		}
	}
	
	for (int i = 0; i < NT; i++) {
		if (sol[i].mach == -1) {
			ondemand_tasks++;
		} else {
			prebooked_tasks_aux++;
		}
	}
	
	assert(ondemand_tasks >= 0);
	assert(ondemand_tasks <= NT);
	assert(prebooked_tasks == prebooked_tasks_aux);
	assert(ondemand_tasks + prebooked_tasks == NT);

	for (int j = 0; j < NT; j++) {
		if (sol[j].mach >= 0) {
			assert(can_exec_task(sol[j].mach, j));
		} else {
			assert(sol[j].ST == 0);
		}
	}

	assert(AlmostEqual2sComplement(total_profit, compute_profit(thread_id), MAX_NFLOAT_ERROR, MAX_ABSOLUTE_ERROR));

	cout << "[DEBUG] Solution verified." << endl;
}

void local_search(int thread_id) {
	float *mach = thread_mach[thread_id];
	asig_type *sol = thread_sol[thread_id];
	int **solM = thread_solM[thread_id];
	int *MC = thread_MC[thread_id];

	bool move_to_ondemand;
	move_to_ondemand = false;
	
	int worst_task_id;
	worst_task_id = rand_r(&(seeds[thread_id])) % NT;

	float worst_profit;
	bool worst_ondemand;

	int worst_mach_id;
	worst_mach_id = sol[worst_task_id].mach;

	if (worst_mach_id == -1) {
		worst_profit = TL[worst_task_id].duration * (price[BF[worst_task_id]] - COD[BF[worst_task_id]]);
		worst_ondemand = true;
	} else {
		worst_profit = TL[worst_task_id].duration * (price[ML[worst_mach_id].type] - C[ML[worst_mach_id].type]);
		worst_ondemand = false;
			
		move_to_ondemand = (COD[BF[worst_task_id]] <= C[ML[worst_mach_id].type]);
	}

	if (!move_to_ondemand) {
		int aux_worst_task_id, aux_worst_mach_id;
		float aux_worst_profit;

		aux_worst_task_id = worst_task_id;
		
		int ls_neigh_size;
		//ls_neigh_size = NT / 10;
		ls_neigh_size = NT * TASK_NHOOD_FACTOR;
		if (ls_neigh_size < 2) ls_neigh_size = 2;

		for (int i = 0; (i < ls_neigh_size) && (!move_to_ondemand); i++) {
			aux_worst_task_id = (aux_worst_task_id+1) % NT;
			aux_worst_mach_id = sol[aux_worst_task_id].mach;

			if (aux_worst_mach_id == -1) {
				aux_worst_profit = TL[aux_worst_task_id].duration * (price[BF[aux_worst_task_id]] - COD[BF[aux_worst_task_id]]);
			} else {
				aux_worst_profit = TL[aux_worst_task_id].duration * (price[ML[aux_worst_mach_id].type] - C[ML[aux_worst_mach_id].type]);
								
				move_to_ondemand = (COD[BF[aux_worst_task_id]] <= C[ML[aux_worst_mach_id].type]);
				
				if (move_to_ondemand) {
					worst_profit = aux_worst_profit;
					worst_task_id = aux_worst_task_id;
					worst_mach_id = aux_worst_mach_id;
					worst_ondemand = false;
				}
			}

			if (aux_worst_profit < worst_profit) {
				worst_profit = aux_worst_profit;
				worst_task_id = aux_worst_task_id;
				worst_mach_id = aux_worst_mach_id;

				worst_ondemand = (sol[aux_worst_task_id].mach == -1);
			}
		}
	}

	#ifdef DEBUG
		cout << "Peor task [ONDEMAND="<< worst_ondemand << "]: worst_task_id=" << worst_task_id << " worst_mach_id=" << worst_mach_id << endl;
	#endif
	
	// Tengo el peor profit
	bool eval_ok = move_to_ondemand;
	if ((!eval_ok) && (!worst_ondemand)) {
		assert(worst_mach_id >= 0);
		eval_ok = (COD[BF[worst_task_id]] <= C[ML[worst_mach_id].type]);
	}
		
	if ((eval_ok) && (worst_mach_id >= 0)) {
		#ifdef DEBUG
			cout << "Muevo la tarea a una máquina ondemand." << endl;
		#endif

		// Muevo la tarea a una máquina ondemand.
		int t_pos;
		for (t_pos = 0; (solM[worst_mach_id][t_pos] != worst_task_id) && (t_pos < MC[worst_mach_id]); t_pos++) {}

		#ifdef DEBUG
			assert(solM[worst_mach_id][t_pos] == worst_task_id);
		#endif

		float starting_time;
		starting_time = sol[worst_task_id].ST;

		move_task_to_ondemand(thread_id, worst_task_id);

		thread_profit[thread_id] -= TL[worst_task_id].duration * (price[ML[worst_mach_id].type] - C[ML[worst_mach_id].type]);

		int curr_id;
		if (MC[worst_mach_id] > t_pos+1) {
			for (int curr_pos = t_pos; curr_pos+1 < MC[worst_mach_id]; curr_pos++) {
				curr_id = solM[worst_mach_id][curr_pos+1];

				solM[worst_mach_id][curr_pos] = curr_id;
				sol[curr_id].ST = starting_time;

				starting_time += TL[curr_id].duration; 
			}
		}

		MC[worst_mach_id]--;

	} else {
		#ifdef DEBUG
			cout << "Muevo la tarea a una máquina prebooked." << endl;
		#endif
		
		int dst_m_id;
		dst_m_id = rand_r(&(seeds[thread_id])) % NM;

		bool found_best;
		found_best = false;

		int ls_neigh_size;
		//ls_neigh_size = NM / 2;
		ls_neigh_size = NM * MACH_NHOOD_FACTOR;
		if (ls_neigh_size < 2) ls_neigh_size = 2;

		for (int i = 0; (i < ls_neigh_size) && (!found_best); i++) {
			if (can_exec_task(dst_m_id, worst_task_id)) {
				if (worst_ondemand) {
					if (C[ML[dst_m_id].type] < COD[BF[worst_task_id]]) {
						found_best = true;
					}
				} else {
					if (C[ML[dst_m_id].type] < C[ML[worst_mach_id].type]) {
						found_best = true;
					}
				}
			}

			if (!found_best) {
				dst_m_id++;
				if (dst_m_id >= NM) dst_m_id = 0;
			}
		}
		
		if (found_best) {
			#ifdef DEBUG
				assert(can_exec_task(dst_m_id, worst_task_id));
			#endif
			
			int dst_m_type = ML[dst_m_id].type;
			float dst_stime;

			int dst_pos = -1;
			
			if (MC[dst_m_id] > 0) {
				if (sol[solM[dst_m_id][MC[dst_m_id]-1]].ST + TL[solM[dst_m_id][MC[dst_m_id]-1]].duration
						<= TL[worst_task_id].deadline) {

					dst_pos = MC[dst_m_id];
					dst_stime = sol[solM[dst_m_id][MC[dst_m_id]-1]].ST + TL[solM[dst_m_id][MC[dst_m_id]-1]].duration;
				} else {
					for (dst_pos = MC[dst_m_id]-1; dst_pos >= 0; dst_pos--) {
						if (sol[solM[dst_m_id][dst_pos]].ST <= TL[worst_task_id].deadline) {
							dst_stime = sol[solM[dst_m_id][dst_pos]].ST;
							break;
						}
					}
				}
			} else {
				dst_stime = 0;
			}
			
			#ifdef DEBUG
				cout << "** Muevo a dst_m_id=" << dst_m_id << " en dst_pos=" << dst_pos << " con dst_stime=" << dst_stime << endl;
				for (int i = 0; i < MC[dst_m_id]; i++) {
					cout << solM[dst_m_id][i] << " ";
				}
				cout << endl;
			#endif

			if (dst_pos >= 0) {
				// Actualizo la máquina destino.
				if (dst_pos < MC[dst_m_id]) {
					float delta_stime;
					delta_stime = TL[worst_task_id].duration;

					int current_pos = dst_pos;
					int current_count = MC[dst_m_id];

					for (int i = dst_pos; i < current_count; i++) {
						if (delta_stime > 0) {
							int dst_t_id;
							dst_t_id = solM[dst_m_id][i];

							if (sol[dst_t_id].ST + delta_stime <= TL[dst_t_id].deadline) {
								sol[dst_t_id].ST += delta_stime;

								if (i > current_pos) {
									solM[dst_m_id][current_pos] = solM[dst_m_id][i];
								}

								current_pos++;
							} else {
								move_task_to_ondemand(thread_id, dst_t_id);

								delta_stime -= TL[dst_t_id].duration;

								#ifdef DEBUG
									cout << " to " << delta_stime << endl;
								#endif

								MC[dst_m_id]--;
								
								#ifdef DEBUG
									cout << "total_profit [overflow] remove from profit m_id:" << dst_m_id 
										<< " t_id:" << dst_t_id << endl;
								#endif
								
								thread_profit[thread_id] -= TL[dst_t_id].duration * 
									(price[ML[dst_m_id].type] - C[ML[dst_m_id].type]);
									
								#ifdef DEBUG
									cout << "total_profit [overflow] remove from profit " << thread_profit[thread_id] << "[" <<
										(price[ML[dst_m_id].type] - C[ML[dst_m_id].type]) * TL[dst_t_id].duration << "]" << endl;
								#endif
							}
						} else {
							if (i > current_pos) {
								solM[dst_m_id][current_pos] = solM[dst_m_id][i];
								sol[solM[dst_m_id][current_pos]].ST += delta_stime;
							}

							current_pos++;
						}
					}

					for (int i = MC[dst_m_id]-1; i >= dst_pos; i--) {
						solM[dst_m_id][i+1] = solM[dst_m_id][i];
					}
				}

				#ifdef DEBUG
					cout << "(CURRENT) sol[" << worst_task_id << "].mach = " << sol[worst_task_id].mach << endl;
					printf("To machine %d (type %d [%d])\n", dst_m_id, ML[dst_m_id].type, dst_m_type);
				#endif

				// Actualizo la tarea movida.
				sol[worst_task_id].ST = dst_stime;
				sol[worst_task_id].mach = dst_m_id;
				solM[dst_m_id][dst_pos] = worst_task_id;
				MC[dst_m_id]++;
				
				#ifdef DEBUG
					cout << "_task_assign[" << dst_m_id << "][" << dst_pos << "] = " << worst_task_id << endl;
					cout << "_machine_assign[" << worst_task_id << "] = " << dst_m_id << endl;
				#endif

				// Actualizo el fitness de la máquina destino.
				assert(worst_task_id >= 0);
				
				#ifdef DEBUG
					cout << "total_profit current " << thread_profit[thread_id] << endl;
				#endif
				
				thread_profit[thread_id] += (price[dst_m_type] - C[dst_m_type]) * TL[worst_task_id].duration;

				#ifdef DEBUG
					cout << "total_profit add to profit " << thread_profit[thread_id] << "[" << 
						(price[dst_m_type] - C[dst_m_type]) * TL[worst_task_id].duration << "]" << endl;
				#endif

				if (worst_ondemand == 1) {
					// El peor esta ondemand
					
					#ifdef DEBUG
						cout << "El peor esta ondemand." << endl;
					#endif

					thread_profit[thread_id] -= (price[BF[worst_task_id]] - COD[BF[worst_task_id]]) * TL[worst_task_id].duration;
			
					#ifdef DEBUG
						cout << "total_profit remove from ondemand " << thread_profit[thread_id] << "[" <<
							(price[BF[worst_task_id]] - COD[BF[worst_task_id]]) * TL[worst_task_id].duration << "]" << endl;
					#endif
			
				} else {
					// El peor esta prebooked
					
					#ifdef DEBUG
						cout << "El peor esta prebooked." << endl;
					#endif
					
					int m_type;
					m_type = ML[worst_mach_id].type;

					// Actualizo el fitness de la máquina origen.
					thread_profit[thread_id] -= (price[m_type] - C[m_type]) * TL[worst_task_id].duration;

					#ifdef DEBUG
						cout << "total_profit remove from profit t_id:" << worst_task_id << " m_id:" << worst_mach_id << endl;

						cout << "total_profit remove from profit " << thread_profit[thread_id] << "[" <<
							(price[m_type] - C[m_type]) * TL[worst_task_id].duration << "]" << endl;
					#endif

					int t_pos = -1;

					for (int i = 0; i < MC[worst_mach_id]; i++) {
						if (solM[worst_mach_id][i] == worst_task_id) {
							t_pos = i;
						} else {
							if (t_pos >= 0) {
								solM[worst_mach_id][i-1] = solM[worst_mach_id][i];

								if (i-2 >= 0) {
									int prev_prev_id;
									prev_prev_id = solM[worst_mach_id][i-2];

									sol[solM[worst_mach_id][i]].ST =
										sol[prev_prev_id].ST + TL[prev_prev_id].duration;
								} else {
									sol[solM[worst_mach_id][i]].ST = 0; 
								}
							}
						}
					}

					MC[worst_mach_id]--;
				}
			}
		}
	}
}

// Task list type
void printlista(ListaT l){
    // Print list
    ListaT ln = l;
    printf("[");
    while (ln != NULL){
        printf("%d ",ln->task_id);
        ln = ln->next;
    }
    printf("]\n");
}

void insertlista(ListaT *l, int t){
    // Insert node with id_task t at the end of list l.

    pNodoT pn;
    pn = (pNodoT) malloc(sizeof(NodoT));
    pn->task_id = t;
    pn->next = NULL;

    if (*l == NULL){
        *l = pn;
    } else {
        ListaT lt = *l;
        while (lt->next != NULL){
            lt = lt->next;
        }
        lt->next = pn;
    }
}

void crearlista(ListaT *l, int NT){
    // Create list with id_task from 0 to NT-1.
    int i;
    for (i=0;i<NT;i++){
        insertlista(l,i);
    }
}

void deletelista(ListaT *l, int t){
    // Delete node with id_task t in the list l.

    pNodoT pn, pant;

    pn = *l;
    pant = NULL;
    while (pn && pn->task_id != t){
        pant = pn;
        pn = pn->next;
    }

    if (pant == NULL){
        // First element
        *l = pn->next;
    } else {
        pant->next = pn->next;
    }
    free(pn);
}
