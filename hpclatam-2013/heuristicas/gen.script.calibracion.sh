#set -x

#INST_PATH="/home/santiago/bitbucket/cloud-brokering/hpclatam-2013/instances"
INST_PATH="/inco/home01/siturria/Local/Codigo/cloud-brokering/hpclatam-2013/instances"
#INST_PATH="/home/siturria/bitbucket/cloud-brokering/hpclatam-2013/instances"

W[0]="w.i.100.20.1"
WD[0]="100"
S[0]="s.20.1"
SD[0]="20"

W[1]="w.i.200.20.2"
WD[1]="200"
S[1]="s.20.2"
SD[1]="20"

A[0]="CI.list.pack+LS.sh"
A[1]="EDF.list.pack+LS.sh"
A[2]="EFT.list.pack+LS.sh"
A[3]="LGF.list.pack+LS.sh"
A[4]="MaxMaxProfit+LS.sh"
A[5]="MaxPMinT+LS.sh"
A[6]="MaxQTMaxP+LS.sh"
A[7]="MinDMaxP+LS.sh"
A[8]="MinGAPMaxP+LS.sh"
A[9]="MinQTMaxP+LS.sh"
A[10]="MinTMaxP+LS.sh"
A[11]="SRCI.list.pack+LS.sh"

THREADS[0]=1
THREADS[1]=2
THREADS[2]=4
THREADS[3]=8

ITER_COUNT=1000000
EXEC_TIMES=50

TASK_NHD[0]="0.1"
TASK_NHD[1]="0.2"
TASK_NHD[2]="0.3"

MACH_NHD[0]="0.1"
MACH_NHD[1]="0.2"
MACH_NHD[2]="0.3"

RESULT_PATH="result"
mkdir ${RESULT_PATH}

#for (( t_idx=0; t_idx<3; t_idx++ ))
#do
    #for (( m_idx=0; m_idx<3; m_idx++ ))
    #do
		#for (( s_idx=0; s_idx<2; s_idx++ ))
		#do
			#for (( w_idx=0; w_idx<2; w_idx++ ))
			#do	
				#for (( a_idx=0; a_idx<12; a_idx++ ))
				#do
					#echo "> ${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}"
					
					#if [ -f ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} ];
					#then
						#rm ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
					#fi
					
					#if [ -f ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} ];
					#then
						#rm ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
					#fi

					#for (( run=0; run<EXEC_TIMES; run++ ))
					#do
						#time (./${A[a_idx]} ${INST_PATH}/${W[w_idx]} ${INST_PATH}/${S[s_idx]} \
							#${WD[w_idx]} ${SD[s_idx]} ${THREADS[0]} ${ITER_COUNT} \
							#${TASK_NHD[t_idx]} ${MACH_NHD[m_idx]}) 1>> ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} \
								#2>> ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
					#done
				#done
			#done
		#done
    #done
#done


for (( a_idx=0; a_idx<12; a_idx++ ))
do
	echo "> ${A[a_idx]}"
	
	echo "#!/bin/bash" > qsub_${a_idx}.script
	echo "# Nombre del trabajo" >> qsub_${a_idx}.script
	echo "#PBS -N A_${a_idx}" >> qsub_${a_idx}.script
	echo "# Requerimientos" >> qsub_${a_idx}.script
	echo "#PBS -l nodes=1,walltime=02:00:00" >> qsub_${a_idx}.script
	cat gen.script.calibracion.qsub_template.sh >> qsub_${a_idx}.script
	echo "ALGO=${A[a_idx]}" >> qsub_${a_idx}.script
	echo "a_idx=$a_idx" >> qsub_${a_idx}.script
	cat gen.script.calibracion.template.sh >> qsub_${a_idx}.script
done
