
W[0]="w.i.100.20.1"
WD[0]="100"
S[0]="s.20.1"
SD[0]="20"

W[1]="w.i.200.20.2"
WD[1]="200"
S[1]="s.20.2"
SD[1]="20"

THREADS[0]=1
THREADS[1]=2
THREADS[2]=4
THREADS[3]=8

ITER_COUNT=1000000
EXEC_TIMES=50

TASK_NHD[0]="0.1"
TASK_NHD[1]="0.2"
TASK_NHD[2]="0.3"

MACH_NHD[0]="0.1"
MACH_NHD[1]="0.2"
MACH_NHD[2]="0.3"

RESULT_PATH="result"
mkdir ${RESULT_PATH}

for (( t_idx=0; t_idx<3; t_idx++ ))
do
    for (( m_idx=0; m_idx<3; m_idx++ ))
    do
		for (( s_idx=0; s_idx<2; s_idx++ ))
		do
			for (( w_idx=0; w_idx<2; w_idx++ ))
			do	
				echo "> ${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}"
				
				if [ -f ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} ];
				then
					rm ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
				fi
				
				if [ -f ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} ];
				then
					rm ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
				fi

				for (( run=0; run<EXEC_TIMES; run++ ))
				do
					time (./${ALGO} ${INST_PATH}/${W[w_idx]} ${INST_PATH}/${S[s_idx]} \
						${WD[w_idx]} ${SD[s_idx]} ${THREADS[0]} ${ITER_COUNT} \
						${TASK_NHD[t_idx]} ${MACH_NHD[m_idx]}) 1>> ${RESULT_PATH}/${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx} \
							2>> ${RESULT_PATH}/time.${W[w_idx]}_${S[s_idx]}_${TASK_NHD[t_idx]}_${MACH_NHD[m_idx]}.${a_idx}
				done
			done
		done
    done
done
