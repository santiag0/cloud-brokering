set -x

#PARAMS="-DDEBUG"
#PARAMS="-DDEBUG -DRANDOM"
PARAMS="-DRANDOM -DQUIET"

g++ -O3 -lpthread ${PARAMS} SRCI.list.pack+LS.c -o SRCI.list.pack+LS
g++ -O3 -lpthread ${PARAMS} MaxMaxProfit+LS.c -o MaxMaxProfit+LS
g++ -O3 -lpthread ${PARAMS} MinDMaxP+LS.c -o MinDMaxP+LS
g++ -O3 -lpthread ${PARAMS} CI.list.pack+LS.c -o CI.list.pack+LS
g++ -O3 -lpthread ${PARAMS} EDF.list.pack+LS.c -o EDF.list.pack+LS
g++ -O3 -lpthread ${PARAMS} EFT.list.pack+LS.c -o EFT.list.pack+LS
g++ -O3 -lpthread ${PARAMS} LGF.list.pack+LS.c -o LGF.list.pack+LS
g++ -O3 -lpthread ${PARAMS} MaxPMinT+LS.c -o MaxPMinT+LS
g++ -O3 -lpthread ${PARAMS} MaxQTMaxP+LS.c -o MaxQTMaxP+LS
g++ -O3 -lpthread ${PARAMS} MinGAPMaxP+LS.c -o MinGAPMaxP+LS
g++ -O3 -lpthread ${PARAMS} MinQTMaxP+LS.c -o MinQTMaxP+LS
g++ -O3 -lpthread ${PARAMS} MinTMaxP+LS.c -o MinTMaxP+LS
