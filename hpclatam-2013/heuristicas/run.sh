#!/bin/bash

# w 50
for k in 1 2 3 4 5; do
	for j in 10 20 30 50; do
		for h in 1 2 3 4 5; do
			echo "50 $k $j $h"
			#echo "50 $k $j $h" >> results_2
			./ejec ../instances/workloads/w.i.50.10.$k ../instances/scenarios/s.$j.$h 50 $j >> results_2
		done
	done
done

for i in 100 200; do
	for k in 1 2 3 4 5; do
		for j in 10 20 30 50; do
			for h in 1 2 3 4 5; do
				echo "$i $k $j $h"
				#echo "$i $k $j $h" >> results_2
				./ejec ../instances/workloads/w.i.$i.20.$k ../instances/scenarios/s.$j.$h $i $j >> results_2
			done
		done
	done
done

# w 400
for k in 1 2 3 4 5; do
	for j in 10 20 30 50; do
		for h in 1 2 3 4 5; do
			echo "400 $k $j $h"
			#echo "400 $k $j $h" >> results_2
			./ejec ../instances/workloads/w.i.400.50.$k ../instances/scenarios/s.$j.$h 400 $j >> results_2
		done
	done
done
