// STF scheduler for VMPP.
// Shortest task first: search for the shortest request and assign it to the instance that complete it faster.
// Parameters: <workload> <scenario> <workload_size> <scenario_size>
//	

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define INFT 9999999999.0
#define NO_ASIG -1
#define SIZE_NOM_ARCH 80

#define DEBUG 0

typedef struct _nodoT {
   int task_id;
   struct _nodoT *next;
} NodoT;

typedef struct _type_asig{
   int mach;
   float ST;
} asig_type;

typedef NodoT *pNodoT;
typedef NodoT *ListaT;

typedef struct _task {
	int id_task;
	float memory;
	float CPU;
	float storage;
	int num_cores;
	float data_transfer;
	float duration;
	float deadline;
	float arrival_time;
} task;

typedef struct _machine {
	int id_machine;
	float memory;
	float CPU;
	float storage;
	int num_cores;
	int type;
} machine;

// Number of instance types = 8
#define num_IT 8

// Costs: all costs hourly, as of April 2013

// Cost, prebooked intances (hourly basis - 3 years)
float C[num_IT] = {0.027,0.054,0.09,0.108,0.136,0.18,0.316,0.36};

// Cost, prebooked intances (hourly basis - 1 years)
float C1Y[num_IT] = {0.034,0.068,0.09,0.136,0.169,0.18,0.36,0.36};

// Cost, on-demand intances (hourly basis)
float COD[num_IT] = {0.06,0.12,0.12,0.24,0.24,0.41,0.48,0.58};

// Price charged by the broker: 20% less than the cost for on-demand intances (hourly basis)
float price[num_IT] = {0.048,0.096,0.096,0.192,0.192,0.328,0.384,0.464};

// Fixed cost price 3Y
float FC[num_IT] = {96.0,192.0,0.0,384.0,398.0,0.0,972.0,0};

// data transfer cost (per GB)
float DT[num_IT] = {0.12,0.12,0.0894,0.12,0.12,0.0894,0.12,0.0894};

// Scenario properties, needed to compute the cheapest instance

float Memory[num_IT] = {1.7,3.75,3.5,7.5,17.1,7.0,7.0,14.0};
float Storage[num_IT] = {160.0,410.0,489.0,850.0,420.0,999.0,1690.0,2039.0};
float CPU[num_IT] = {1.0,2.0,1.6,2.0,3.25,1.6,2.5,1.6};
float Cores[num_IT] = {1,1,2,2,2,4,8,8};

int main(int argc, char *argv[]){

// Task list type

void printlista(ListaT l){
	// Print list
	ListaT ln = l;
	printf("[");
	while (ln != NULL){
		printf("%d ",ln->task_id);
		ln = ln->next;
	}
	printf("]\n");
}

void insertlista(ListaT *l, int t){
	// Insert node with id_task t at the end of list l.

	pNodoT pn;
	pn = (pNodoT) malloc(sizeof(NodoT));
	pn->task_id = t;
	pn->next = NULL;

	if (*l == NULL){
		*l = pn;
	} else {	
		ListaT lt = *l;
		while (lt->next != NULL){
			lt = lt->next;
		}
		lt->next = pn;
	}
}

void crearlista(ListaT *l, int NT){
	// Create list with id_task from 0 to NT-1.
	int i;
	for (i=0;i<NT;i++){
		insertlista(l,i);
	}
}

void delete(ListaT *l, int t){
	// Delete node with id_task t in the list l.

	pNodoT pn, pant;

	pn = *l;
	pant = NULL;
	while (pn && pn->task_id != t){
		pant = pn;
		pn = pn->next;
	}

	if (pant == NULL){
		// First element
		*l = pn->next;
	} else {
		pant->next = pn->next;
	}
	free(pn);
}	

if (argc < 5){
        printf("Sintaxis: %s <workload> <scenario> <workload_size> <scenario_size>\n", argv[0]);
        exit(1);
}

int NT, NM;

FILE *wf, *sf;

char *workload_file, *scenario_file;
workload_file = (char *)malloc(sizeof(char)*120);
scenario_file = (char *)malloc(sizeof(char)*120);

strcpy(workload_file,argv[1]);
strcpy(scenario_file,argv[2]);

printf("Workload: %s, scenario: %s\n",workload_file,scenario_file);

if((wf=fopen(workload_file, "r"))==NULL){
    printf("Cannot read workload file: %s\n",workload_file);
    exit(1);
}
if((sf=fopen(scenario_file, "r"))==NULL){
    printf("Cannot read scenario file: %s\n",scenario_file);
    exit(1);
}

NT = atoi(argv[3]);
NM = atoi(argv[4]);
printf("NT: %d, NM: %d\n",NT,NM);
int *BF = (int *) malloc(sizeof(int)*NT);
if (BF == NULL){
    printf("Error in BF malloc, dimension %d\n",NT);
    exit(2);
}


// Read instance files

int i,j;

task *TL = (task *) malloc(sizeof(task)*NT);
if (TL == NULL){
	printf("Error in TL malloc, dimension %d\n",NT);
	exit(2);
}

for (i=0;i<NT;i++){
    TL[i].id_task = i;
    fscanf(wf,"%f %f %f %d %f %f %f %f",&(TL[i].memory),&(TL[i].CPU),&(TL[i].storage),&(TL[i].num_cores),&(TL[i].data_transfer),&(TL[i].duration),&(TL[i].deadline),&(TL[i].arrival_time));

    // Search for Best Fit machine
    BF[i] = NO_ASIG;
//printf("task %d, requires: mem: %.2f, CPU: %.2f, storage: %.2f, num_cores_ %d\n",i,TL[i].memory,TL[i].CPU,TL[i].storage,TL[i].num_cores);
    for (j=0;j<num_IT;j++){
//printf("machine %d, provides: mem: %.2f, CPU: %.2f, storage: %.2f, num_cores_ %d\n",j,ML[j].memory,ML[j].CPU,ML[j].storage,ML[j].num_cores);
        if ( (Cores[j] >= TL[i].num_cores) && (CPU[j] >= TL[i].CPU) && (Storage[j] >= TL[i].storage) && (Memory[j] >= TL[i].memory) ){
            BF[i] = j;
            break;
        }
    }
    if (BF[i] == NO_ASIG){
        printf("No BF assignment for request %d\n",i);
printf("task %d, requires: mem: %.2f, CPU: %.2f, storage: %.2f, num_cores_ %d\n",i,TL[i].memory,TL[i].CPU,TL[i].storage,TL[i].num_cores);
        exit(3);
    }
//  printf("BF[%d]: %d\n",i,BF[i]);
}

close(wf);

machine *ML = (machine *) malloc(sizeof(machine)*NT);
if (ML == NULL){
	printf("Error in ML malloc, dimension %d\n",NM);
	exit(2);
}

for (j=0;j<NM;j++){
	ML[j].id_machine = j;
	fscanf(sf,"%f %f %f %d %d",&(ML[j].memory),&(ML[j].CPU),&(ML[j].storage),&(ML[j].num_cores),&(ML[j].type));
}

close(sf);

// Machine array, stores MET
float *mach = (float *) malloc(sizeof(float)*NM);
if (mach == NULL){
	printf("Error al reservar memoria para mach, dimension %d\n",NT);
	exit(2);
}

for (j=0;j<NM;j++){
	mach[j]=0.0;
}

// Array of task-machine assignments
asig_type *sol = (asig_type *) malloc(sizeof(asig_type)*NT);
if (sol == NULL){
	printf("Error in solution malloc, dimension %d\n",NT);
	exit(2);
}

int nro_asig=0;
for (i=0;i<NT;i++){
	sol[i].mach = NO_ASIG;
	sol[i].ST = -1.0;
}

float total_profit = 0.0;
float delta_profit = 0.0;

ListaT l = NULL;

crearlista(&l,NT);

float min_FT, min_ct;
int STF_task, best_machine;

int violated = 0;
ListaT temp_l = NULL;

while ((nro_asig < NT) && (l != NULL)){
	// Select unassigned task with minimum cumputing time.
	STF_task = -1;
	min_ct = INFT;
	temp_l = l;

	// Search tasks.
	while (temp_l != NULL){
		if (TL[temp_l->task_id].duration < min_ct){
			min_ct = TL[temp_l->task_id].duration; 
			STF_task = temp_l->task_id;
		}
		temp_l = temp_l->next;
	}

	min_FT = INFT;
	best_machine = -1;
	// Search machine for STF task 
	for (j=0;j<NM;j++){
		if ( (TL[STF_task].memory <= ML[j].memory) & (TL[STF_task].CPU <= ML[j].CPU) & (TL[STF_task].storage <= ML[j].storage) & (TL[STF_task].num_cores <= ML[j].num_cores) ){
			if (mach[j]	+ TL[STF_task].duration < min_FT){
				min_FT = mach[j] + TL[STF_task].duration;
				best_machine = j;
			}
		}
	}

    if (mach[best_machine] <= TL[STF_task].deadline) {
        // deadline is not violated
        sol[STF_task].mach = best_machine;
        sol[STF_task].ST = mach[best_machine];
        mach[best_machine]+=TL[STF_task].duration;
        delta_profit = TL[STF_task].duration * (price[BF[STF_task]] - C[ML[best_machine].type]);
    } else {
        // deadline violated
        delta_profit = TL[STF_task].duration * (price[BF[STF_task]] - COD[BF[STF_task]]);
		violated++;
    }
    total_profit += delta_profit;
    nro_asig++;

	// l = l->next;
	delete(&l,STF_task);

}

float makespan=0.0;
for (j=0;j<NM;j++){
    printf("M[%d]: %.2f\n",j,mach[j]);
    if (mach[j]>makespan){
        makespan = mach[j];
    }
}

float flowtime = 0.0;

printf("solution: [");
for (i=0;i<NT;i++){
    printf("%d(%.2f)",sol[i].mach,sol[i].ST);
    if (sol[i].ST > 0) {
        flowtime += (sol[i].ST+TL[i].duration);
    } else {
        flowtime += TL[i].duration;
    }
}

printf("]\n");

printf("STF. Profit: %.2f, makespan: %.2f, flowtime: %.2f, violated: %d\n",total_profit,makespan,flowtime,violated);
}

