# Random generator for VPPM instances
# (workloads)

#encoding: utf-8

'''
Created on Apr 16, 2013

@author: Ruso
'''

import sys
import random
import math

# rescheduling time
T_R = 60

# lambda values for sampling

# Memory
M_L = 0.5 

# Processor speed
P_L = 1

# Storage
S_L = 0.01

# Data transfer
# High rate
DT_HL = 50
DT_HU = 250
# Medium rate
DT_ML = 20 
DT_MU = 50
# Low rate
DT_LL = 10 
DT_LU = 20

A_vi = 0 

# number of cores
nC = [1,2,4,8]

# probabilities for nomber of cores requested
pnC = [0.6,0.8,0.95]

if __name__ == '__main__':
    argc = len(sys.argv)
    
    if argc != 4:
        #print "Usage: python %s <size V> <size_B> <lambda_Poisson> <seed>" % sys.argv[0]
        print "Usage: python %s <size V> <size_B> <seed>" % sys.argv[0]
        exit(0)

	# Number of VM requests
    size_V = int(sys.argv[1])

	# Number of pre-booked instances
    size_B = int(sys.argv[2])

	# Lambda for the exponential distribution (arrival times, Poisson process)
    # l = float(sys.argv[3])
    l = float(size_V)/T_R
    # print "l: %.2f" % l

	# Seed for the PRNG
    current_seed = int(sys.argv[3])

	# Time
    T_avg = 60/(size_V/size_B)
    T_std = 10
    
    random.seed(current_seed)

    for task in range(size_V):
		# Sample relevant parameters: M, P, S, T, DT
		M_vi = round(random.expovariate(M_L),2)
		P_vi = round(random.expovariate(P_L),1)
		if P_vi > 3.25:
			P_vi = 3.25
		S_vi = round(random.expovariate(S_L),2)
		if P_vi > 2.5 and S_vi > 420.0:
			S_vi = 420.0
		T_vi = random.normalvariate(T_avg,T_std)
		if T_vi < 0:
			T_vi = T_avg
       
		DT_type = random.randint(1,3) 
		if DT_type == 1:
			# high data transfer
			DT_vi = random.randint(DT_HL,DT_HU)
		if DT_type == 2:
			# medium data transfer
			DT_vi = random.randint(DT_ML,DT_MU)
		if DT_type == 3:
			# low data transfer
			DT_vi = random.randint(DT_LL,DT_LU)

		#A_vi = A_vi + random.expovariate(l)*T_avg
		A_vi = A_vi + random.expovariate(l)

		priority = random.randint(1,5)
		theta = 0.1 * priority 
		# theta indicate how thight is the deadline
		D_vi = T_R * (1 + theta)

		prob_nC = random.uniform(0,1)
		if prob_nC < pnC[0]:
			nC_vi = nC[0]
		elif prob_nC < pnC[1]:
			nC_vi = nC[1]
		elif prob_nC < pnC[2]:
			nC_vi = nC[2]
		else:
			nC_vi = nC[3]

		if nC_vi == 8 and M_vi > 7.0:
			M_vi = 7.0

		if nC_vi == 4 or nC_vi == 8:
			if P_vi > 2.5:
				P_vi = 2.5

		print "%.2f %.2f %.2f %d %.2f %.2f %.2f %.2f" % (M_vi,P_vi,S_vi,nC_vi,DT_vi,T_vi,D_vi,A_vi)

# format line: Memory, Processor, Storage, Number of cores, Data Transfer, Time, Deadline, Arrival Time
