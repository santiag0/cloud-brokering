# Random generator for VPPM instances
# (workloads)
# VARIANT: generate workloads FOR THE 8 INSTANCES IN HPCLATAM PAPER !

#encoding: utf-8

'''
Created on Apr 16, 2013

@author: Ruso
'''

import sys
import random
import math

# Memory, storage, and processor array
# Instances:
# from Amazon: m1.small, m1.medium, m1.large, m2.xlarge, c1.xlarge
# from Azure: medium, large, extralarge

M = [1.7,3.75,7.5,17.1,7.0,3.5,7.0,14.0]
S = [160.0,410.0,850.0,420.0,1690.0,489.0,999.0,2039.0]
P = [1.0,2.0,2.0,3.25,2.5,1.6,1.6,1.6]

# number of computing elements (cores)
nC = [1,1,2,2,8,2,4,8]

# rescheduling time
T_R = 60

# Data transfer
# High rate
DT_HL = 50
DT_HU = 250
# Medium rate
DT_ML = 20 
DT_MU = 50
# Low rate
DT_LL = 10 
DT_LU = 20

A_vi = 0 

if __name__ == '__main__':
    argc = len(sys.argv)
    
    if argc != 4:
        #print "Usage: python %s <size V> <size_B> <lambda_Poisson> <seed>" % sys.argv[0]
        print "Usage: python %s <size V> <size_B> <seed>" % sys.argv[0]
        exit(0)

	# Number of VM requests
    size_V = int(sys.argv[1])

	# Number of pre-booked instances
    size_B = int(sys.argv[2])

	# Lambda for the exponential distribution (arrival times, Poisson process)
    # l = float(sys.argv[3])
    l = float(size_V)/T_R
    # print "l: %.2f" % l

	# Seed for the PRNG
    current_seed = int(sys.argv[3])

	# Time
    T_avg = 60/(size_V/size_B)
    T_std = 10
    
    random.seed(current_seed)

    for task in range(size_V):
		task_type = random.randint(0,7);

		# Sample relevant parameters: M, P, S, T, DT
		M_vi = random.uniform(1,M[task_type]);
		P_vi = random.uniform(1,P[task_type]);
		S_vi = random.uniform(1,S[task_type]);
		T_vi = random.normalvariate(T_avg,T_std)
		nC_vi = random.uniform(1,nC[task_type]);

		if T_vi < 0:
			T_vi = T_avg
       
		DT_type = random.randint(1,3) 
		if DT_type == 1:
			# high data transfer
			DT_vi = random.randint(DT_HL,DT_HU)
		if DT_type == 2:
			# medium data transfer
			DT_vi = random.randint(DT_ML,DT_MU)
		if DT_type == 3:
			# low data transfer
			DT_vi = random.randint(DT_LL,DT_LU)

		#A_vi = A_vi + random.expovariate(l)*T_avg
		A_vi = A_vi + random.expovariate(l)

		priority = random.randint(1,5)
		theta = 0.1 * priority 
		# theta indicate how thight is the deadline
		D_vi = T_R * (1 + theta)

		print "%.2f %.2f %.2f %d %.2f %.2f %.2f %.2f" % (M_vi,P_vi,S_vi,nC_vi,DT_vi,T_vi,D_vi,A_vi)

# format line: Memory, Processor, Storage, Number of cores, Data Transfer, Time, Deadline, Arrival Time
