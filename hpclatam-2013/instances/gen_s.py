# Random generator for VPPM instances
# (scenarios)

#encoding: utf-8

'''
Created on Apr 16, 2013

@author: Ruso
'''

import sys
import random

# Memory, storage, and processor array
# Instances:
# from Amazon: m1.small, m1.medium, m1.large, m2.xlarge, c1.xlarge
# from Azure: medium, large, extralarge

M = [1.7,3.75,7.5,17.1,7.0,3.5,7.0,14.0]
S = [160.0,410.0,850.0,420.0,1690.0,489.0,999.0,2039.0]
P = [1.0,2.0,2.0,3.25,2.5,1.6,1.6,1.6]

# number of computing elements (cores)
nC = [1,1,2,2,8,2,4,8]

# All costs hourly, as of April 2013
# Cost, prebooked intances (hourly basis)
C = [0.027,0.054,0.108,0.136,0.316,0.09,0.18,0.36]
# Cost, on-demand intances (hourly basis)
COD = [0.06,0.12,0.24,0.41,0.58,0.12,0.24,0.48]

# number of instance types
type_number = 8

# data transfer cost (per GB)
DT = [0.12,0.12,0.12,0.12,0.12,0.0894,0.0894,0.0894]

if __name__ == '__main__':
    argc = len(sys.argv)

    if argc != 3:
        print "Usage: python %s <size B> <seed>" % sys.argv[0]
        exit(0)

    size_B = int(sys.argv[1])
    current_seed = int(sys.argv[2])

    random.seed(current_seed)

    for machine in range(size_B):
        # Sample instance type
        type = random.randint(0,type_number-1)
        # print "%.2f %.2f %.2f %.2f %.2f" % (M[type],P[type],S[type],nC[type],C[type],COD[type])
        print "%.2f %.2f %.2f %d %d" % (M[type],P[type],S[type],nC[type],type)

# format of each line: Memory, Processor, Storage, Number of cores, type
